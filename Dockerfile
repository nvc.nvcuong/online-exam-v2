FROM openjdk:8

WORKDIR /online

COPY target/online-exam-0.0.1-SNAPSHOT.jar online/online-exam-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java", "-jar", "online/online-exam-0.0.1-SNAPSHOT.jar"]
