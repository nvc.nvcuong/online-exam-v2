package com.example.onlineexam.config;

import com.example.onlineexam.dto.TokenDto;
import com.example.onlineexam.entity.User;
import com.example.onlineexam.service.UserService;
import com.example.onlineexam.utils.EbsConvertUtils;
import com.example.onlineexam.utils.EbsTokenUtils;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CustomAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

    private final UserService userService;

    public CustomAuthenticationSuccessHandler(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {
        User user = (User) authentication.getPrincipal();

        userService.permanentLock(user.getUsername());

        String accessToken = EbsTokenUtils.createAccessToken(user);
        String refreshToken = EbsTokenUtils.createRefreshToken(user.getUsername());

        TokenDto tokenDto = new TokenDto(accessToken, refreshToken);
        response.getWriter().write(EbsConvertUtils.toString(tokenDto));
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
    }
}
