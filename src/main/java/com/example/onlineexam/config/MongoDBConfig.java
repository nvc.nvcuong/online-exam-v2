package com.example.onlineexam.config;

import com.example.onlineexam.entity.*;
import com.example.onlineexam.repository.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;


@Configuration
@RequiredArgsConstructor
public class MongoDBConfig {
    private final MongoTemplate mongoTemplate;
    private final ExamDayRepository dayRepos;
    private final ExamQuestionRepository examQuestionRepos;
    private final ExamRoomRepository examRoomRepos;
    private final ExaminerRepository examinerRepos;
    private final ExamQuestionRepository questionRepos;
    private final ExamRepository examRepos;
    private final ExamSessionRepository sessionRepos;
    private final ExamSubjectRepository subjectRepos;
    private final ExamTestRepository testRepos;
    private final ExamTestResultRepository testResultRepos;
    private final UserRepository userRepos;
    private final RoleRepository roleRepos;

    @Value("${init.database}")
    private String initDatabase;


    @Bean
    CommandLineRunner initDb() throws IOException {
        this.initialize();
        return null;
    }
    private void initialize() throws IOException {
        if (!mongoTemplate.collectionExists("tbl_exam_day")) {
            ExamDayEntity entity = new ExamDayEntity();
            dayRepos.insert(entity);
        }
        if (!mongoTemplate.collectionExists("tbl_exam")) {
            ExamEntity entity = new ExamEntity();
            examRepos.insert(entity);
        }
        if (!mongoTemplate.collectionExists("tbl_examiner")) {
            ExaminerEntity entity = new ExaminerEntity();
            examinerRepos.insert(entity);
        }
        if (!mongoTemplate.collectionExists("tbl_question")) {
            ExamQuestionEntity entity = new ExamQuestionEntity();
            questionRepos.insert(entity);
        }
        if (!mongoTemplate.collectionExists("tbl_rooms")) {
            ExamRoomEntity entity = new ExamRoomEntity();
            examRoomRepos.insert(entity);
        }
        if (!mongoTemplate.collectionExists("tbl_session")) {
            ExamSessionEntity entity = new ExamSessionEntity();
            sessionRepos.insert(entity);
        }
        if (!mongoTemplate.collectionExists("tbl_subject")) {
            ExamSubjectEntity entity = new ExamSubjectEntity();
            subjectRepos.insert(entity);
        }
        if (!mongoTemplate.collectionExists("tbl_test")) {
            ExamTestEntity entity = new ExamTestEntity();
            testRepos.insert(entity);
        }
        if (!mongoTemplate.collectionExists("tbl_test_result")) {
            ExamTestResultEntity entity = new ExamTestResultEntity();
            testResultRepos.insert(entity);
        }

        if (!mongoTemplate.collectionExists("tbl_user")) {
            ObjectMapper objectMapper = new ObjectMapper();
            User users = objectMapper.readValue(new File(initDatabase + "tbl_user.json"), User.class);
            userRepos.insert(users);
        }
        if (!mongoTemplate.collectionExists("tbl_role")) {
            ObjectMapper objectMapper = new ObjectMapper();
            List<Role> roles = Arrays.asList(objectMapper.readValue(new File(initDatabase + "tbl_role.json"), Role[].class));
            roleRepos.insert(roles);
        }
    }
}
