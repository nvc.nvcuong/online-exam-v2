package com.example.onlineexam.rest;
import com.example.onlineexam.dto.request.ExamRequestDto;
import com.example.onlineexam.dto.request.searchDto.ExamSearchDto;
import com.example.onlineexam.dto.response.ExamResponseDto;
import com.example.onlineexam.service.ExamService;
import com.example.onlineexam.utils.EOResponse;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/v1/exam")
@AllArgsConstructor
public class RestExamController {

    private final ExamService examService;

    @GetMapping("/{id}")
    public EOResponse<ExamResponseDto> getById(@PathVariable(value = "id") UUID id) {
        return EOResponse.build(examService.getById(id));
    }

    @PostMapping
    public EOResponse<ExamResponseDto> createBy(@RequestBody ExamRequestDto dto) {
        return EOResponse.build(examService.createBy(dto));
    }

    @PutMapping("/{id}")
    public EOResponse<ExamResponseDto> updateBy(@PathVariable(value = "id") UUID id, @RequestBody ExamRequestDto dto) {
        return EOResponse.build(examService.updateBy(id, dto));
    }

    @GetMapping("/page")
    public EOResponse<Page<ExamResponseDto>> searchBy(ExamSearchDto dto) {
        return EOResponse.build(examService.searchBy(dto));
    }

    @DeleteMapping("/{id}")
    public EOResponse<Boolean> deleteBy(@PathVariable(value = "id") UUID id){
        examService.deleteBy(id);
        return EOResponse.build(true);
    }

    @DeleteMapping("/{id}/{idExamier}")
    public EOResponse<Boolean> deleteByExamier(@PathVariable(value = "id") UUID id, @PathVariable(value = "idExamier") UUID idExamier ){
        examService.deleteByExamier(id,idExamier);
        return EOResponse.build(true);
    }
}
