package com.example.onlineexam.rest;

import com.example.onlineexam.dto.request.ExamQuestionRequestDto;
import com.example.onlineexam.dto.request.searchDto.ExamQuestionSearchDto;
import com.example.onlineexam.dto.response.ExamQuestionResponseDto;
import com.example.onlineexam.entity.ExamQuestionEntity;
import com.example.onlineexam.service.ExamQuestionService;
import com.example.onlineexam.utils.EOResponse;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1/exam-question")
@AllArgsConstructor
public class RestExamQuestionController {

    private final ExamQuestionService questionService;
    @GetMapping("/{id}")
    public EOResponse<ExamQuestionResponseDto> getById(@PathVariable(value = "id") String id) {
        return EOResponse.build(questionService.getById(id));
    }

    @PostMapping
    public EOResponse<ExamQuestionResponseDto> createBy(@RequestBody @Valid ExamQuestionRequestDto dto) {
        return EOResponse.build(questionService.createBy(dto));
    }

    @PutMapping("/{id}")
    public EOResponse<ExamQuestionResponseDto> updateBy(@PathVariable(value = "id") String id, @RequestBody ExamQuestionRequestDto dto) {
        return EOResponse.build(questionService.updateBy(id, dto));
    }

    @GetMapping("/bySubject/{subjectId}")
    public EOResponse<List<ExamQuestionResponseDto>> getBySubject(@PathVariable(value = "subjectId") String id) {
        return EOResponse.build(questionService.getBySubject(id));
    }

    @GetMapping("/page")
    public EOResponse<Page<ExamQuestionResponseDto>> searchBy(ExamQuestionSearchDto dto) {
        return EOResponse.build(questionService.searchBy(dto));
    }

    @DeleteMapping("/{id}")
    public EOResponse<Boolean> deleteBy(@PathVariable(value = "id") String id) {
        questionService.deleteBy(id);
        return EOResponse.build(true);
    }
    @GetMapping("getAll")
    public EOResponse< List<ExamQuestionEntity>> getAll() {
        return EOResponse.build(questionService.getAllExamQuestion());
    }
}
