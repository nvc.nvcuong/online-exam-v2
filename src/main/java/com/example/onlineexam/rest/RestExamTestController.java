package com.example.onlineexam.rest;

import com.example.onlineexam.dto.request.ExamTestAutoGenRequestDto;
import com.example.onlineexam.dto.request.ExamTestRequestDto;
import com.example.onlineexam.dto.request.searchDto.ExamTestSearchDto;
import com.example.onlineexam.dto.response.ExamTestResponseDto;
import com.example.onlineexam.dto.response.ExaminerUserResponseDto;
import com.example.onlineexam.dto.response.ExanTestExaminer;
import com.example.onlineexam.service.ExamTestService;
import com.example.onlineexam.service.UserService;
import com.example.onlineexam.utils.EOResponse;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
    @RequestMapping("/api/v1/exam-test")
@RequiredArgsConstructor
public class RestExamTestController {
    private final ExamTestService examTestService;
    private final UserService userService;

    @GetMapping("/{id}")
    public EOResponse<ExamTestResponseDto> getById(@PathVariable(value = "id") String id) {
        return EOResponse.build(examTestService.getDtoById(id));
    }

    @PostMapping
    public EOResponse<ExamTestResponseDto> createBy(@RequestBody ExamTestRequestDto dto) {
        return EOResponse.build(examTestService.createBy(dto));
    }

    @PostMapping("/auto-gen-exam-test")
    public EOResponse<String> autoGenExamTest(@NonNull @RequestBody ExamTestAutoGenRequestDto dto) {
        return EOResponse.build(examTestService.createByQuestion(dto));
    }

    @PostMapping("/auto-gen-list-exam-test")
    public EOResponse<String> autoGenListExamTest(@NonNull @RequestBody ExamTestAutoGenRequestDto dto) {
        return EOResponse.build(examTestService.createExamByNumbers(dto));
    }

    @PostMapping("/gen-test-examiner")
    public EOResponse<String> genTestExaminers(@NonNull @RequestBody ExamTestAutoGenRequestDto dto) {
        return EOResponse.build(examTestService.createByExaminers(dto));
    }

    @PutMapping("/{id}")
    public EOResponse<ExamTestResponseDto> updateBy(@PathVariable(value = "id") String id, @NonNull @RequestBody ExamTestRequestDto dto) {
        return EOResponse.build(examTestService.updateBy(id, dto));
    }

    @PutMapping("/approve/{id}")
    public EOResponse<ExamTestResponseDto> approveExamTest(@PathVariable(value = "id") String id) {
        return EOResponse.build(examTestService.approveExamTest(id));
    }

    @GetMapping("/page")
    public EOResponse<Page<ExamTestResponseDto>> searchBy(ExamTestSearchDto dto) {
        return EOResponse.build(examTestService.searchBy(dto));
    }

    @GetMapping("/by-exam/{examId}")
    public EOResponse<List<ExamTestResponseDto>> getByExam(@PathVariable(value = "examId") String id) {
        return EOResponse.build(examTestService.getByExam(id));
    }


    @DeleteMapping("/{id}")
    public EOResponse<Boolean> deleteBy(@PathVariable(value = "id") String id) {
        examTestService.deleteBy(id);
        return EOResponse.build(true);
    }

    @GetMapping("/user-test")
    public EOResponse<ExaminerUserResponseDto> getExamByUser(ExamTestSearchDto dto){
        return EOResponse.build(userService.getExamTestByUser(dto));
    }

    @GetMapping("/show-tested")
    public EOResponse<Page<ExanTestExaminer>> showTested(ExamTestSearchDto dto) {
        return EOResponse.build(examTestService.showTested(dto));
    }

    @PostMapping("/auto-gen-example-test")
    public EOResponse<String> autoGenExampleTest(@NonNull @RequestBody ExamTestAutoGenRequestDto dto) {
        return EOResponse.build(examTestService.autoGenExampleTest(dto));
    }
}
