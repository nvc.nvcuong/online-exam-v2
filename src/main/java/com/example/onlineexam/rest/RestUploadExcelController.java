package com.example.onlineexam.rest;

import com.example.onlineexam.dto.request.ExamSessionRequestDto;
import com.example.onlineexam.dto.request.ExaminerImportRequest;
import com.example.onlineexam.dto.request.ExaminerRequestDto;
import com.example.onlineexam.dto.response.ExamQuestionImportResp;
import com.example.onlineexam.dto.response.ExaminerImportResp;
import com.example.onlineexam.dto.response.ExaminerResponseDto;
import com.example.onlineexam.dto.response.ExaminerUserResponseDto;
import com.example.onlineexam.service.UploadExcelService;
import com.example.onlineexam.utils.EOResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/api/v1/import-excel")
@RequiredArgsConstructor
public class RestUploadExcelController {

    private final UploadExcelService uploadExcelServicel;

    @PostMapping("/import-question")
    public EOResponse<ExamQuestionImportResp> importQuestion (@RequestParam("uploadfiles") MultipartFile[] uploadfiles,
                                                              @RequestParam("examId") String examId, String subjectId) {
        return EOResponse.build(uploadExcelServicel.importQuestions(uploadfiles, examId, subjectId));
    }

//    @PostMapping("/import-examiner")
//    public EOResponse<ExaminerImportResp> imporExaminer(@RequestParam("uploadfiles") MultipartFile[] uploadfiles,
//                                                          ExaminerImportRequest dto) {
//        return EOResponse.build(uploadExcelServicel.importExaminer(uploadfiles, dto));
//    }

    @PostMapping("/import-examiner")
    public EOResponse<ExaminerImportResp> imporExaminer(@RequestParam("uploadfiles") MultipartFile[] uploadfiles,
                                                        String examId) {
        return EOResponse.build(uploadExcelServicel.importExaminer(uploadfiles, examId));
    }
    @PutMapping("/assign-room-for-examinees/{examId}")
    public boolean assignRoomForExaminees(@PathVariable("examId") String examId) {
        return uploadExcelServicel.assignRoomForExaminees(examId);
    }

    @PostMapping("/import-account")
    public EOResponse<ExaminerImportResp> importAccountExaminer(@RequestParam("uploadfiles") MultipartFile[] uploadfiles,
                                                        @RequestParam("examId") String examId) {
        return EOResponse.build(uploadExcelServicel.importAccountExaminer(uploadfiles, examId));
    }
    @PostMapping("/import-list-examiner")
    public EOResponse<ExaminerImportResp> importListExaminer(@RequestBody List<ExaminerRequestDto> dtos){
        return EOResponse.build(uploadExcelServicel.importAccountExaminer(dtos));
    }

}