package com.example.onlineexam.rest;

import com.example.onlineexam.dto.UserDto;
import com.example.onlineexam.dto.request.ExaminerUserRequestDto;
import com.example.onlineexam.dto.request.searchDto.SearchUserDto;
import com.example.onlineexam.dto.response.ExaminerUserResponseDto;
import com.example.onlineexam.service.UserService;
import com.example.onlineexam.utils.EOResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequiredArgsConstructor
@RequestMapping("/user")
public class UserController {
    private final UserService userService;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/{username}")
    public UserDto getByUsername(@PathVariable("username") String username) {
        return userService.getUserDtoByUsername(username);
    }

    @GetMapping
    public UserDto getInfo() {
        return userService.getInfo();
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping
    public UserDto save(@RequestBody UserDto userDto) {
        return userService.save(userDto);
    }

    @PutMapping("/logout")
    public boolean logout(HttpServletRequest request) {
        userService.logout(request);
        return true;
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping("/create-examiner")
    public UserDto createExaminer(@RequestBody ExaminerUserRequestDto dto) {
        return userService.createAccountExaminer(dto);
    }

    //    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/page")
    public EOResponse<Page<ExaminerUserResponseDto>> getExaminerUser(SearchUserDto dto) {
        return EOResponse.build(userService.searchBy(dto));
    }

    @PutMapping("/{id}")
    public UserDto updateAccountExaminer(@PathVariable(value = "id") Long id, @RequestBody ExaminerUserRequestDto dto) {
        return userService.updateAccountExaminer(id, dto);
    }

    @DeleteMapping("/{id}")
    public EOResponse<Boolean> deleteBy(@PathVariable(value = "id") Long id) {
        userService.deleteAccountExaminer(id);
        return EOResponse.build(true);
    }
}