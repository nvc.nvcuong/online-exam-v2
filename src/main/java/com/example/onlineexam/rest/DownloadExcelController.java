package com.example.onlineexam.rest;


import com.example.onlineexam.dto.request.searchDto.ExamTestResultSerchDto;
import com.example.onlineexam.service.ExportExcelService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

@RestController
@RequestMapping("/api/v1/download-excel")
@RequiredArgsConstructor
public class DownloadExcelController {
    private final Logger logger = LoggerFactory.getLogger(DownloadExcelController.class);
    private final ExportExcelService exportExcelService;


    @PostMapping("/danh-sach-diem-thi")
    public void exportDiemThi(HttpServletResponse response, @RequestBody ExamTestResultSerchDto searchDto) {
        ByteArrayResource byteArrayResource;
        try {
            byteArrayResource = exportExcelService.exportDiemThi(searchDto);
            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            response.addHeader("Content-Disposition", "attachment; filename=DANH_SACH_DIEM_THI.xlsx");
            InputStream inputStream = new ByteArrayInputStream(byteArrayResource.getByteArray());
            IOUtils.copy(inputStream, response.getOutputStream());

        } catch (Exception e) {
            logger.error("error  export danh sach diem thi {} ", e.getMessage());
        }

    }
}
