package com.example.onlineexam.rest;
import com.example.onlineexam.dto.request.ExamTestResultRequestDto;
import com.example.onlineexam.dto.request.searchDto.ExamTestResultSerchDto;
import com.example.onlineexam.dto.response.ExamTestResultResponseDto;
import com.example.onlineexam.dto.response.TestResultDetailReponse;
import com.example.onlineexam.service.ExamTestResultService;
import com.example.onlineexam.utils.EOResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/api/v1/result")
@RequiredArgsConstructor
public class RestExamTestResultController {
    @Autowired
    private ExamTestResultService examTestResultService;
    @GetMapping("/{id}")
    public EOResponse<ExamTestResultResponseDto> getById(@PathVariable(value = "id") String id) {
        return EOResponse.build(examTestResultService.getById(id));
    }

    @PostMapping
    public EOResponse<ExamTestResultResponseDto> createBy(@RequestBody ExamTestResultRequestDto dto) {
        return EOResponse.build(examTestResultService.createBy(dto));
    }

    @PutMapping("/update-Mark/{id}")
    public EOResponse<ExamTestResultResponseDto> showMark(@PathVariable(value = "id") String id) {
        return EOResponse.build(examTestResultService.showMark(id));
    }
    @PutMapping("/{id}")
    public EOResponse<ExamTestResultResponseDto> updateBy(@PathVariable(value = "id") String id, @RequestBody ExamTestResultRequestDto dto) {
        return EOResponse.build(examTestResultService.updateBy(id, dto));
    }

    @GetMapping("/page")
    public EOResponse<Page<ExamTestResultResponseDto>> searchBy(ExamTestResultSerchDto dto) {
        return EOResponse.build(examTestResultService.searchBy(dto));
    }

    @DeleteMapping("/{id}")
    public EOResponse<Boolean> deleteBy(@PathVariable(value = "id") String id){
        examTestResultService.deleteBy(id);
        return EOResponse.build(true);
    }

    @DeleteMapping("/deleteByTestId/{id}")
    public EOResponse<Boolean> deleteByTestId(@PathVariable(value = "id") String id) {
        examTestResultService.deleteByTestId(id);
        return EOResponse.build(true);
    }

    @GetMapping("/detail")
    public EOResponse<Page<TestResultDetailReponse>> testDetail(ExamTestResultSerchDto dto) {
        return EOResponse.build(examTestResultService.testDetail(dto));
    }
}
