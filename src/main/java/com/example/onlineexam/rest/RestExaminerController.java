package com.example.onlineexam.rest;

import com.example.onlineexam.dto.request.ExaminerRequestDto;
import com.example.onlineexam.dto.request.searchDto.ExaminerSearchDto;
import com.example.onlineexam.dto.response.ExaminerResponseDto;
import com.example.onlineexam.dto.response.ExaminerUserResponseDto;
import com.example.onlineexam.service.ExaminerService;
import com.example.onlineexam.utils.EOResponse;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/examiners")
@AllArgsConstructor
public class RestExaminerController {

    private final ExaminerService examinerService;

    @GetMapping("/{id}")
    public EOResponse<ExaminerResponseDto> getById(@PathVariable(value = "id") String id) {
        return EOResponse.build(examinerService.getById(id));
    }

    @GetMapping("/page")
    public EOResponse<Page<ExaminerResponseDto>> searchBy(ExaminerSearchDto searchDto) {
        return EOResponse.build(examinerService.searchBy(searchDto));
    }
    @GetMapping("/pagePerson")
    public EOResponse<Page<ExaminerUserResponseDto>> searchPerson(ExaminerSearchDto searchDto) {
        return EOResponse.build(examinerService.searchByPerson(searchDto));
    }
    @PostMapping
    public EOResponse<ExaminerResponseDto> createBy(@RequestBody ExaminerRequestDto dto) {
        return EOResponse.build(examinerService.createBy(dto));
    }

    @PutMapping("/{id}")
    public EOResponse<ExaminerResponseDto> updateBy(@PathVariable(value = "id") String id,
                                                    @RequestBody ExaminerRequestDto dto) {
        return EOResponse.build(examinerService.updateBy(id, dto));
    }

    @DeleteMapping("/{id}")
    public EOResponse<Boolean> deleteBy(@PathVariable(value = "id") String id) {
        examinerService.deleteBy(id);
        return EOResponse.build(true);
    }
}
