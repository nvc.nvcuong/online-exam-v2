package com.example.onlineexam.rest;

import com.example.onlineexam.dto.request.ExamDayRequestDto;
import com.example.onlineexam.dto.request.searchDto.ExamDaySearchDto;
import com.example.onlineexam.dto.response.ExamDayResponseDto;
import com.example.onlineexam.utils.EOResponse;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/exam-day")
@AllArgsConstructor
public class RestExamDayController {

    private final ExamDayService examDayService;

    @GetMapping("/{id}")
    public EOResponse<ExamDayResponseDto> getById(@PathVariable(value = "id") String id) {
        return EOResponse.build(examDayService.getById(id));
    }

    @PostMapping
    public EOResponse<ExamDayResponseDto> createBy(@RequestBody ExamDayRequestDto dto) {
        return EOResponse.build(examDayService.createBy(dto));
    }

    @PutMapping("/{id}")
    public EOResponse<ExamDayResponseDto> updateBy(@PathVariable(value = "id") String id, @RequestBody ExamDayRequestDto dto) {
        return EOResponse.build(examDayService.updateBy(id, dto));
    }

    @GetMapping("/page")
    public EOResponse<Page<ExamDayResponseDto>> searchBy(ExamDaySearchDto dto) {
        return EOResponse.build(examDayService.searchBy(dto));
    }

    @DeleteMapping("/{id}")
    public EOResponse<Boolean> deleteBy(@PathVariable(value = "id") String id) {
        examDayService.deleteBy(id);
        return EOResponse.build(true);
    }
}
