package com.example.onlineexam.rest;


import com.example.onlineexam.dto.TokenDto;
import com.example.onlineexam.dto.UserDto;
import com.example.onlineexam.service.FileDescriptionService;
import com.example.onlineexam.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class PublicController {
    private final UserService userService;
    private final FileDescriptionService fileDescriptionService;

    @GetMapping("/change-password/{username}")
    public boolean createVerificationToChangePassword(@PathVariable("username") String username) {
        return userService.createVerificationToChangePassword(username);
    }

    @PostMapping("/change-password")
    public boolean changePassword(@RequestBody UserDto dto) {
        return userService.changePassword(dto);
    }

    @PostMapping("/register")
    public TokenDto register(@RequestBody UserDto userDto) {
        return userService.register(userDto);
    }

    @PutMapping("/refresh-token")
    public TokenDto refreshToken(@RequestParam("token") String token) {
        return userService.refreshToken(token);
    }

}
