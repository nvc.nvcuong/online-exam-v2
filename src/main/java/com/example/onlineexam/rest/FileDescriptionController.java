package com.example.onlineexam.rest;

import com.example.onlineexam.dto.response.FileDescriptionResponseDto;
import com.example.onlineexam.service.FileDescriptionService;
import com.example.onlineexam.utils.EOResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
@RequestMapping("api/v1/file")
@RequiredArgsConstructor
public class FileDescriptionController {
    private final FileDescriptionService fileDescriptionService;

    @PostMapping("/upload")
    public EOResponse<FileDescriptionResponseDto> saveUploadFile(@RequestParam("uploadFile") MultipartFile file) {
        return EOResponse.build(fileDescriptionService.saveUploadFile(file));
    }

    @PostMapping("/upload-list")
    public EOResponse<List<FileDescriptionResponseDto>> saveUploadListFile(@RequestParam(value = "files") List<MultipartFile> files) {
        return EOResponse.build(fileDescriptionService.saveUploadListFile(files));
    }

    @GetMapping("/{fileId}")
    public EOResponse<FileDescriptionResponseDto> getFileById(@PathVariable(value = "fileId") String fileId) {
        return EOResponse.build(fileDescriptionService.getFile(fileId));
    }

    @GetMapping("/document/{fileId}")
    public void getFile(HttpServletResponse response, @PathVariable(value = "fileId") String fileId) {
        fileDescriptionService.getFileById(response, fileId);
    }


    @GetMapping("/document")
    public void getFileByName(HttpServletResponse response, @RequestParam(value = "fileName") String fileName) {
        fileDescriptionService.getFileByName(response, fileName);
    }

    @DeleteMapping(path = "/{fileId}")
    public EOResponse<Boolean> deleteFileDescription(@PathVariable("fileId") String fileId) {
        Boolean result = fileDescriptionService.deleteFileDescription(fileId);
        return EOResponse.build(result);
    }

    @PostMapping("/upload-image")
    public EOResponse<FileDescriptionResponseDto> saveUploadImage(@RequestParam("uploadImage") MultipartFile file) {
        return EOResponse.build(fileDescriptionService.saveUploadImage(file));
    }

    @GetMapping("/get-image/{imageName}")
    public void getImage(HttpServletResponse response, @PathVariable(value = "imageName") String fileName) {
        fileDescriptionService.viewImage(response, fileName);
    }

}
