package com.example.onlineexam.rest;

import com.example.onlineexam.dto.request.ExamSubjectRequestDto;
import com.example.onlineexam.dto.request.searchDto.ExamSubjectSearchDto;
import com.example.onlineexam.dto.response.ExamSubjectResponseDto;
import com.example.onlineexam.service.ExamSubjectService;
import com.example.onlineexam.utils.EOResponse;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/subject")
@AllArgsConstructor
public class RestExamSubjectController {

    private final ExamSubjectService subjectService;

    @GetMapping("/{id}")
    public EOResponse<ExamSubjectResponseDto> getById(@PathVariable(value = "id") String id) {
        return EOResponse.build(subjectService.getById(id));
    }

    @PostMapping
    public EOResponse<ExamSubjectResponseDto> createBy(@RequestBody ExamSubjectRequestDto dto) {
        return EOResponse.build(subjectService.createBy(dto));
    }

    @PutMapping("/{id}")
    public EOResponse<ExamSubjectResponseDto> updateBy(@PathVariable(value = "id") String id, @RequestBody ExamSubjectRequestDto dto) {
        return EOResponse.build(subjectService.updateBy(id, dto));
    }

    @GetMapping("/page")
    public EOResponse<Page<ExamSubjectResponseDto>> searchBy(ExamSubjectSearchDto dto) {
        return EOResponse.build(subjectService.searchBy(dto));
    }

    @DeleteMapping("/{id}")
    public EOResponse<Boolean> deleteBy(@PathVariable(value = "id") String id) {
        subjectService.deleteBy(id);
        return EOResponse.build(true);
    }
}
