package com.example.onlineexam.rest;

import com.example.onlineexam.dto.request.ExamRoomRequestDto;
import com.example.onlineexam.dto.request.searchDto.ExamRoomSearchDto;
import com.example.onlineexam.dto.response.ExamRoomResponseDto;
import com.example.onlineexam.service.ExamRoomService;
import com.example.onlineexam.utils.EOResponse;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/room")
@AllArgsConstructor
public class RestExamRoomController {

    private final ExamRoomService examRoomService;

    @GetMapping("/{id}")
    public EOResponse<ExamRoomResponseDto> getById(@PathVariable(value = "id") String id) {
        return EOResponse.build(examRoomService.getById(id));
    }

    @PostMapping
    public EOResponse<ExamRoomResponseDto> createBy(@RequestBody ExamRoomRequestDto dto) {
        return EOResponse.build(examRoomService.createBy(dto));
    }

    @PutMapping("/{id}")
    public EOResponse<ExamRoomResponseDto> updateBy(@PathVariable(value = "id") String id, @RequestBody ExamRoomRequestDto dto) {
        return EOResponse.build(examRoomService.updateBy(id, dto));
    }

    @GetMapping("/page")
    public EOResponse<Page<ExamRoomResponseDto>> searchBy(ExamRoomSearchDto dto) {
        return EOResponse.build(examRoomService.searchBy(dto));
    }

    @DeleteMapping("/{id}")
    public EOResponse<Boolean> deleteBy(@PathVariable(value = "id") String id) {
        examRoomService.deleteBy(id);
        return EOResponse.build(true);
    }

}
