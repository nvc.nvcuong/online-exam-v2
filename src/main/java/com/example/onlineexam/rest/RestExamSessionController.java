package com.example.onlineexam.rest;

import com.example.onlineexam.dto.request.ExamSessionRequestDto;
import com.example.onlineexam.dto.request.searchDto.ExamSessionSearchDto;
import com.example.onlineexam.dto.response.ExamSessionResponseDto;
import com.example.onlineexam.service.ExamSessionService;
import com.example.onlineexam.utils.EOResponse;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/exam-session")
@AllArgsConstructor
public class RestExamSessionController {

    private final ExamSessionService examSessionService;

    @GetMapping("/{id}")
    public EOResponse<ExamSessionResponseDto> getById(@PathVariable(value = "id") String id) {
        return EOResponse.build(examSessionService.getById(id));
    }

    @PostMapping
    public EOResponse<ExamSessionResponseDto> createBy(@RequestBody ExamSessionRequestDto dto) {
        return EOResponse.build(examSessionService.createBy(dto));
    }

    @PutMapping("/{id}")
    public EOResponse<ExamSessionResponseDto> updateBy(@PathVariable(value = "id") String id, @RequestBody ExamSessionRequestDto dto) {
        return EOResponse.build(examSessionService.updateBy(id, dto));
    }

    @GetMapping("/page")
    public EOResponse<Page<ExamSessionResponseDto>> searchBy(ExamSessionSearchDto dto) {
        return EOResponse.build(examSessionService.searchBy(dto));
    }

    @GetMapping("/by-exam/{examId}")
    public EOResponse<List<ExamSessionResponseDto>> getByExam(@PathVariable(value = "examId") String id) {
        return EOResponse.build(examSessionService.getByExam(id));
    }


    @DeleteMapping("/{id}")
    public EOResponse<Boolean> deleteBy(@PathVariable(value = "id") String id) {
        examSessionService.deleteBy(id);
        return EOResponse.build(true);
    }
}
