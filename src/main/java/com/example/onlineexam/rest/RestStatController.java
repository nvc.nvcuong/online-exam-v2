package com.example.onlineexam.rest;

import com.example.onlineexam.dto.request.searchDto.StatisticalQuestSearchDto;
import com.example.onlineexam.dto.response.*;
import com.example.onlineexam.service.ThongKeService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/stat")
@RequiredArgsConstructor
public class RestStatController {

    private final ThongKeService thongKeService;

    @GetMapping("/question")
    public StatisticalQuestionRespDto getStatisticalQuestionByExam(StatisticalQuestSearchDto searchDto){
        return thongKeService.getStatisticalQuestionByExam(searchDto);
    }

    @GetMapping("/subject-question")
    public List<SubjectQuestionResponseDto> getStatisticalQuestionBySubject(StatisticalQuestSearchDto searchDto) {
        return thongKeService.getStatisticalQuestionBySubject(searchDto);
    }

    @GetMapping("/examiner")
    public List<ExamUserStats> getStatisticalExaminerByExam() {
        return thongKeService.getStatisticalExaminerByExam();
    }

    @GetMapping("/session-examiner")
    public List<SessionExaminerResponseDto> getStatisticalExaminerBySession(StatisticalQuestSearchDto searchDto) {
        return thongKeService.getStatisticalExaminerBySession(searchDto);
    }

    @GetMapping("/static-mark")
    public StatsMarkResponseDto getStatisticalMark(StatisticalQuestSearchDto search) {
        return thongKeService.getStatisticalMark(search);
    }

    @GetMapping("/static-answer-subject")
    public List<StatsSubjectAnswerQuestResp> getStatisticalAnswerSubject(StatisticalQuestSearchDto search) {
        return thongKeService.getStatisticalAnswerSubject(search);
    }

}
