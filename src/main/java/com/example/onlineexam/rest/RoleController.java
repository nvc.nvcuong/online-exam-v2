package com.example.onlineexam.rest;


import com.example.onlineexam.dto.RoleDto;
import com.example.onlineexam.service.RoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/admin/role")
public class RoleController {

    private final RoleService roleService;

    @PostMapping
    public RoleDto save(@RequestBody RoleDto dto) {
        return roleService.save(dto);
    }

    @PutMapping("/{id}")
    public RoleDto update(@PathVariable("id") Long id, @RequestBody RoleDto dto) {
        return roleService.update(id, dto);
    }

}
