package com.example.onlineexam.repository;

import com.example.onlineexam.entity.ExamRoomEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.UUID;

public interface ExamRoomRepository extends JpaRepository<ExamRoomEntity, UUID> {

    List<ExamRoomEntity> findAllBySessionId(UUID sessionId);
}
