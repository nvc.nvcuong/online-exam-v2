package com.example.onlineexam.repository;

import com.example.onlineexam.entity.ExamSessionEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface ExamSessionRepository extends JpaRepository<ExamSessionEntity, UUID> {
    List<ExamSessionEntity> findByExamId(UUID examId);
}
