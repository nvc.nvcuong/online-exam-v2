package com.example.onlineexam.repository;

import com.example.onlineexam.entity.ExamTestEntity;
import org.bson.types.ObjectId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ExamTestRepository extends JpaRepository<ExamTestEntity, UUID> {

    ExamTestEntity findByExaminerId(UUID examinerId, UUID sessionId);

    ExamTestEntity findByExaminerAndSession(UUID examinerId, UUID sessionId);

    List<ExamTestEntity> findByExam(UUID exam);
}
