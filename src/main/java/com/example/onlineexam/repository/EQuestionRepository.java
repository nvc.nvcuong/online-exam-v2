package com.example.onlineexam.repository;

import com.example.onlineexam.entity.ExamQuestionEntity;
import com.example.onlineexam.exception.EOException;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.stream.Collectors;

import static com.example.onlineexam.constant.ErrorCodes.CONFLICT_VALUE;
import static com.example.onlineexam.constant.MessageCodes.NUMBER_QUESTION_NOT_ENOUGH;

@Repository
public class EQuestionRepository {

    @Autowired
    private MongoTemplate mongoTemplate;

    public List<ObjectId> getRandomQuestions(  List<ExamQuestionEntity> examQuestionEntityList,int count, int level,String examId,String subjectId) {
        String leveCode=level==1?"Easy":level==2?"MEDIUM":level==3?"HARD":"VERY_HARD";

            Set<ObjectId> uniqueQuestionIds = new HashSet<>();

            while (uniqueQuestionIds.size()<count){
                Set<ObjectId> objectIdSet = examQuestionEntityList.stream()
                        .filter(entity -> entity.getLevel() == level && entity.getSubject().toString().equals(subjectId))
                        .limit(count - uniqueQuestionIds.size()) // Giới hạn số lượng phần tử trả về
                        .map(entity -> new ObjectId(entity.getId())) // Chuyển đổi từ ExamQuestionEntity sang objectId
                        .collect(Collectors.toSet());

                uniqueQuestionIds.addAll(objectIdSet);
                if(uniqueQuestionIds.isEmpty()){
                    throw new EOException(CONFLICT_VALUE, NUMBER_QUESTION_NOT_ENOUGH, count+leveCode);
                }
            }

            return new ArrayList<>(uniqueQuestionIds);

    }
}
