package com.example.onlineexam.repository;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ExamDayRepository extends JpaRepository<ExamDayEntity, String> {
}
