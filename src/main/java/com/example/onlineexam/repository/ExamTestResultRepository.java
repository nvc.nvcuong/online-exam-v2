package com.example.onlineexam.repository;

import com.example.onlineexam.entity.ExamTestResultEntity;
import org.bson.types.ObjectId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ExamTestResultRepository extends JpaRepository<ExamTestResultEntity, UUID> {

    List<ExamTestResultEntity> findAllByExamId(UUID examId);

    List<ExamTestResultEntity> findAllByStatus(int status);
    boolean existsByTestAndExaminer(UUID testId, UUID examinerId);


}
