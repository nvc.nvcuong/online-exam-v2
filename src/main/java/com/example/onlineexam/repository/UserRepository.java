package com.example.onlineexam.repository;


import com.example.onlineexam.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);

    @Query("update User u SET u.failedAttempt = ?1 WHERE u.username = ?2")
    void updateFailedAttempts(byte failAttempts, String username);

    @Query("update User u SET u.lockTime = ?1 WHERE u.username = ?2")
    void updateLockedTime(LocalDateTime time, String username);
}
