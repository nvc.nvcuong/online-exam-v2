package com.example.onlineexam.repository;

import com.example.onlineexam.entity.ExaminerEntity;
import org.bson.types.ObjectId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ExaminerRepository extends JpaRepository<ExaminerEntity, UUID> {

    ExaminerEntity findExaminerEntitiesByUserId(Long userId);

    int countAllByExam(ObjectId examId);

    int countAllByExamAndSession(ObjectId examId, ObjectId sessionId);

    ExaminerEntity findByUserId(Long id);

    List<ExaminerEntity> findByExam(ObjectId examId);

    ExaminerEntity findById(ObjectId objectId);
}
