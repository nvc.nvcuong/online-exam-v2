package com.example.onlineexam.repository;

import com.example.onlineexam.entity.ExamQuestionEntity;
import org.bson.types.ObjectId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ExamQuestionRepository extends JpaRepository<ExamQuestionEntity, UUID> {

    List<ExamQuestionEntity> getExamQuestionEntities(ObjectId examId, ObjectId subjectId);
    List<ExamQuestionEntity> findByLevel(int level);
    List<ExamQuestionEntity> findByLevelAndSubject(int level, ObjectId subject);
    List<ExamQuestionEntity> findBySubject(ObjectId subject);

    List<ExamQuestionEntity> findAllByExamAndSubject(ObjectId examId, ObjectId subject);

}
