package com.example.onlineexam.repository;

import com.example.onlineexam.entity.ExamQuestionEntity;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface StaticticalQuestRepository extends MongoRepository<ExamQuestionEntity, String> {

    Integer countAllByExam(UUID examId);

    Integer countAllByExamAndLevel(UUID examId, int level);

    Integer countAllBySubject(UUID subjectId);

    Integer countAllBySubjectAndLevel(UUID subjectId, int level);

    List<ExamQuestionEntity> findAllByExam(UUID examId);

}
