package com.example.onlineexam.repository;

import com.example.onlineexam.entity.ExamSubjectEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ExamSubjectRepository extends JpaRepository<ExamSubjectEntity, UUID> {


}
