package com.example.onlineexam.repository;


import com.example.onlineexam.entity.FileDescriptionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface FileDescriptionRepository extends JpaRepository<FileDescriptionEntity, UUID> {

    List<FileDescriptionEntity> getByName(String name);

    FileDescriptionEntity findByUuid(String uuid);
}
