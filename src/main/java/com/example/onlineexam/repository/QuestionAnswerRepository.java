package com.example.onlineexam.repository;

import com.example.onlineexam.entity.QuestionAnswerEntity;
import org.bson.types.ObjectId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface QuestionAnswerRepository extends JpaRepository<QuestionAnswerEntity, UUID> {

    List<QuestionAnswerEntity> findAllBySubjectId(UUID subjectId);

}
