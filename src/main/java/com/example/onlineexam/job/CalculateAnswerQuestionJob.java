package com.example.onlineexam.job;

import com.example.onlineexam.constant.enums.ExamTestStatus;
import com.example.onlineexam.entity.ExamTestResultEntity;
import com.example.onlineexam.repository.ExamTestResultRepository;
import com.example.onlineexam.service.impl.ExamTestResultImpl;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

@Component
@AllArgsConstructor
@Slf4j
public class CalculateAnswerQuestionJob {

    private final ExamTestResultImpl examTestResult;
    private final ExamTestResultRepository examTestResultRepository;

    @Scheduled(cron = "${scheduler-result.cron}")
    public void updateCorrectAns() {
        log.info("Calculate result exam test sessionid: {}", UUID.randomUUID());
        List<ExamTestResultEntity> entityList = examTestResultRepository
                .findAllByStatus(1);
        for (ExamTestResultEntity e : entityList) {
            log.info("Calculate Result Exam Test id {} ", e.getTest().toString());
            try {
                examTestResult.setCorrectAnswer(e);
                e.setStatus(ExamTestStatus.RESULT_STATUS.DA_CHAM);
                examTestResultRepository.save(e);
            }
            catch (Exception exception){
                log.info("Error {}", exception);
            }
        }
        log.info("End Calculate result exam test!");
    }
}
