package com.example.onlineexam.job;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class ThongKeJob {

    @Scheduled(cron = "-")
    public void run() {
        log.info("Running job ");
    }
}
