package com.example.onlineexam.job;

import com.example.onlineexam.entity.ExamEntity;
import com.example.onlineexam.repository.ExamRepository;
import com.example.onlineexam.service.impl.ExamServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

@Component
@RequiredArgsConstructor
@Slf4j
public class ExamStatusJob {

    private final ExamServiceImpl examService;
    private final ExamRepository examRepository;

    @Scheduled(cron = "${scheduler.cron}")
    public void updateExamStatusDaily() {
        log.info("Update status exam session id: {}", UUID.randomUUID());
        List<ExamEntity> exams = examRepository.findAll();
        for (ExamEntity exam : exams) {
            log.info("Update status for Exam: {}", exam);
            examService.updateStatus(exam);
            examRepository.save(exam);
        }
        log.info("End update status exam!");
    }
}
