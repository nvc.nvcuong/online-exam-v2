package com.example.onlineexam.service;

import com.example.onlineexam.dto.TokenDto;
import com.example.onlineexam.dto.UserDto;
import com.example.onlineexam.dto.request.ExaminerUserRequestDto;
import com.example.onlineexam.dto.request.searchDto.ExamTestSearchDto;
import com.example.onlineexam.dto.request.searchDto.SearchUserDto;
import com.example.onlineexam.dto.response.ExaminerUserResponseDto;
import com.example.onlineexam.entity.User;
import org.springframework.data.domain.Page;

import javax.servlet.http.HttpServletRequest;

public interface UserService {
    // get
    UserDto getUserDtoByUsername(String username);

    User getUserByUsername(String username);

    UserDto getInfo();

    // post
    UserDto save(UserDto dto);

    boolean createVerificationToChangePassword(String username);
//
//    boolean createVerificationToRegister(String username);

    TokenDto register(UserDto dto);

    TokenDto refreshToken(String refreshToken);

    void logout(HttpServletRequest request);

    // put
    boolean changePassword(UserDto dto);

    // System
    void increaseFailedAttempts(String username, byte newFailAttempts);

    void temporaryLock(String username, int coefficient);

    boolean permanentLock(String username);

    UserDto createAccountExaminer(ExaminerUserRequestDto dto);

    ExaminerUserResponseDto getExamTestByUser(ExamTestSearchDto dto);

    Page<ExaminerUserResponseDto> searchBy(SearchUserDto dto);

    UserDto updateAccountExaminer(Long id, ExaminerUserRequestDto dto);

    Boolean deleteAccountExaminer(Long id);
}
