package com.example.onlineexam.service;

import com.example.onlineexam.dto.request.searchDto.StatisticalQuestSearchDto;
import com.example.onlineexam.dto.response.*;

import java.util.List;

public interface ThongKeService {
    void getDataThongKe();

    StatisticalQuestionRespDto getStatisticalQuestionByExam(StatisticalQuestSearchDto searchDto);

    List<SubjectQuestionResponseDto> getStatisticalQuestionBySubject(StatisticalQuestSearchDto searchDto);

    List<ExamUserStats> getStatisticalExaminerByExam();

    List<SessionExaminerResponseDto> getStatisticalExaminerBySession(StatisticalQuestSearchDto searchDto);

    StatsMarkResponseDto getStatisticalMark(StatisticalQuestSearchDto search);

    List<StatsSubjectAnswerQuestResp> getStatisticalAnswerSubject(StatisticalQuestSearchDto search);
}
