package com.example.onlineexam.service.mapper;

import com.example.onlineexam.dto.request.ExamRequestDto;
import com.example.onlineexam.dto.request.ExamRoomRequestDto;
import com.example.onlineexam.dto.request.ExamSessionRequestDto;
import com.example.onlineexam.dto.request.ExamSubjectRequestDto;
import com.example.onlineexam.dto.response.ExamResponseDto;
import com.example.onlineexam.dto.response.ExamRoomResponseDto;
import com.example.onlineexam.dto.response.ExamSessionResponseDto;
import com.example.onlineexam.dto.response.ExamSubjectResponseDto;
import com.example.onlineexam.entity.ExamEntity;
import com.example.onlineexam.entity.ExamRoomEntity;
import com.example.onlineexam.entity.ExamSessionEntity;
import com.example.onlineexam.entity.ExamSubjectEntity;
import org.bson.types.ObjectId;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.lang.annotation.Target;

@Mapper(componentModel = "spring")
public interface ExamSessionResponseMapper {
    @Mapping(target = "exam", ignore = true)
    ExamSessionResponseDto entityToResponse(ExamSessionEntity entity);
    ExamSessionEntity requestToEntity(ExamSessionRequestDto dto);
    ObjectId map(String value);
    ObjectId map(ExamRequestDto value);

    ObjectId map(ExamSubjectRequestDto value);

    ObjectId map(ExamRoomRequestDto value);

    void setValueToResponse(ExamSessionEntity entity, @MappingTarget ExamSessionResponseDto dto);
}
