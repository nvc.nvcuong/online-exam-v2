package com.example.onlineexam.service.mapper;

import com.example.onlineexam.dto.response.FileDescriptionResponseDto;
import com.example.onlineexam.entity.FileDescriptionEntity;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface FileDescriptionResponseMapper {
    FileDescriptionResponseDto entityToResponse(FileDescriptionEntity entity);
}
