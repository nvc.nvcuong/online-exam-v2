package com.example.onlineexam.service.mapper;
import com.example.onlineexam.dto.request.*;
import com.example.onlineexam.dto.response.*;
import com.example.onlineexam.entity.*;
import org.bson.types.ObjectId;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface ExamTestResponseMapper {
    ExamQuestionResponseDto entityToResponse(ExamQuestionEntity entity);

    ObjectId map(String value);
    @Mapping(target = "question", ignore = true)
    @Mapping(target = "examiner", ignore = true)
    ExamTestResponseDto entityToResponseDto (ExamTestEntity entity);

    ExamTestEntity requestToEntity(ExamTestRequestDto dto);

    ObjectId map(ExamDayRequestDto value);

    ObjectId map(ExamQuestionRequestDto value);

    ObjectId map(ExamRequestDto value);

    ObjectId map(ExamSessionRequestDto value);

    ObjectId map(ExaminerRequestDto value);

    ObjectId map(ExamSubjectRequestDto value);
}
