package com.example.onlineexam.service.mapper;

import com.example.onlineexam.dto.request.*;
import com.example.onlineexam.dto.response.ExaminerResponseDto;
import com.example.onlineexam.entity.ExaminerEntity;
import org.bson.types.ObjectId;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface ExaminerResponseMapper {

    ExaminerResponseDto entityToResponse(ExaminerEntity entity);

    String map(ObjectId value);

    ExaminerEntity requestToEntity(ExaminerRequestDto dto);

    ObjectId map(String value);

    ObjectId map(ExamRoomRequestDto value);

    ObjectId map(ExamSubjectRequestDto value);

    ObjectId map(ExamRequestDto value);

    ObjectId map(ExamSessionRequestDto value);
}
