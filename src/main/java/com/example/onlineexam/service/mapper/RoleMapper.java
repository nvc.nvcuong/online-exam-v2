package com.example.onlineexam.service.mapper;


import com.example.onlineexam.dto.RoleDto;
import com.example.onlineexam.entity.Role;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface RoleMapper {
    RoleDto entityToDto(Role entity);
}
