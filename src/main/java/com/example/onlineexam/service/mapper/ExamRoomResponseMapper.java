package com.example.onlineexam.service.mapper;

import com.example.onlineexam.dto.response.ExamRoomResponseDto;
import com.example.onlineexam.entity.ExamRoomEntity;
import org.bson.types.ObjectId;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ExamRoomResponseMapper {
    ExamRoomResponseDto entityToResponseDto(ExamRoomEntity entity);
}
