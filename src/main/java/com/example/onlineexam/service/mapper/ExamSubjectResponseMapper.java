package com.example.onlineexam.service.mapper;

import com.example.onlineexam.dto.request.ExamQuestionRequestDto;
import com.example.onlineexam.dto.request.ExamSubjectRequestDto;
import com.example.onlineexam.dto.response.ExamQuestionResponseDto;
import com.example.onlineexam.dto.response.ExamSubjectResponseDto;
import com.example.onlineexam.entity.ExamQuestionEntity;
import com.example.onlineexam.entity.ExamSubjectEntity;
import org.bson.types.ObjectId;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface ExamSubjectResponseMapper {

    @Mapping(target = "questions", ignore = true)
    ExamSubjectResponseDto entityToResponse(ExamSubjectEntity entity);
    ExamQuestionResponseDto entityToResponse(ExamQuestionEntity entity);

    ExamSubjectEntity requestToEntity(ExamSubjectRequestDto dto);

    List<ObjectId> requestToEntity(List<String> questions);
    ObjectId map(String value);
    ObjectId map(ExamQuestionRequestDto value);

}
