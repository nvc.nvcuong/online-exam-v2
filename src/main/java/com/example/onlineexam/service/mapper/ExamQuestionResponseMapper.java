package com.example.onlineexam.service.mapper;
import com.example.onlineexam.dto.request.ExamQuestionRequestDto;
import com.example.onlineexam.dto.request.ExamRequestDto;
import com.example.onlineexam.dto.request.ExamSubjectRequestDto;
import com.example.onlineexam.dto.response.ExamQuestionResponseDto;

import com.example.onlineexam.entity.ExamQuestionEntity;

import org.bson.types.ObjectId;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface ExamQuestionResponseMapper {
    @Mapping(target = "exam",source = "exam",ignore = true)
    @Mapping(target = "subject",source = "subject",ignore = true)
    ExamQuestionResponseDto entityToResponse(ExamQuestionEntity entity);


    ExamQuestionEntity requestToEntity(ExamQuestionRequestDto dto);


    ObjectId map(ExamSubjectRequestDto value);

    ObjectId map(ExamRequestDto value);
}
