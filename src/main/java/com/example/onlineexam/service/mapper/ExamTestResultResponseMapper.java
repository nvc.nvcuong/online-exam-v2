package com.example.onlineexam.service.mapper;
import com.example.onlineexam.constant.enums.ExamTestStatus;
import com.example.onlineexam.dto.request.ExamTestResultRequestDto;
import com.example.onlineexam.dto.response.ExamTestResultResponseDto;
import com.example.onlineexam.entity.ExamTestEntity;
import com.example.onlineexam.entity.ExamTestResultEntity;
import com.example.onlineexam.repository.ExamQuestionRepository;
import org.bson.types.ObjectId;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

@Mapper(componentModel = "spring")
public interface ExamTestResultResponseMapper {
    @Mapping(target ="questionAnswer" ,source = "questionAnswer")
    @Mapping(target = "status", source = "status")
    ExamTestResultResponseDto entityToResponseDto(ExamTestResultEntity entity);

    @Mapping(target = "status", source = "status")
    ExamTestResultEntity requestToEntity(ExamTestResultRequestDto dto);
    ObjectId map(String value);
    default String map(ObjectId objectId) {
        return objectId != null ? objectId.toString() : null;
    }
    void setValue(ExamTestResultEntity entity, @MappingTarget ExamTestResultResponseDto dto);
}
