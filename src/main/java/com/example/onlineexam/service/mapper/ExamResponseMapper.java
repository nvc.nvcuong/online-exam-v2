package com.example.onlineexam.service.mapper;

import com.example.onlineexam.dto.response.ExamResponseDto;
import com.example.onlineexam.entity.ExamEntity;
import org.bson.types.ObjectId;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface ExamResponseMapper {

    @Mapping(target = "examinersId", ignore = true)
    @Mapping(target = "examTest", ignore = true)
    ExamResponseDto entityToResponse(ExamEntity exam);

    String map (ObjectId id);
}
