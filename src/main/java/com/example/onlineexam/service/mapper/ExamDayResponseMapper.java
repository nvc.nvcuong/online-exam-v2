package com.example.onlineexam.service.mapper;

import com.example.onlineexam.dto.request.ExamDayRequestDto;
import com.example.onlineexam.dto.response.ExamDayResponseDto;
import org.bson.types.ObjectId;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ExamDayResponseMapper {
    ExamDayResponseDto entityToResponse(ExamDayEntity entity);
    ObjectId map(String value);
    ExamDayEntity  requestToEntity(ExamDayRequestDto dto);
}
