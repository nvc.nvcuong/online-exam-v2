package com.example.onlineexam.service.mapper;


import com.example.onlineexam.dto.UserDto;
import com.example.onlineexam.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface UserMapper {
    @Mapping(target = "password", ignore = true)
    UserDto entityToDto(User entity);
}
