package com.example.onlineexam.service;

import com.example.onlineexam.dto.response.FileDescriptionResponseDto;
import lombok.NonNull;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.UUID;

public interface FileDescriptionService {
    FileDescriptionResponseDto saveUploadFile(@NonNull MultipartFile file);

    List<FileDescriptionResponseDto> saveUploadListFile(@NonNull List<MultipartFile> fileList);

    void getFileById(HttpServletResponse response, String fileId);

    void getFileByName(HttpServletResponse response, String fileName);

    FileDescriptionResponseDto getFile(@NonNull String fileId);

    boolean deleteFileDescription(@NonNull String fileId);

    FileDescriptionResponseDto saveUploadImage(@NonNull MultipartFile image);

    void viewImage(HttpServletResponse response, String fileName);
}
