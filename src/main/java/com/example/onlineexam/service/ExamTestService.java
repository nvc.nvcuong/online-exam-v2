package com.example.onlineexam.service;

import com.example.onlineexam.dto.request.ExamTestAutoGenRequestDto;
import com.example.onlineexam.dto.request.ExamTestRequestDto;
import com.example.onlineexam.dto.request.searchDto.ExamTestSearchDto;
import com.example.onlineexam.dto.response.ExamTestResponseDto;
import com.example.onlineexam.dto.response.ExanTestExaminer;
import com.example.onlineexam.entity.ExamTestEntity;
import lombok.NonNull;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ExamTestService {

    ExamTestResponseDto getDtoById(String id);

    List<ExamTestResponseDto> getByExam(String id);

    public Page<ExamTestResponseDto> searchBy(ExamTestSearchDto dto);

    ExamTestResponseDto createBy(@NonNull ExamTestRequestDto dto);

    ExamTestResponseDto updateBy(@NonNull String id, @NonNull ExamTestRequestDto dto);

    ExamTestResponseDto approveExamTest(@NonNull String id);

    Boolean deleteBy(@NonNull String id);

    String createByQuestion(@NonNull ExamTestAutoGenRequestDto dto);

    String createExamByNumbers(@NonNull ExamTestAutoGenRequestDto dto);

    String createByExaminers(@NonNull ExamTestAutoGenRequestDto dto);

    Page<ExanTestExaminer> showTested(ExamTestSearchDto dto);

    ExamTestResponseDto entityToResponseDtoMapper (ExamTestEntity entity);

    String autoGenExampleTest(@NonNull ExamTestAutoGenRequestDto dto);

}
