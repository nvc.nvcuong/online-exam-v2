package com.example.onlineexam.service;

import com.example.onlineexam.dto.request.ExamSessionRequestDto;
import com.example.onlineexam.dto.request.ExaminerImportRequest;
import com.example.onlineexam.dto.request.ExaminerRequestDto;
import com.example.onlineexam.dto.response.ExamQuestionImportResp;
import com.example.onlineexam.dto.response.ExaminerImportResp;
import com.example.onlineexam.dto.response.ExaminerResponseDto;
import com.example.onlineexam.dto.response.ExaminerUserResponseDto;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface UploadExcelService {

     ExamQuestionImportResp importQuestions(MultipartFile[] uploadFiles, String examId, String subjectId);

     ExaminerImportResp importExaminer(MultipartFile[] uploadFiles, String examId);

     boolean assignRoomForExaminees(String examId);

     ExaminerImportResp importAccountExaminer(MultipartFile[] uploadFiles, String examId);
     ExaminerImportResp importAccountExaminer(List<ExaminerRequestDto> dto);
}
