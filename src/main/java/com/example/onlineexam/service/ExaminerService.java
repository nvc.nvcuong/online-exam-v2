package com.example.onlineexam.service;

import com.example.onlineexam.dto.request.ExaminerRequestDto;
import com.example.onlineexam.dto.request.searchDto.ExaminerSearchDto;
import com.example.onlineexam.dto.response.ExaminerResponseDto;
import com.example.onlineexam.dto.response.ExaminerUserResponseDto;
import com.example.onlineexam.entity.ExaminerEntity;
import lombok.NonNull;
import org.springframework.data.domain.Page;

public interface ExaminerService {
    ExaminerResponseDto getById(String id);
    Page<ExaminerResponseDto> searchBy(ExaminerSearchDto searchDto);
    ExaminerResponseDto createBy(@NonNull ExaminerRequestDto dto);
    ExaminerResponseDto updateBy(@NonNull String id, @NonNull ExaminerRequestDto dto);
    void deleteBy(@NonNull String id);
    Page<ExaminerUserResponseDto> searchByPerson(ExaminerSearchDto searchDto);

    ExaminerEntity requestToEntityMapper(ExaminerRequestDto dto);
}
