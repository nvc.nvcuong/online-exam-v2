package com.example.onlineexam.service.impl;

import com.example.onlineexam.constant.MessageCodes;
import com.example.onlineexam.constant.enums.ExamTestStatus;
import com.example.onlineexam.constant.enums.ExaminerUserEnum;
import com.example.onlineexam.dto.request.ExamTestResultRequestDto;
import com.example.onlineexam.dto.request.QuestionAnswerRequestDto;
import com.example.onlineexam.dto.request.searchDto.ExamTestResultSerchDto;
import com.example.onlineexam.dto.response.ExamTestResultResponseDto;
import com.example.onlineexam.dto.response.QuestionAnswerResponseDto;
import com.example.onlineexam.dto.response.TestResultDetailReponse;
import com.example.onlineexam.entity.*;
import com.example.onlineexam.error.UserStatus;
import com.example.onlineexam.exception.EOException;
import com.example.onlineexam.exception.EntityNotFoundException;
import com.example.onlineexam.repository.ExamQuestionRepository;
import com.example.onlineexam.repository.ExamTestRepository;
import com.example.onlineexam.repository.ExamTestResultRepository;
import com.example.onlineexam.repository.ExaminerRepository;
import com.example.onlineexam.service.ExamTestResultService;
import com.example.onlineexam.service.ExamTestService;
import com.example.onlineexam.service.mapper.ExamTestResponseMapper;
import com.example.onlineexam.service.mapper.ExamTestResultResponseMapper;
import com.example.onlineexam.service.mapper.ExaminerResponseMapper;
import com.example.onlineexam.utils.PageUtils;
import lombok.AllArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.domain.*;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import static com.example.onlineexam.constant.ErrorCodes.ENTITY_NOT_FOUND;

@Service
@AllArgsConstructor
public class ExamTestResultImpl implements ExamTestResultService {
    private final ExamTestResultRepository examTestResultRepository;
    private final MongoTemplate mongoTemplate;
    private final ExamQuestionRepository examQuestionRepository;
    private final ExamTestResultResponseMapper examTestResultResponseMapper;
    private final ExamQuestionServiceImpl questionService;
    private final ExaminerServiceImpl examinerService;
    private final ExaminerResponseMapper examinerMapper;
    private final ExaminerRepository examinerRepo;
    private final ExamTestService examTestService;
    private final ExamTestResponseMapper examTestMapper;
    private final ExamTestRepository examTestRepo;

    @Override
    public ExamTestResultResponseDto getById(String id) {
        ExamTestResultEntity examTestResultEntity = examTestResultRepository.findById(id)
                .orElseThrow(() -> new EOException(ENTITY_NOT_FOUND, MessageCodes.ENTITY_NOT_FOUND, id));
        return this.entityToResponseDtoMapper(examTestResultEntity);

    }
    private void validateDto(ExamTestResultRequestDto dto){
        if(examTestResultRepository.existsByTestAndExaminer(new ObjectId(dto.getTestId())
                ,new ObjectId(dto.getExaminerId()))){
            throw new EOException(UserStatus.USERNAME_HAD_TEST);
        }
    }
    @Override
    public ExamTestResultResponseDto createBy(@NotNull ExamTestResultRequestDto dto) {
        this.validateDto(dto);
        ExamTestResultEntity entity = requestToEntityMapper(dto);
        examTestResultRepository.save(entity);
        return entityToResponseDtoMapper(entity);
    }

    @Override
    public ExamTestResultResponseDto updateBy(String id, @NotNull ExamTestResultRequestDto dto) {
        ExamTestResultEntity entity = examTestResultRepository.findById(id)
                .orElseThrow(() -> new EOException(ENTITY_NOT_FOUND, MessageCodes.ENTITY_NOT_FOUND, id));
        entity = this.requestToEntityMapper(dto);
        examTestResultRepository.save(entity);
        return this.entityToResponseDtoMapper(entity);
    }

    @Override
    public ExamTestResultResponseDto showMark(String id) {
        ExamTestResultEntity examTestResultEntity = examTestResultRepository.findById(id)
                .orElseThrow(() -> new EOException(ENTITY_NOT_FOUND, MessageCodes.ENTITY_NOT_FOUND, id));
        return this.entityToResponseDtoMapperMark(examTestResultEntity);
    }

    @Override
    public Page<ExamTestResultResponseDto> searchBy(@NotNull ExamTestResultSerchDto dto) {

        Query query = new Query();
        Pageable pageable = PageUtils.getPageable(dto.getPageIndex(), dto.getPageSize());
        this.genWhereClause(dto, query);
        query.with(Sort.by(Sort.Order.desc("startDate")));
        query.with(pageable);
        List<ExamTestResultEntity> entityList = mongoTemplate.find(query, ExamTestResultEntity.class);
        long count = entityList.size();
        List<ExamTestResultResponseDto> responseDtos = entityList.stream().map(this::entityToResponseDtoMapper).collect(Collectors.toList());
        return new PageImpl<>(responseDtos, pageable, count);
    }

    private void genWhereClause(ExamTestResultSerchDto dto, Query query) {
        if ( StringUtils.hasText(dto.getExamId())) {
            query.addCriteria(Criteria.where("examId").is( new ObjectId(dto.getExamId())));
        }
        if (StringUtils.hasText(dto.getTestId())) {
            query.addCriteria(Criteria.where("testId").is( new ObjectId(dto.getTestId())));
        }
    }

    @Override
    public Boolean deleteBy(@NotNull String id) {
        examTestResultRepository.deleteById(id);
        return true;
    }
    @Override
    public Boolean deleteByTestId(String testId) {
        Query query = new Query(Criteria.where("exam_test_id").is(new ObjectId(testId)));
        mongoTemplate.remove(query, ExamTestResultEntity.class);
        return true;
    }

    public ExamTestResultEntity calculateExamResult(ExamTestResultEntity entity) {

        int correctAnswers = 0;
        int incorrectAnswers = 0;
        int totalQuestion = entity.getTotalQuestion();

        for (QuestionAnswerEntity questionAnswer : entity.getQuestionAnswer()) {
            if (questionAnswer.getAnswer().length() == questionAnswer.getCorrectAnswer().length()){
                String[] arrQuestionAnswer = questionAnswer.getAnswer().split(",");
                String[] arrQuestionAnswerCorrect = questionAnswer.getCorrectAnswer().split(",");

                List<String> listA = Arrays.asList(arrQuestionAnswer);
                List<String> listB = Arrays.asList(arrQuestionAnswerCorrect);

                if (new HashSet<>(listB).containsAll(listA)) {
                    correctAnswers++;
                }
            }

        }
        incorrectAnswers = totalQuestion - correctAnswers;

        float mark = (float) correctAnswers / totalQuestion * 10;
        mark = (float) (Math.round(mark * 100.0) / 100.0);

        entity.setCorrectAnswers(correctAnswers);
        entity.setIncorrectAnswers(incorrectAnswers);
        entity.setTotalQuestion(totalQuestion);
        entity.setMark(mark);

        return entity;
    }

    private void setExamForTestResult(ExamTestResultEntity entity, String testId){
        ExamTestEntity test = examTestRepo.findById(testId)
                .orElseThrow(()-> new EntityNotFoundException(ExamTestEntity.class.getName(), "ID", testId));
        entity.setExamId(test.getExam());
    }

    public ExamTestResultEntity requestToEntityMapper(ExamTestResultRequestDto dto) {
        ExamTestResultEntity entity = examTestResultResponseMapper.requestToEntity(dto);

        if (dto.getExaminerId() != null)
            entity.setExaminer(new ObjectId(dto.getExaminerId()));
        if (dto.getTestId() != null)
            entity.setTest(new ObjectId(dto.getTestId()));

        List<QuestionAnswerEntity> questionAnswerEntities = new ArrayList<>();
        for (QuestionAnswerRequestDto answerRequestDto : dto.getQuestionAnswer()) {
            QuestionAnswerEntity answerEntity = new QuestionAnswerEntity();
            answerEntity.setQuestionId(answerRequestDto.getQuestionId());
            answerEntity.setAnswer(answerRequestDto.getAnswer());
            answerEntity.setCorrectAnswer(answerRequestDto.getCorrectAnswer());
            questionAnswerEntities.add(answerEntity);
        }
        this.setExamForTestResult(entity, dto.getTestId());
        entity.setStatus(ExamTestStatus.RESULT_STATUS.CHUA_CHAM);
        return entity;
    }

    public QuestionAnswerResponseDto mapToResponseDto(QuestionAnswerEntity entity) {
        QuestionAnswerResponseDto dto = new QuestionAnswerResponseDto();
        dto.setQuestionId(entity.getQuestionId().toString());
        dto.setAnswer(entity.getAnswer());
        dto.setCorrectAnswer(entity.getCorrectAnswer());
        return dto;
    }

    ExamTestResultResponseDto entityToResponseDtoMapper(ExamTestResultEntity entity) {

        ExamTestResultResponseDto examTestResultResponseDto = examTestResultResponseMapper.entityToResponseDto(entity);
        if (entity.getExaminer() != null) {
            ExaminerEntity examinerEntity = examinerRepo.findById(entity.getExaminer().toString())
                    .orElseThrow(() -> new EntityNotFoundException(ExaminerEntity.class.getName(), "id", entity.getExaminer().toString()));
            examinerEntity.setStatus(ExaminerUserEnum.USER_STATUS.DA_THI);
            examinerRepo.save(examinerEntity);
            examTestResultResponseDto.setExaminer(examinerMapper.entityToResponse(examinerEntity));
        }

        if (entity.getTest() != null)
            examTestResultResponseDto.setTest(examTestMapper.entityToResponseDto(examTestRepo.findById(entity.getTest().toString())
                    .orElseThrow(() -> new EntityNotFoundException(ExamTestEntity.class.getName(), "id", entity.getTest().toString()))));

        List<QuestionAnswerResponseDto> questionAnswerDtoList = new ArrayList<>();
        for (QuestionAnswerEntity questionAnswerEntity : entity.getQuestionAnswer()) {
            QuestionAnswerResponseDto questionAnswerResponseDto = mapToResponseDto(questionAnswerEntity);
            questionAnswerDtoList.add(questionAnswerResponseDto);
        }
        examTestResultResponseDto.setQuestionAnswer(questionAnswerDtoList);

        return examTestResultResponseDto;
    }
    ExamTestResultResponseDto entityToResponseDtoMapperMark(ExamTestResultEntity entity) {

        ExamTestResultResponseDto examTestResultResponseDto = this.entityToResponseDtoMapper(entity);
        List<QuestionAnswerEntity> questionAnswerList = entity.getQuestionAnswer();
        AtomicReference<Float> Mark = new AtomicReference<>((float) 0);
        examTestResultResponseDto.setMark(Mark.get());
        return examTestResultResponseDto;
    }
    @Override
    public Page<TestResultDetailReponse> testDetail(@NotNull ExamTestResultSerchDto dto) {
        Query query = new Query();
        Pageable pageable = PageRequest.of(dto.getPageIndex(), dto.getPageSize());
        if (dto.getTestId()!=null) {
            query.addCriteria(Criteria.where("exam_test_id").is(new ObjectId(dto.getTestId())));
        }
        query.with(Sort.by(Sort.Order.desc("startDate")));
        query.with(pageable);
        List<ExamTestResultEntity> entityList = mongoTemplate.find(query, ExamTestResultEntity.class);

        long count = entityList.size();
        List<TestResultDetailReponse> responseDtos=new ArrayList<>();
        if (!entityList.isEmpty()) {
            this.mapTestDetail(responseDtos, entityList.get(0));
        }
        return new PageImpl<>(responseDtos, pageable, count);
    }

    private void mapTestDetail(List<TestResultDetailReponse> reponses, ExamTestResultEntity entityList) {
        ExamTestResultResponseDto examTestResultResponseDto = this.entityToResponseDtoMapper(entityList);
        for (QuestionAnswerResponseDto e : examTestResultResponseDto.getQuestionAnswer()) {
            TestResultDetailReponse testResultDetailReponse = new TestResultDetailReponse();
            testResultDetailReponse.setQuestionAnswer(e);
            ExamQuestionEntity entity = examQuestionRepository.findById(e.getQuestionId()).orElseThrow(
                    () -> new EntityNotFoundException(ExamQuestionEntity.class.getName(), "id", e.getQuestionId()));
            String content = entity.getQuestionContent();
            testResultDetailReponse.setContent(content);
            List<String> answers = new ArrayList<>();
            questionService.mapAnswerQuestionToList(entity, answers);
            testResultDetailReponse.setAnswers(answers);
            reponses.add(testResultDetailReponse);
        }
    }

    public void setCorrectAnswer(ExamTestResultEntity entity) {
        for (QuestionAnswerEntity questionAnswer : entity.getQuestionAnswer()) {
            if (Objects.isNull(questionAnswer.getCorrectAnswer())) {
                ExamQuestionEntity examQuestionEntity = examQuestionRepository.findById(questionAnswer.getQuestionId().toString()).orElseThrow(
                        () -> new EntityNotFoundException(ExamQuestionEntity.class.getName(), "id", questionAnswer.getQuestionId()));
                questionAnswer.setCorrectAnswer(examQuestionEntity.getCorrectAnswer());
            }
        }
        calculateExamResult(entity);
    }
}
