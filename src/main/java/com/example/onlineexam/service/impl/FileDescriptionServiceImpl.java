package com.example.onlineexam.service.impl;


import com.example.onlineexam.dto.response.FileDescriptionResponseDto;
import com.example.onlineexam.entity.FileDescriptionEntity;
import com.example.onlineexam.exception.EOException;
import com.example.onlineexam.repository.FileDescriptionRepository;
import com.example.onlineexam.service.FileDescriptionService;
import com.example.onlineexam.service.mapper.FileDescriptionResponseMapper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;


import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.example.onlineexam.constant.MessageCodes.INVALID_VALUE;
import static com.example.onlineexam.error.CommonStatus.NOT_FOUND;


@Slf4j
@Service
@RequiredArgsConstructor
public class FileDescriptionServiceImpl implements FileDescriptionService {
    private final FileDescriptionResponseMapper fileDescriptionResponseMapper;
    private final FileDescriptionRepository fileDescriptionRepository;

    @Value("${config.upload-path}")
    private String path;

    @Value("${config.image-path}")
    private String imagePath;

    @Value("${config.view-path}")
    private String viewImage;

    @Override
    public FileDescriptionResponseDto saveUploadFile(@NonNull MultipartFile file) {

        String filePath = Paths.get(path).toAbsolutePath().toString();
        FileDescriptionEntity fileDesc = new FileDescriptionEntity();
        try {
            Path pathFile = Paths.get(filePath);
            Files.createDirectories(pathFile);

            File fileToBeSave = new File(filePath, fileDesc.getUuid());
            file.transferTo(fileToBeSave);

            fileDesc.setName(fileDesc.getUuid());
            fileDesc.setContentType(file.getContentType());
            fileDesc.setContentSize(file.getSize());
            fileDesc.setFilePath(fileToBeSave.getAbsolutePath());
            fileDesc = fileDescriptionRepository.save(fileDesc);

        } catch (Exception e) {
            log.error("Upload to server fail: Error: {}", e.getMessage(), e);
        }
        return fileDescriptionResponseMapper.entityToResponse(fileDesc);
    }

    @Override
    public List<FileDescriptionResponseDto> saveUploadListFile(@NonNull List<MultipartFile> fileList) {

        List<FileDescriptionEntity> entities = new ArrayList<>();
        if (ObjectUtils.isNotEmpty(fileList) && fileList.size() > 0) {
            for (MultipartFile dto : fileList) {
                String filePath = Paths.get(path).toAbsolutePath().toString();
                FileDescriptionEntity entity = new FileDescriptionEntity();
                try {

                    Path file = Paths.get(filePath);
                    Files.createDirectories(file);

                    File fileToBeSave = new File(filePath, dto.getOriginalFilename());
                    dto.transferTo(fileToBeSave);

                    entity.setName(dto.getOriginalFilename());
                    entity.setContentType(dto.getContentType());
                    entity.setContentSize(dto.getSize());
                    entity.setFilePath(fileToBeSave.getAbsolutePath());
                    entity = fileDescriptionRepository.save(entity);

                    entities.add(entity);
                } catch (Exception e) {
                    log.error("Upload to server fail: Error: {}", e.getMessage(), e);
                }
            }
        }
        List<FileDescriptionResponseDto> fileResponseDtoList = entities.stream()
                .map(fileDescriptionResponseMapper::entityToResponse)
                .collect(Collectors.toList());
        return fileResponseDtoList;
    }

    @Override
    public void getFileById(HttpServletResponse response, @NonNull String fileId) {
        FileDescriptionEntity entity = fileDescriptionRepository.findById(fileId)
                .orElseThrow(() -> new EOException(NOT_FOUND.getCode(), INVALID_VALUE, fileId));

        if (entity.getContentType().equals("application/pdf")) {
            response.setContentType("application/pdf");
        }

        if (entity.getContentType().equals("application/vnd.openxmlformats-officedocument.wordprocessingml.document")) {
            response.setContentType("application/vnd.openxmlformats-officedocument.wordprocessingml.document");
        }

        if (entity.getContentType().equals("image/jpeg")) {
            response.setContentType("image/jpeg");
        }

        if (entity.getContentType().equals("image/png")) {
            response.setContentType("image/png");
        }

        File file = new File(entity.getFilePath());
        try {
            if (file.exists()) {
                OutputStream outputStream = response.getOutputStream();
                FileInputStream in = new FileInputStream(file);
                IOUtils.copy(in, outputStream);
                outputStream.close();
                in.close();

            } else {
                throw new FileNotFoundException();
            }
        } catch (Exception e) {
            log.error("Get file error {}", e.getMessage());
        }
    }

    @Override
    public void getFileByName(HttpServletResponse response, String fileName) {
        List<FileDescriptionEntity> entityList = fileDescriptionRepository.getByName(fileName);

        if (!entityList.isEmpty()) {
            String contentType = entityList.get(0).getContentType();
            response.setContentType(contentType);

            try {
                OutputStream outputStream = response.getOutputStream();

                for (FileDescriptionEntity entity : entityList) {
                    File file = new File(entity.getFilePath());

                    if (file.exists()) {
                        FileInputStream in = new FileInputStream(file);
                        IOUtils.copy(in, outputStream);
                        in.close();
                    } else {
                        log.error("File not found: {}", file.getAbsolutePath());
                    }
                }

                outputStream.close();
            } catch (Exception e) {
                log.error("Get file error: {}", e.getMessage());
            }
        } else {
            log.error("No files found with the specified name: {}", fileName);
        }
    }

    @Override
    public FileDescriptionResponseDto getFile(@NonNull String fileId) {

        FileDescriptionEntity entity = fileDescriptionRepository.findByUuid(fileId);
        return fileDescriptionResponseMapper.entityToResponse(entity);
    }

    @Override
    public boolean deleteFileDescription(@NonNull String fileId) {
        if (ObjectUtils.isNotEmpty(fileId)) {
            fileDescriptionRepository.findById(fileId).ifPresent(file -> fileDescriptionRepository.deleteById(fileId));
            return true;
        }
        throw new EOException(NOT_FOUND.getCode(), INVALID_VALUE, fileId);
    }

    @Override
    public FileDescriptionResponseDto saveUploadImage(MultipartFile image) {
        String filePath = Paths.get(imagePath).toAbsolutePath().toString();
        FileDescriptionEntity fileDesc = new FileDescriptionEntity();
        try {
            Path pathFile = Paths.get(filePath);
            Files.createDirectories(pathFile);

            File fileToBeSave = new File(filePath, fileDesc.getUuid());
            image.transferTo(fileToBeSave);

            fileDesc.setName(fileDesc.getUuid());
            fileDesc.setContentType(image.getContentType());
            fileDesc.setContentSize(image.getSize());
            fileDesc.setFilePath(viewImage + fileDesc.getUuid());
            fileDesc = fileDescriptionRepository.save(fileDesc);

        } catch (Exception e) {
            log.error("Upload to server fail: Error: {}", e.getMessage(), e);
        }
        return fileDescriptionResponseMapper.entityToResponse(fileDesc);
    }

    public void viewImage(HttpServletResponse response, String filename) {
        File file = new File(imagePath + "/" + filename);
        try {
            if (file.exists()) {
                String contentType = "application/octet-stream";
                response.setContentType(contentType);
                OutputStream out = response.getOutputStream();
                FileInputStream in = new FileInputStream(file);
                IOUtils.copy(in, out);
                out.close();
                in.close();
            } else {
                throw new FileNotFoundException();
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }

    }
}
