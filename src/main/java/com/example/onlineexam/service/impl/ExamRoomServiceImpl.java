package com.example.onlineexam.service.impl;

import com.example.onlineexam.constant.ErrorCodes;
import com.example.onlineexam.constant.MessageCodes;
import com.example.onlineexam.dto.request.ExamRoomRequestDto;
import com.example.onlineexam.dto.request.searchDto.ExamRoomSearchDto;
import com.example.onlineexam.dto.response.ExamRoomResponseDto;
import com.example.onlineexam.entity.ExamRoomEntity;
import com.example.onlineexam.exception.EOException;
import com.example.onlineexam.repository.ExamRoomRepository;
import com.example.onlineexam.service.ExamRoomService;
import com.example.onlineexam.service.mapper.ExamRoomResponseMapper;
import com.example.onlineexam.utils.PageUtils;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.data.domain.*;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ExamRoomServiceImpl implements ExamRoomService {

    private final ExamRoomRepository examRoomRepos;
    private final MongoTemplate mongoTemplate;
    private final ExamRoomResponseMapper roomResponseMapper;


    @Override
    public ExamRoomResponseDto getById(String id) {
        ExamRoomEntity entity = examRoomRepos.findById(id)
                .orElseThrow(() -> new EOException(ErrorCodes.ENTITY_NOT_FOUND, MessageCodes.ENTITY_NOT_FOUND, id));
        return roomResponseMapper.entityToResponseDto(entity);
    }

    @Override
    public ExamRoomResponseDto createBy(ExamRoomRequestDto dto) {
        ExamRoomEntity entity = new ExamRoomEntity();
        this.setValue(entity, dto);
        examRoomRepos.save(entity);
        return roomResponseMapper.entityToResponseDto(entity);
    }

    @Override
    public ExamRoomResponseDto updateBy(String id, ExamRoomRequestDto dto) {
        ExamRoomEntity entity = examRoomRepos.findById(id).
                orElseThrow(() -> new EOException(ErrorCodes.ENTITY_NOT_FOUND, MessageCodes.ENTITY_NOT_FOUND, id));
        this.setValue(entity, dto);
        examRoomRepos.save(entity);
        return roomResponseMapper.entityToResponseDto(entity);
    }

    public void setValue(ExamRoomEntity entity, ExamRoomRequestDto dto) {
        entity.setNameRoom(dto.getNameRoom());
        entity.setSlots(dto.getSlots());
    }

    @Override
    public Page<ExamRoomResponseDto> searchBy(@NonNull ExamRoomSearchDto searchDto) {
        Query query = new Query();
        Pageable pageable = PageUtils.getPageable(searchDto.getPageIndex(), searchDto.getPageSize());
        if (StringUtils.hasText(searchDto.getKeyword())) {
            query.addCriteria(Criteria.where("").is(searchDto.getKeyword()));
        }
        query.with(Sort.by(Sort.Order.desc("startDate")));
        query.with(pageable);
        List<ExamRoomEntity> entityList = mongoTemplate.find(query, ExamRoomEntity.class);
        long count = entityList.size();
        List<ExamRoomResponseDto> responseDtos = entityList.stream().map(roomResponseMapper::entityToResponseDto).collect(Collectors.toList());
        return new PageImpl<>(responseDtos, pageable, count);
    }

    @Override
    public void deleteBy(String id) {
        examRoomRepos.findById(id).orElseThrow(() -> new EOException(ErrorCodes.ENTITY_NOT_FOUND, MessageCodes.ENTITY_NOT_FOUND, id));
        examRoomRepos.deleteById(id);
    }
}
