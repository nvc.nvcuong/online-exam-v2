package com.example.onlineexam.service.impl;

import com.example.onlineexam.constant.ErrorCodes;
import com.example.onlineexam.constant.MessageCodes;
import com.example.onlineexam.dto.request.ExamRoomRequestDto;
import com.example.onlineexam.dto.request.ExamSessionRequestDto;
import com.example.onlineexam.dto.request.searchDto.ExamSessionSearchDto;
import com.example.onlineexam.dto.response.ExamRoomResponseDto;
import com.example.onlineexam.dto.response.ExamSessionResponseDto;
import com.example.onlineexam.entity.*;
import com.example.onlineexam.exception.EOException;
import com.example.onlineexam.exception.EntityNotFoundException;
import com.example.onlineexam.repository.ExamRepository;
import com.example.onlineexam.repository.ExamRoomRepository;
import com.example.onlineexam.repository.ExamSessionRepository;
import com.example.onlineexam.repository.ExamSubjectRepository;
import com.example.onlineexam.service.ExamSessionService;
import com.example.onlineexam.service.mapper.ExamResponseMapper;
import com.example.onlineexam.service.mapper.ExamRoomResponseMapper;
import com.example.onlineexam.service.mapper.ExamSessionResponseMapper;
import com.example.onlineexam.service.mapper.ExamSubjectResponseMapper;
import com.example.onlineexam.utils.PageUtils;
import lombok.AllArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.domain.*;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ExamSessionServiceImpl implements ExamSessionService {
    private final ExamSessionRepository examSessionRepository;
    private final ExamSessionResponseMapper examSessionResponseMapper;
    private MongoTemplate mongoTemplate;
    private final ExamRepository examRepository;
    private final ExamSubjectRepository examSubjectRepository;
    private final ExamRoomRepository examRoomRepository;
    private final ExamResponseMapper examMapper;
    private final ExamSubjectResponseMapper subjectMapper;
    private final ExamRoomResponseMapper roomMapper;


    @Override
    public ExamSessionResponseDto getById(UUID id) {
        ExamSessionEntity entity = examSessionRepository.findById(id)
                .orElseThrow(()->new EOException(ErrorCodes.ENTITY_NOT_FOUND, MessageCodes.ENTITY_NOT_FOUND, String.valueOf(id)));
        return entityToResponseMapper(entity);
    }

    @Override
    public ExamSessionResponseDto createBy(ExamSessionRequestDto dto) {
        ExamSessionEntity entity = requestToEntityMapper(dto);
        examSessionRepository.save(entity);
        return entityToResponseMapper(entity);
    }

    @Override
    public ExamSessionResponseDto updateBy(UUID id, ExamSessionRequestDto dto) {
        ExamSessionEntity entity = examSessionRepository.findById(id)
                .orElseThrow(() -> new EOException(ErrorCodes.ENTITY_NOT_FOUND, MessageCodes.ENTITY_NOT_FOUND, String.valueOf(id)));
        setValue(entity, dto);
        examSessionRepository.save(entity);
        return entityToResponseMapper(entity);
    }

    public void setValue(ExamSessionEntity entity, ExamSessionRequestDto dto)
    {
        entity.setExamDay(dto.getExamDay());
        entity.setExam(this.requestToEntityMapper(dto).getExam());
        entity.setSubject(this.requestToEntityMapper(dto).getSubject());
//        entity.setExamRoom(this.requestToEntityMapper(dto).getExamRoom());
        entity.setStartSession(dto.getStartSession());
        entity.setEndSession(dto.getEndSession());
    }

    ExamSessionEntity requestToEntityMapper(ExamSessionRequestDto dto)
    {
        ExamSessionEntity entity = examSessionResponseMapper.requestToEntity(dto);
        entity.setExam(new ObjectId(dto.getExam().getId()));
        entity.setSubject(new ObjectId(dto.getSubject().getId()));
        Set<String> examRoomIdSet = new HashSet<>();
        List<ObjectId> examRoomIds = new ArrayList<>();
        if (dto.getExamRoom() != null) {
            for (ExamRoomRequestDto room : dto.getExamRoom()) {
                if (examRoomIdSet.contains(room.getId())) {
                    new EOException(HttpStatus.INTERNAL_SERVER_ERROR, "error.room_already_added", "500");
                } else {
                    examRoomIdSet.add(room.getId());
                    examRoomIds.add(new ObjectId(room.getId()));
                }
            }
//            entity.setExamRoom(examRoomIds);
        }

        return entity;
    }

    @Override
    public ExamSessionResponseDto entityToResponseMapper(ExamSessionEntity entity)
    {
        ExamSessionResponseDto examSessionResponseDto = examSessionResponseMapper.entityToResponse(entity);

        if (entity.getExam() != null) {
            examSessionResponseDto.setExam(examMapper.entityToResponse(examRepository.findById(entity.getExam().toString())
                    .orElseThrow(() -> new EntityNotFoundException(ExamEntity.class.getName(), "id", entity.getExam().toString()))));
        }

        if (entity.getSubject() != null) {
            examSessionResponseDto.setSubject(subjectMapper.entityToResponse(examSubjectRepository.findById(entity.getSubject().toString())
                    .orElseThrow(() -> new EntityNotFoundException(ExamSubjectEntity.class.getName(), "id", entity.getSubject().toString()))));
        }

        List<ExamRoomResponseDto> rooms = new ArrayList<>();
        if (entity.getExamRoom() != null) {
            for (ObjectId roomId : entity.getExamRoom()) {
                ExamRoomEntity roomEntity = examRoomRepository.findById(roomId.toString())
                        .orElseThrow(() -> new EntityNotFoundException(ExamRoomEntity.class.getName(), "id", roomId.toString()));
                rooms.add(roomMapper.entityToResponseDto(roomEntity));
            }
        }
        examSessionResponseDto.setRoom(rooms);
        return examSessionResponseDto;
    }


    @Override
    public Page<ExamSessionResponseDto> searchBy (ExamSessionSearchDto dto) {
        Query query = new Query();
        Pageable pageable = PageUtils.getPageable(dto.getPageIndex(), dto.getPageSize());
        if (StringUtils.hasText(dto.getKeyword())) {
            query.addCriteria(Criteria.where("").is(dto.getKeyword()));
        }
        query.with(Sort.by(Sort.Order.desc("startTime")));
        query.with(pageable);
        List<ExamSessionEntity> entityList = mongoTemplate.find(query, ExamSessionEntity.class);
        long cout = entityList.size();
        List<ExamSessionResponseDto> responseDtos = entityList.stream()
                .map(this::entityToResponseMapper)
                .collect(Collectors.toList());
        return new PageImpl<>(responseDtos, pageable, cout);
    }

    @Override
    public List<ExamSessionResponseDto> getByExam(String id) {
        List<ExamSessionEntity> sessions = examSessionRepository.findByExamId(new ObjectId(id));
        return sessions.stream()
                .map(this::entityToResponseMapper)
                .collect(Collectors.toList());
    }

    @Override
    public void deleteBy(UUID id) {
        examSessionRepository.findById(id).orElseThrow(()->new EOException(ErrorCodes.ENTITY_NOT_FOUND, MessageCodes.ENTITY_NOT_FOUND, String.valueOf(id)));
        examSessionRepository.deleteById(id);
    }
}
