package com.example.onlineexam.service.impl;

import com.example.onlineexam.constant.ErrorCodes;
import com.example.onlineexam.constant.MessageCodes;
import com.example.onlineexam.dto.request.ExamQuestionRequestDto;
import com.example.onlineexam.dto.request.searchDto.ExamQuestionSearchDto;
import com.example.onlineexam.dto.response.ExamQuestionResponseDto;
import com.example.onlineexam.entity.ExamEntity;
import com.example.onlineexam.entity.ExamQuestionEntity;
import com.example.onlineexam.entity.ExamSubjectEntity;
import com.example.onlineexam.exception.EOException;
import com.example.onlineexam.exception.EntityNotFoundException;
import com.example.onlineexam.repository.ExamQuestionRepository;
import com.example.onlineexam.repository.ExamRepository;
import com.example.onlineexam.repository.ExamSubjectRepository;
import com.example.onlineexam.service.ExamQuestionService;
import com.example.onlineexam.service.mapper.ExamQuestionResponseMapper;
import com.example.onlineexam.service.mapper.ExamResponseMapper;
import com.example.onlineexam.service.mapper.ExamSubjectResponseMapper;
import com.example.onlineexam.utils.PageUtils;
import com.example.onlineexam.utils.RenderCodeTest;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.bson.types.ObjectId;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.*;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.example.onlineexam.error.CommonStatus.NOT_FOUND;

@Service
@AllArgsConstructor
public class ExamQuestionServiceImpl implements ExamQuestionService {

    private final ExamQuestionRepository examQuestionRepos;
    private final ExamSubjectRepository examSubjectRepository;
    private final ExamRepository examRepository;
    private final ExamQuestionResponseMapper questionResponseMapper;
    private final ExamSubjectResponseMapper examSubjectResponseMapper;
    private final ExamResponseMapper examResponseMapper;
    private final MongoTemplate mongoTemplate;
    @Override
    public ExamQuestionResponseDto getById(String id) {
        ExamQuestionEntity entity = examQuestionRepos.findById(id).orElseThrow(() -> new EOException(ErrorCodes.ERROR_CODE, MessageCodes.ENTITY_NOT_FOUND, id));
        return this.entityToResponseMapper(examQuestionRepos.findById(id)
                .orElseThrow(()-> new EntityNotFoundException(NOT_FOUND.getMessage())));
    }

    @Override
    public ExamQuestionResponseDto createBy(ExamQuestionRequestDto dto) {
        ExamQuestionEntity entity = this.requestToEntityMapper(dto);
        entity.setQuestionCode(RenderCodeTest.setValue());
        examQuestionRepos.save(entity);
        return this.entityToResponseMapper(entity);
    }

    @Override
    public ExamQuestionResponseDto updateBy(String id, ExamQuestionRequestDto dto) {
        ExamQuestionEntity entity = examQuestionRepos.findById(id).orElseThrow(()-> new EOException(ErrorCodes.ERROR_CODE, MessageCodes.ENTITY_NOT_FOUND, id));
        entity = this.requestToEntityMapper(dto);
        entity.setId(new ObjectId(id));
        examQuestionRepos.save(entity);
        return this.entityToResponseMapper(entity);
    }

    @Override
    public Page<ExamQuestionResponseDto> searchBy(@NonNull ExamQuestionSearchDto searchDto) {
        Query query = new Query();
        Pageable pageable = PageUtils.getPageable(searchDto.getPageIndex(), searchDto.getPageSize());
        if (StringUtils.hasText(searchDto.getKeyword())) {
            query.addCriteria(Criteria.where("").is(searchDto.getKeyword()));
        }
        if(searchDto.getExamId()!=null){
            query.addCriteria(Criteria.where("exam_id").is(new ObjectId(searchDto.getExamId())));
        }
        if(searchDto.getSubject()!=null){
            query.addCriteria(Criteria.where("subject_id").is(new ObjectId(searchDto.getSubject())));
        }
        query.with(Sort.by(Sort.Order.desc("startDate")));
        query.with(pageable);
        List<ExamQuestionEntity> entityList = mongoTemplate.find(query, ExamQuestionEntity.class);
        long count = entityList.size();
        List<ExamQuestionResponseDto> responseDtos = entityList.stream().map(this::entityToResponseMapper).collect(Collectors.toList());
        return new PageImpl<>(responseDtos, pageable, count);
    }

    public List<ExamQuestionResponseDto> getBySubject(String id) {
        List<ExamQuestionEntity> subject = examQuestionRepos.findBySubject(new ObjectId(id));
        return subject.stream()
                .map(this::entityToResponseMapper)
                .collect(Collectors.toList());
    }

    @Override
    public void deleteBy(String id) {
        examQuestionRepos.deleteById(id);
    }

    ExamQuestionResponseDto entityToResponseMapper(ExamQuestionEntity entity){
        ExamQuestionResponseDto examQuestionResponseDto = questionResponseMapper.entityToResponse(entity);
        List<String> answers = new ArrayList<>();
        mapAnswerQuestionToList(entity, answers);
        examQuestionResponseDto.setAnswers(answers);
        if(entity.getExam()!=null)
            examQuestionResponseDto.setExam(examResponseMapper
                    .entityToResponse(examRepository.findById(entity.getExam().toString())
                            .orElseThrow(() -> new EntityNotFoundException(ExamEntity.class.getName(), "id", entity.getExam().toString()))));
        if(entity.getSubject()!=null)
            examQuestionResponseDto
                    .setSubject(examSubjectResponseMapper.entityToResponse(examSubjectRepository.findById(entity.getSubject().toString())
                            .orElseThrow(() -> new EntityNotFoundException(ExamSubjectEntity.class.getName(), "id", entity.getSubject().toString()))));

        return  examQuestionResponseDto;
    }

    public void mapAnswerQuestionToList(ExamQuestionEntity question, List<String> answers) {
        answers.add(question.getAnswer1());
        answers.add(question.getAnswer2());
        answers.add(question.getAnswer3());
        answers.add(question.getAnswer4());
        answers.add(question.getAnswer5());
        answers.add(question.getAnswer6());
        answers.add(question.getAnswer7());
        answers.add(question.getAnswer8());
        answers.add(question.getAnswer9());
        answers.add(question.getAnswer10());
    }

    ExamQuestionEntity requestToEntityMapper(ExamQuestionRequestDto dto) {
        ExamQuestionEntity entity = questionResponseMapper.requestToEntity(dto);
        if (dto.getExam() != null)
            entity.setExam(new ObjectId(dto.getExam().getId()));
        if (dto.getSubject() != null)
            entity.setSubject(new ObjectId(dto.getSubject().getId()));
        return entity;
    }



    @Override
    @Cacheable(value ="questions")
//    @Scheduled(cron = "${scheduler.cron}")
    public List<ExamQuestionEntity> getAllExamQuestion() {
        List<ExamQuestionEntity> examQuestionEntityList = examQuestionRepos.findAll();
        return examQuestionEntityList;
    }



}
