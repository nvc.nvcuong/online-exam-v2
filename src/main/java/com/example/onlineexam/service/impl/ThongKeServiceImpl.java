package com.example.onlineexam.service.impl;

import com.example.onlineexam.constant.MessageCodes;
import com.example.onlineexam.constant.enums.ExamTestEnum;
import com.example.onlineexam.dto.request.searchDto.StatisticalQuestSearchDto;
import com.example.onlineexam.dto.response.*;
import com.example.onlineexam.entity.*;
import com.example.onlineexam.exception.EOException;
import com.example.onlineexam.exception.EntityNotFoundException;
import com.example.onlineexam.repository.*;
import com.example.onlineexam.service.ThongKeService;
import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.*;

import static com.example.onlineexam.constant.ErrorCodes.CONFLICT_VALUE;
import static com.example.onlineexam.constant.MessageCodes.*;
import static com.example.onlineexam.constant.enums.ExamTestEnum.DIFFICULTY.*;
import static com.example.onlineexam.error.CommonStatus.NOT_FOUND;

@Service
@RequiredArgsConstructor
public class ThongKeServiceImpl implements ThongKeService {

    private final StaticticalQuestRepository questRepos;
    private final ExamSubjectRepository subjectRepos;
    private final ExamRepository examRepos;
    private final ExaminerRepository examinerRepos;
    private final ExamSessionRepository sessionRepos;
    private final ExamQuestionRepository questionRepos;
    private final ExamTestResultRepository examTestResultRepos;
    private final QuestionAnswerRepository questionAnswerRepos;
    private final ExamTestRepository testRepository;

    @Override
    public void getDataThongKe() {

    }

    @Override
    public StatisticalQuestionRespDto getStatisticalQuestionByExam(StatisticalQuestSearchDto searchDto) {

        StatisticalQuestionRespDto respDto = new StatisticalQuestionRespDto();
        this.setValueResponse(respDto, searchDto);
        return respDto;
    }

    private void setValueResponse(StatisticalQuestionRespDto response, StatisticalQuestSearchDto dto){

        if (Objects.isNull(dto.getExamId())){
            throw new EOException(HttpStatus.NOT_FOUND, EXAM_ID_NOT_NULL, null);
        }
        ObjectId examId = new ObjectId(dto.getExamId());

        Integer countAllQuest = questRepos.countAllByExam(examId);
        response.setTotalQuestion(countAllQuest);

        Integer countQuestEasy = questRepos.countAllByExamAndLevel(examId, EASY.getCode());
        response.setTotalQuestEasy(countQuestEasy);

        Integer countQuestMedium = questRepos.countAllByExamAndLevel(examId, ExamTestEnum.DIFFICULTY.MEDIUM.getCode());
        response.setTotalQuestMedium(countQuestMedium);

        Integer countQuestHard = questRepos.countAllByExamAndLevel(examId, ExamTestEnum.DIFFICULTY.HARD.getCode());
        response.setTotalQuestHard(countQuestHard);

        Integer countQuestVeryHard = questRepos.countAllByExamAndLevel(examId, ExamTestEnum.DIFFICULTY.VERY_HARD.getCode());
        response.setTotalQuestVeryHard(countQuestVeryHard);
    }

    @Override
    public List<SubjectQuestionResponseDto> getStatisticalQuestionBySubject(StatisticalQuestSearchDto searchDto) {

        List<SubjectQuestionResponseDto> responseDtos = new ArrayList<>();
        ObjectId examId =  searchDto.getExamId() == null ? null : new ObjectId(searchDto.getExamId());
        ObjectId subId =  searchDto.getSubjectId() == null ? null : new ObjectId(searchDto.getSubjectId());
        List<ExamQuestionEntity> listQuestion = questionRepos.getExamQuestionEntities(examId, subId);

        Map<String, List<ExamQuestionEntity>> groupMap = this.groupQuestionBySubject(listQuestion);

        for (Map.Entry<String, List<ExamQuestionEntity>> entry : groupMap.entrySet()) {
            SubjectQuestionResponseDto response = new SubjectQuestionResponseDto();
            String subjectId = entry.getKey();

            ExamSubjectEntity subject = subjectRepos.findById(subjectId)
                    .orElseThrow(() -> new EOException(NOT_FOUND.getCode(), MessageCodes.ENTITY_NOT_FOUND, subjectId));
            response.setSubjectId(subjectId);
            response.setSubjectName(subject.getSubjectName());

            List<ExamQuestionEntity> questionSubject = entry.getValue();
            StatisticalQuestionRespDto statQuest = new StatisticalQuestionRespDto();

            long easyCount = questionSubject.stream().filter(q -> q.getLevel() == EASY.getCode()).count();
            long mediumCount = questionSubject.stream().filter(q -> q.getLevel() == MEDIUM.getCode()).count();
            long hardCount = questionSubject.stream().filter(q -> q.getLevel() == HARD.getCode()).count();
            long veryHardCount = questionSubject.stream().filter(q -> q.getLevel() == VERY_HARD.getCode()).count();

            statQuest.setTotalQuestion(questionSubject.size());
            statQuest.setTotalQuestEasy((int) easyCount);
            statQuest.setTotalQuestMedium((int) mediumCount);
            statQuest.setTotalQuestHard((int) hardCount);
            statQuest.setTotalQuestVeryHard((int) veryHardCount);

            response.setStats(statQuest);
            responseDtos.add(response);
        }
        return responseDtos;
    }

    private Map<String, List<ExamQuestionEntity>> groupQuestionBySubject(List<ExamQuestionEntity> questions) {
        Map<String, List<ExamQuestionEntity>> subjectQuestMap = new HashMap<>();

        for (ExamQuestionEntity question : questions) {
            if (Objects.nonNull(question.getSubject())){
                String subjectId = question.getSubject().toString();

                subjectQuestMap.computeIfAbsent(subjectId, k -> new ArrayList<>());
                subjectQuestMap.get(subjectId).add(question);
            }
        }
        return subjectQuestMap;
    }

    @Override
    public List<ExamUserStats> getStatisticalExaminerByExam() {

        List<ExamUserStats> stats = new ArrayList<>();
        List<ExamEntity> exams = examRepos.findAll();
        for (ExamEntity exam : exams){
            ExamUserStats stat = new ExamUserStats();
            stat.setExamId(exam.getId());
            stat.setExamName(exam.getExamName());
            stat.setNumberExaminers(examinerRepos.countAllByExam(new ObjectId(exam.getId())));
            stats.add(stat);
        }
        return stats;
    }

    @Override
    public List<SessionExaminerResponseDto> getStatisticalExaminerBySession(StatisticalQuestSearchDto searchDto) {

        List<SessionExaminerResponseDto> stats = new ArrayList<>();
        if (examinerRepos.existsById(searchDto.getExamId())){
            throw new EOException(NOT_FOUND.getCode(), NOT_FOUND.getMessage(), EXAM_NOT_EXIST);
        }
        List<ExamSessionEntity> sessions = sessionRepos.findByExamId(new ObjectId(searchDto.getExamId()));

        for (ExamSessionEntity session : sessions) {
            SessionExaminerResponseDto stat = new SessionExaminerResponseDto();
            stat.setSessionId(session.getId());
            String sessionName = session.getStartSession() + " - " + session.getEndSession();
            stat.setSessionName(sessionName);
            stat.setNumberExaminer(examinerRepos.countAllByExamAndSession(new ObjectId(searchDto.getExamId()), new ObjectId(session.getId())));
            stats.add(stat);
        }
        return stats;
    }

    @Override
    public StatsMarkResponseDto getStatisticalMark(StatisticalQuestSearchDto search) {
        StatsMarkResponseDto stat = new StatsMarkResponseDto();
        Set<Map<Float, Float>> mapMarks = new HashSet<>();
        ExamEntity exam = examRepos.findById(search.getExamId())
                .orElseThrow(()-> new EntityNotFoundException(ExamEntity.class.getName(), "id", search.getExamId()));
        stat.setExamId(exam.getId());
        stat.setExamName(exam.getExamName());

        List<ExamTestResultEntity> examiners = examTestResultRepos.findAllByExamId(new ObjectId(exam.getId()));

        for (float i = 0.5f; i < 10.0f; i += 0.5f) {
            Map<Float, Float> marks = new HashMap<>();
            mapMarks.add(marks);
        }

        for (ExamTestResultEntity entity : examiners) {
            float mark = entity.getMark();
            for (Map<Float, Float> marks : mapMarks) {
                float lowerBound = (float) (Math.floor(mark * 2) / 2.0);
                float upperBound = (float) (Math.ceil(mark * 2) / 2.0);
                if (upperBound > 10.0f) upperBound = 10.0f;

                if (mark >= lowerBound && mark <= upperBound) {
                    marks.put(lowerBound, marks.getOrDefault(lowerBound, 0.0f) + 1);
                    break;
                }
            }
        }
        stat.setMark(mapMarks);
        return stat;
    }

    @Override
    public List<StatsSubjectAnswerQuestResp> getStatisticalAnswerSubject(StatisticalQuestSearchDto search) {
        List<StatsSubjectAnswerQuestResp> statQuestAnswer = new ArrayList<>();
        List<ExamTestResultEntity> examTestResultEntities = examTestResultRepos.findAllByExamId(new ObjectId(search.getExamId()));
        Map<String, StatsSubjectAnswerQuestResp> statMap = new HashMap<>();

        int totalCorrectAnswers = 0;
        int totalIncorrectAnswers = 0;

        for (ExamTestResultEntity examTestResult : examTestResultEntities) {

            totalCorrectAnswers += examTestResult.getCorrectAnswers();
            totalIncorrectAnswers += examTestResult.getIncorrectAnswers();

            List<QuestionAnswerEntity> questionAnswerEntities = examTestResult.getQuestionAnswer();

            QuestionAnswerEntity firstQuestion = questionAnswerEntities.get(0);
            ExamQuestionEntity examQuestion = questRepos.findById(String.valueOf(firstQuestion.getQuestionId()))
                    .orElseThrow(() -> new EOException(NOT_FOUND.getCode(), MessageCodes.ENTITY_NOT_FOUND, firstQuestion.getQuestionId().toString()));

            ObjectId subjectId = new ObjectId(search.getSubjectId());
            if (examQuestion.getSubject().equals(subjectId)) {
                StatsSubjectAnswerQuestResp stat = statMap.get(search.getSubjectId());

                if (Objects.isNull(stat)) {
                    stat = new StatsSubjectAnswerQuestResp();
                    stat.setSubjectId(search.getSubjectId());
                    ExamSubjectEntity subject = subjectRepos.findById(search.getSubjectId())
                            .orElseThrow(() -> new EOException(NOT_FOUND.getCode(), MessageCodes.ENTITY_NOT_FOUND, search.getSubjectId()));
                    stat.setSubjectName(subject.getSubjectName());
                    statMap.put(search.getSubjectId(), stat);
                    statQuestAnswer.add(stat);
                }
                stat.setTotalCorrect(totalCorrectAnswers);
                stat.setTotalWrong(totalIncorrectAnswers);
            } else {
                throw new EOException(CONFLICT_VALUE, INVALID_VALUE, subjectId.toString());
            }
        }
        return statQuestAnswer;
    }
}
