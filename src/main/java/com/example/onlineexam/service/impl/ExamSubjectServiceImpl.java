package com.example.onlineexam.service.impl;

import com.example.onlineexam.constant.ErrorCodes;
import com.example.onlineexam.constant.MessageCodes;
import com.example.onlineexam.dto.request.ExamSubjectRequestDto;
import com.example.onlineexam.dto.request.searchDto.ExamSubjectSearchDto;
import com.example.onlineexam.dto.response.ExamSubjectResponseDto;
import com.example.onlineexam.entity.ExamSubjectEntity;
import com.example.onlineexam.exception.EOException;
import com.example.onlineexam.repository.ExamSubjectRepository;
import com.example.onlineexam.service.ExamSubjectService;
import com.example.onlineexam.service.mapper.ExamSubjectResponseMapper;
import com.example.onlineexam.utils.PageUtils;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.data.domain.*;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ExamSubjectServiceImpl implements ExamSubjectService {

    private final ExamSubjectRepository subjectRepos;
    private final ExamSubjectResponseMapper subjectResponseMapper;
    private final MongoTemplate template;

    @Override
    public ExamSubjectResponseDto getById(String id) {
        ExamSubjectEntity entity = subjectRepos.findById(id)
                .orElseThrow(() -> new EOException(ErrorCodes.ENTITY_NOT_FOUND, MessageCodes.ENTITY_NOT_FOUND, id));
        return subjectResponseMapper.entityToResponse(entity);
    }

    @Override
    public ExamSubjectResponseDto createBy(ExamSubjectRequestDto dto) {
        ExamSubjectEntity entity = subjectResponseMapper.requestToEntity(dto);
        subjectRepos.save(entity);
        return subjectResponseMapper.entityToResponse(entity);
    }

    @Override
    public ExamSubjectResponseDto updateBy(String id, ExamSubjectRequestDto dto) {
        ExamSubjectEntity entity = subjectRepos.findById(id).
                orElseThrow(() -> new EOException(ErrorCodes.ENTITY_NOT_FOUND, MessageCodes.ENTITY_NOT_FOUND, id));
        setValue(entity, dto);
        subjectRepos.save(entity);
        return subjectResponseMapper.entityToResponse(entity);
    }

    public void setValue(ExamSubjectEntity entity, ExamSubjectRequestDto dto)
    {
        entity.setSubjectName(dto.getSubjectName());
        entity.setQuestions(subjectResponseMapper.requestToEntity(dto).getQuestions());
    }

//    ExamSubjectEntity requestToEntityMapper (ExamSubjectRequestDto dto) {
//        ExamSubjectEntity entity = subjectResponseMapper.requestToEntity(dto);
//        List<ObjectId> question = new ArrayList<>();
//        for (String id : dto.getQuestions()) {
//            question.add(new ObjectId(id));
//        }
//        entity.setQuestions(question);
//        return entity;
//    }

//    ExamSubjectResponseDto entityToResponseDtoMapper(ExamSubjectEntity entity) {
//        ExamSubjectResponseDto examSubjectResponseDto = subjectResponseMapper.entityToResponse(entity);
//
//        List<ExamQuestionResponseDto> examQuestionResponseDtos = new ArrayList<>();
//
//        for (ObjectId id : entity.getQuestions()) {
//            ExamQuestionEntity question = examQuestionRepository.findById(id.toString())
//                    .orElseThrow(() -> new EOException(NOT_FOUND.getCode(), ENTITY_NOT_FOUND));
//            examQuestionResponseDtos.add(subjectResponseMapper.entityToResponse(question));
//        }
//        examSubjectResponseDto.setQuestions(examQuestionResponseDtos);
//        return examSubjectResponseDto;
//    }

    @Override
    public Page<ExamSubjectResponseDto> searchBy(@NonNull ExamSubjectSearchDto searchDto) {

        Query query = new Query();
        Pageable pageable = PageUtils.getPageable(searchDto.getPageIndex(), searchDto.getPageSize());
        if (StringUtils.hasText(searchDto.getKeyword())) {
            query.addCriteria(Criteria.where("").is(searchDto.getKeyword()));
        }
        query.with(Sort.by(Sort.Order.desc("startDate")));
        query.with(pageable);
        List<ExamSubjectEntity> entityList = template.find(query, ExamSubjectEntity.class);
        long count = entityList.size();
        List<ExamSubjectResponseDto> responseDtos = entityList.stream().map(subjectResponseMapper::entityToResponse).collect(Collectors.toList());
        return new PageImpl<>(responseDtos, pageable, count);
    }

    @Override
    public void deleteBy(String id) {
        subjectRepos.findById(id).orElseThrow(() -> new EOException(ErrorCodes.ENTITY_NOT_FOUND, MessageCodes.ENTITY_NOT_FOUND, id));
        subjectRepos.deleteById(id);
    }
}
