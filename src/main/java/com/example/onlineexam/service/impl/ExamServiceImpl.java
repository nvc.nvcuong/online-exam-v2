package com.example.onlineexam.service.impl;

import com.example.onlineexam.constant.ErrorCodes;
import com.example.onlineexam.constant.MessageCodes;
import com.example.onlineexam.constant.enums.STATUS;
import com.example.onlineexam.dto.request.ExamRequestDto;
import com.example.onlineexam.dto.request.searchDto.ExamSearchDto;
import com.example.onlineexam.dto.response.ExamResponseDto;
import com.example.onlineexam.dto.response.ExamSessionResponseDto;
import com.example.onlineexam.entity.ExamEntity;
import com.example.onlineexam.entity.ExamRoomEntity;
import com.example.onlineexam.entity.ExamSessionEntity;
import com.example.onlineexam.entity.ExamSubjectEntity;
import com.example.onlineexam.exception.EOException;
import com.example.onlineexam.exception.EntityNotFoundException;
import com.example.onlineexam.repository.ExamRepository;
import com.example.onlineexam.repository.ExamRoomRepository;
import com.example.onlineexam.repository.ExamSessionRepository;
import com.example.onlineexam.repository.ExamSubjectRepository;
import com.example.onlineexam.service.ExamService;
import com.example.onlineexam.service.mapper.ExamResponseMapper;
import com.example.onlineexam.service.mapper.ExamRoomResponseMapper;
import com.example.onlineexam.service.mapper.ExamSessionResponseMapper;
import com.example.onlineexam.service.mapper.ExamSubjectResponseMapper;
import com.example.onlineexam.utils.PageUtils;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ExamServiceImpl implements ExamService {


    private final ExamResponseMapper examResponseMapper;
    private final ExamRepository examRepo;
    public final MongoTemplate mongoTemplate;
    private final ExamSessionRepository sessionRepos;
    private final ExamSessionResponseMapper sessionMapper;
    private final ExamSubjectRepository subjectRepos;
    private final ExamSubjectResponseMapper subjectMapper;
    private final ExamRoomRepository roomRepos;
    private final ExamRoomResponseMapper roomMapper;

    @Override
    public ExamResponseDto getById(@NonNull UUID id) {
        ExamEntity entity = examRepo.findById(id).
                orElseThrow(() -> new EOException(ErrorCodes.ENTITY_NOT_FOUND, MessageCodes.ENTITY_NOT_FOUND, String.valueOf(id)));
        return entityToResponseMapper(entity);
    }

    @Override
    public ExamResponseDto createBy(ExamRequestDto dto) {
        ExamEntity entity = new ExamEntity();
        this.setValue(entity, dto);
        examRepo.save(entity);
        return entityToResponseMapper(entity);
    }

    @Override
    public ExamResponseDto updateBy(@NonNull UUID id, @NonNull ExamRequestDto dto) {
        ExamEntity exam = examRepo.findById(id).
                orElseThrow(() -> new EOException(ErrorCodes.ENTITY_NOT_FOUND, MessageCodes.ENTITY_NOT_FOUND, String.valueOf(id)));
        this.setValue(exam, dto);
        examRepo.save(exam);
        return entityToResponseMapper(exam);
    }

    private List<ObjectId> mapid(List<String> dto){

        List<ObjectId> list=new ArrayList<>();
        for(String id:dto){
            list.add(new ObjectId(id));
        }
            return  list;
    }

    private void setValue(ExamEntity entity, ExamRequestDto dto) {
        entity.setExamName(dto.getExamName());
        entity.setStartDate(dto.getStartDate());
        entity.setEndDate(dto.getEndDate());
        if(dto.getExamTest()!=null)
//            entity.setExamTest(mapid(dto.getExamTest()));
        if(dto.getExaminersId()!=null)
//            entity.setExaminersId(mapid(dto.getExaminersId()));
        updateStatus(entity);
    }

    public void updateStatus(ExamEntity entity) {
        Date currentDate = removeTime(new Date());
        Date startDate = removeTime(entity.getStartDate());
        Date endDate = removeTime(entity.getEndDate());

        if (currentDate.before(startDate)) {
            entity.setStatus(STATUS.CHUA_THI);
        } else if (currentDate.after(endDate)) {
            entity.setStatus(STATUS.DA_THI);
        } else {
            entity.setStatus(STATUS.DANG_THI);
        }
    }

    public Date removeTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public ExamResponseDto entityToResponseMapper(ExamEntity entity) {
        ExamResponseDto examResponseDto = examResponseMapper.entityToResponse(entity);
        List<ExamSessionResponseDto> sessions = new ArrayList<>();

        if (Objects.nonNull(entity)) {
            List<ExamSessionEntity> entities = sessionRepos.findByExamId(entity.getId());
            for (ExamSessionEntity session : entities) {
                ExamSessionResponseDto dto = sessionMapper.entityToResponse(session);

                ExamSubjectEntity subject = subjectRepos.findById(session.getSubjectId())
                        .orElseThrow(() -> new EntityNotFoundException(ExamSubjectEntity.class.getName(), "id", session.getSubjectId()));
                if (Objects.nonNull(subject)) {
                    dto.setSubject(subjectMapper.entityToResponse(subject));
                }
                List<ExamRoomEntity> rooms = roomRepos.findAllBySessionId(session.getId());
                if (Objects.nonNull(rooms)) {
                    dto.setRoom(rooms.stream().map(roomMapper::entityToResponseDto).collect(Collectors.toList()));
                }
            }
            examResponseDto.setSessions(sessions);
        }
        return examResponseDto;
    }

    @Override
    public Page<ExamResponseDto> searchBy(@NonNull ExamSearchDto searchDto) {

        Pageable pageable = PageUtils.getPageable(searchDto.getPageIndex(), searchDto.getPageSize());

        Query query = new Query();
        if (StringUtils.hasText(searchDto.getKeyword())) {
            query.addCriteria(Criteria.where("examName").is(searchDto.getKeyword()));
        }
        query.with(Sort.by(Sort.Order.desc("startDate")));
        query.with(pageable);

        List<ExamEntity> entityList = mongoTemplate.find(query, ExamEntity.class);
        long count = entityList.size();
        List<ExamResponseDto> responseDtos =entityList.stream().map(examResponseMapper::entityToResponse).collect(Collectors.toList());

        return new PageImpl<>(responseDtos, pageable, count);
    }

    @Override
    public void deleteBy(UUID id) {
        validByDelete(id);
        examRepo.deleteById(id);
    }

    @Override
    public void deleteByExamier(UUID id,UUID idExamier) {
        ExamEntity exam= examRepo.findById(id).get();
//       exam.getExaminersId().removeIf(element->element.equals(idExamier));
       examRepo.save(exam);
    }

    public void validByDelete(UUID id) {
        if (Objects.nonNull(id)) {
            examRepo.findById(id).
                    orElseThrow(() -> new EOException(ErrorCodes.ENTITY_NOT_FOUND, MessageCodes.ENTITY_NOT_FOUND, String.valueOf(id)));
        }
    }

}
