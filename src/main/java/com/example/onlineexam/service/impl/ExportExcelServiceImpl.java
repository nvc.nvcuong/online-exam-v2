package com.example.onlineexam.service.impl;

import com.example.onlineexam.dto.request.searchDto.ExamTestResultSerchDto;
import com.example.onlineexam.dto.response.ExamTestResultResponseDto;
import com.example.onlineexam.service.ExamTestResultService;
import com.example.onlineexam.service.ExportExcelService;
import com.example.onlineexam.utils.ExportExcelUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ExportExcelServiceImpl implements ExportExcelService {

    private final ExamTestResultService resultService;

    @Value("${config.templates}")
    private String attachmentPath;

    @Override
    public ByteArrayResource exportDiemThi(ExamTestResultSerchDto searchDto) throws IOException {
        List<ExamTestResultResponseDto> resultDtos = resultService.searchBy(searchDto).getContent();
        return new ByteArrayResource(ExportExcelUtil.exportToExcel(resultDtos, attachmentPath + "DANH_SACH_DIEM_THI.xlsx", 2, 0));
    }
}
