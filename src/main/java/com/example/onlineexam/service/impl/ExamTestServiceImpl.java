package com.example.onlineexam.service.impl;

import com.example.onlineexam.constant.MessageCodes;
import com.example.onlineexam.constant.enums.ExamTestStatus;
import com.example.onlineexam.dto.request.ExamTestAutoGenRequestDto;
import com.example.onlineexam.dto.request.ExamTestRequestDto;
import com.example.onlineexam.dto.request.searchDto.ExamTestSearchDto;
import com.example.onlineexam.dto.response.*;
import com.example.onlineexam.entity.*;
import com.example.onlineexam.exception.EOException;
import com.example.onlineexam.exception.EntityNotFoundException;
import com.example.onlineexam.repository.*;
import com.example.onlineexam.service.ExamQuestionService;
import com.example.onlineexam.service.ExamTestService;
import com.example.onlineexam.service.mapper.*;

import com.example.onlineexam.utils.PageUtils;
import com.example.onlineexam.utils.RenderCodeTest;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import org.bson.types.ObjectId;
import org.springframework.data.domain.*;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

import static com.example.onlineexam.constant.ErrorCodes.CONFLICT_VALUE;
import static com.example.onlineexam.constant.ErrorCodes.ENTITY_NOT_FOUND;
import static com.example.onlineexam.constant.MessageCodes.*;
import static com.example.onlineexam.constant.enums.ExamTestEnum.DIFFICULTY.*;

@Service
@RequiredArgsConstructor
public class ExamTestServiceImpl implements ExamTestService {

    private final ExamTestResponseMapper examTestMapper;
    private final ExamResponseMapper examMapper;
    private final ExamSubjectResponseMapper subjectMapper;
    private final ExamTestRepository examTestRepos;
    private final MongoTemplate mongoTemplate;
    private final ExamSubjectRepository subjectRepos;
    private final ExamRepository examRepository;
    private final ExaminerRepository examinerRepos;
    private final ExamQuestionResponseMapper questionMapper;
    private final ExaminerResponseMapper examinerService;
    private final ExamSessionServiceImpl sessionService;
    private final EQuestionRepository eQuestionRepos;
    private final ExamSessionRepository sessionRepos;
    private final ExamRepository examRepo;
    private final ExamQuestionRepository questionRepos;

    private final ExamSessionResponseMapper sessionResponseMapper;
    private final ExamSubjectResponseMapper examSubjectResponseMapper;
    private final ExaminerResponseMapper examinerMapper;
    private final UserMapper userMapper;
    private final UserRepository userRepository;
    private final ExamQuestionService questionService;
    private final ExamQuestionServiceImpl examQuestionService;

    public static List<ObjectId> questionExampleTest;

    @Override
    public ExamTestResponseDto getDtoById(String id) {
        ExamTestEntity entity = examTestRepos.findById(id)
                .orElseThrow(() -> new EOException(ENTITY_NOT_FOUND, MessageCodes.ENTITY_NOT_FOUND, id));
        return entityToResponseDtoMapper(entity);
    }

    @Override
    public List<ExamTestResponseDto> getByExam(String id) {
        List<ExamTestEntity> tests = examTestRepos.findByExam(new ObjectId(id));
        return tests.stream()
                .map(this::entityToResponseMapperExam)
                .collect(Collectors.toList());
    }

    @Override
    public Page<ExamTestResponseDto> searchBy(ExamTestSearchDto dto) {

        Query query = new Query();
        Pageable pageable = PageUtils.getPageable(dto.getPageIndex(), dto.getPageSize());
        this.genWhereClause(dto, query);
        query.with(Sort.by(Sort.Order.desc("startDate")));
        query.with(pageable);
        List<ExamTestEntity> entityList = mongoTemplate.find(query, ExamTestEntity.class);
        long count = entityList.size();
        List<ExamTestResponseDto> responseDtos = entityList.stream().map(this::entityToResponseMapperExam).collect(Collectors.toList());
        return new PageImpl<>(responseDtos, pageable, count);
    }

    private void genWhereClause(ExamTestSearchDto dto, Query query) {
        List<Criteria> criteriaList = new ArrayList<>();

        if (StringUtils.hasText(dto.getKeyword())) {
            criteriaList.add(Criteria.where("testName").is(dto.getKeyword()));
        }
        if (dto.getStatus() != 0) {
            criteriaList.add(Criteria.where("status").is(dto.getStatus()));
        }
        if (Objects.nonNull(dto.getLevel())) {
            criteriaList.add(Criteria.where("level").is(dto.getLevel()));
        }
        if (StringUtils.hasText(dto.getExamId())) {
            criteriaList.add(Criteria.where("exam").is(new ObjectId(dto.getExamId())));
        }
        if (StringUtils.hasText(dto.getSessionId())) {
            criteriaList.add(Criteria.where("session").is(new ObjectId(dto.getSessionId())));
        }
        if (Objects.nonNull(dto.getType())) {
            criteriaList.add(Criteria.where("type").is(dto.getType()));
        }


        Criteria finalCriteria = new Criteria().andOperator(criteriaList.toArray(new Criteria[0]));
        query.addCriteria(finalCriteria);
    }

    @Override
    public Page<ExanTestExaminer> showTested(ExamTestSearchDto dto) {
        dto.setPageIndexAndPageSize(dto.getPageIndex(), dto.getPageSize());
        Query query = new Query();
        Pageable pageable = PageUtils.getPageable(dto.getPageIndex(), dto.getPageSize());
        this.genWhereClause(dto, query);
        query.with(Sort.by(Sort.Order.desc("startDate")));
        query.with(pageable);
        List<ExamTestEntity> entityList = mongoTemplate.find(query, ExamTestEntity.class);
        List<ExanTestExaminer> responseDtos = new ArrayList<>();
        long count = entityList.size();
        this.mapTestExamnier(responseDtos, entityList);
        return new PageImpl<>(responseDtos, pageable, count);
    }

    private void mapTestExamnier(List<ExanTestExaminer> responseDtos, List<ExamTestEntity> entity) {
        if (!CollectionUtils.isEmpty(entity)) {
            for (ExamTestEntity examTestEntity : entity) {
                ExanTestExaminer exanTestExaminer = new ExanTestExaminer();
                if (examTestEntity.getTestName() != null) {
                    exanTestExaminer.setNameTest(examTestEntity.getTestName());
                }
                if (examTestEntity.getId() != null) {
                    exanTestExaminer.setId(examTestEntity.getId());
                }
                if (examTestEntity.getSession() != null) {
                    exanTestExaminer.setExamSession(sessionResponseMapper.entityToResponse(
                            sessionRepos.findById(examTestEntity.getSession().toString()).orElseThrow(
                                    () -> new EntityNotFoundException(ExamSessionEntity.class.getName(), "id", examTestEntity.getSession())
                            )
                    ));
                }
                if (examTestEntity.getSubject() != null) {
                    exanTestExaminer.setSubject(examSubjectResponseMapper.entityToResponse(
                            subjectRepos.findById(examTestEntity.getSubject().toString()).orElseThrow(
                                    () -> new EntityNotFoundException(ExamSubjectEntity.class.getName(), "id", examTestEntity.getSubject())
                            )
                    ));
                }
                if (examTestEntity.getExaminer() != null) {
                    ExaminerUserResponseDto examinerUserResponseDto = new ExaminerUserResponseDto();
                    ExaminerResponseDto examinerResponseDto = examinerMapper.entityToResponse(
                            examinerRepos.findById(examTestEntity.getExaminer().toString()).orElseThrow(
                                    () -> new EntityNotFoundException(ExaminerEntity.class.getName(), "id", examTestEntity.getExaminer())
                            )
                    );
                    examinerUserResponseDto.setExaminer(examinerResponseDto);
                    examinerUserResponseDto.setUser(userMapper.entityToDto(
                            userRepository.findById(examinerResponseDto.getUserId()).orElseThrow(
                                    () -> new EntityNotFoundException(User.class.getName(), "id", examinerResponseDto.getUserId())
                            )
                    ));
                    exanTestExaminer.setExaminerUser(examinerUserResponseDto);
                }
                responseDtos.add(exanTestExaminer);
            }
        }
    }

    @Override
    public ExamTestResponseDto createBy(@NonNull ExamTestRequestDto dto) {
        ExamTestEntity entity = this.requestToEntityMapper(dto);
        entity.setType(ExamTestStatus.TEST_TYPE.DE_MAU);
        entity.setTestCode(RenderCodeTest.setValue());
        entity.setStatus(ExamTestStatus.TEST_STATUS.CHUA_DUYET);
        examTestRepos.save(entity);
        setExam(dto.getExam().getId(), entity.getId());
        return entityToResponseDtoMapper(entity);
    }

    @Override
    public ExamTestResponseDto updateBy(@NonNull String id, @NonNull ExamTestRequestDto dto) {
        ExamTestEntity entity = examTestRepos.findById(id)
                .orElseThrow(() -> new EOException(ENTITY_NOT_FOUND, MessageCodes.ENTITY_NOT_FOUND, id));
        entity = this.requestToEntityMapper(dto);
        entity.setId(new ObjectId(id));
        examTestRepos.save(entity);
        setExam(dto.getExam().getId(), entity.getId());
        return entityToResponseDtoMapper(entity);
    }

    @Override
    public ExamTestResponseDto approveExamTest(@NonNull String id) {
        ExamTestEntity entity = examTestRepos.findById(id)
                .orElseThrow(() -> new EOException(ENTITY_NOT_FOUND, MessageCodes.ENTITY_NOT_FOUND, id));
        entity.setStatus(ExamTestStatus.TEST_STATUS.DA_DUYET);
        examTestRepos.save(entity);
        return entityToResponseDtoMapper(entity);
    }

    @Override
    public Boolean deleteBy(@NonNull String id) {
        examTestRepos.deleteById(id);
        return true;
    }

    @Override
    public String createByQuestion(@NonNull ExamTestAutoGenRequestDto dto) {
        int easy = questionRepos.findByLevelAndSubject(EASY.getCode(),new ObjectId(dto.getExamTest().getSubject().getId())).size();
        int medium= questionRepos.findByLevelAndSubject(MEDIUM.getCode(),new ObjectId(dto.getExamTest().getSubject().getId())).size();
        int hard= questionRepos.findByLevelAndSubject(HARD.getCode(),new ObjectId(dto.getExamTest().getSubject().getId())).size();
        int veryHard= questionRepos.findByLevelAndSubject(VERY_HARD.getCode(),new ObjectId(dto.getExamTest().getSubject().getId())).size();
        checkTotalQuestionInData(easy,medium,hard,veryHard, dto);
        this.validNumberQuestionInTest(dto);
        ExamTestEntity entity = this.requestToEntityMapper(dto.getExamTest());
        List<ExamQuestionEntity> examQuestionEntityList=examQuestionService.getAllExamQuestion();
        List<ObjectId> questions = eQuestionRepos.getRandomQuestions(examQuestionEntityList,dto.getNumberEasy(), EASY.getCode(),
                dto.getExamId(), dto.getExamTest().getSubject().getId());
        List<ObjectId> questionMedium = eQuestionRepos.getRandomQuestions(examQuestionEntityList,dto.getNumberMedium(), MEDIUM.getCode()
                , dto.getExamId(), dto.getExamTest().getSubject().getId());
        questions.addAll(questionMedium);
        List<ObjectId> questionHard = eQuestionRepos.getRandomQuestions(examQuestionEntityList,dto.getNumberHard(), HARD.getCode()
                , dto.getExamId(), dto.getExamTest().getSubject().getId());
        questions.addAll(questionHard);
        List<ObjectId> questionVeryHard = eQuestionRepos.getRandomQuestions(examQuestionEntityList,dto.getNumberVeryHard(), VERY_HARD.getCode()
                , dto.getExamId(), dto.getExamTest().getSubject().getId());
        questions.addAll(questionVeryHard);
        entity.setQuestion(questions);
        entity.setExam(new ObjectId(dto.getExamId()));
        entity.setType(ExamTestStatus.TEST_TYPE.DE_MAU);
        entity.setStatus(ExamTestStatus.TEST_STATUS.CHUA_DUYET);
        entity.setTestCode(RenderCodeTest.setValue());
        examTestRepos.save(entity);
        setExam(dto.getExamId(), entity.getId());
        return PROCESSED_SUCCESSFULLY;
    }

    private void setExam(String examId, String examTestId) {
        ExamEntity examEntity = examRepository.findById(examId).orElseThrow(
                () -> new EntityNotFoundException(ExamEntity.class.getName(), "Id", examId)
        );
        List<ObjectId> list = new ArrayList<>();
        if (examEntity.getExamTest() != null) {
            list = examEntity.getExamTest();
        }
        ObjectId objectId = new ObjectId(examTestId);
        list.add(objectId);
        examEntity.setExamTest(list);
        examRepository.save(examEntity);
    }

    private void checkTotalQuestionInData(int easy, int medium, int hard, int veryHard, ExamTestAutoGenRequestDto dto) {
        if (easy < dto.getNumberEasy()) {
            throw new EOException(CONFLICT_VALUE, NUMBER_QUESTION_NOT_ENOUGH, dto.getNumberEasy() + " Easy");
        }
        if (medium < dto.getNumberMedium()) {
            throw new EOException(CONFLICT_VALUE, NUMBER_QUESTION_NOT_ENOUGH, dto.getNumberMedium() + " Medium");
        }
        if (hard < dto.getNumberHard()) {
            throw new EOException(CONFLICT_VALUE, NUMBER_QUESTION_NOT_ENOUGH, dto.getNumberHard() + " Hard");
        }
        if (veryHard < dto.getNumberVeryHard()) {
            throw new EOException(CONFLICT_VALUE, NUMBER_QUESTION_NOT_ENOUGH, dto.getNumberVeryHard() + " VeryHard");
        }
    }

    private void validNumberQuestionInTest(ExamTestAutoGenRequestDto dto) {
        int totalQ = dto.getNumberEasy() + dto.getNumberHard() + dto.getNumberMedium() + dto.getNumberVeryHard();
        if (dto.getNumberQuestion() != null && dto.getNumberQuestion() != totalQ) {
            throw new EOException(CONFLICT_VALUE, NUMBER_QUESTION_IS_NULL_OR_BIGER_TOTAL_QUESTION, dto.getNumberQuestion().toString());
        }
    }

    @Override
    public String createExamByNumbers(ExamTestAutoGenRequestDto dto) {
        List<ExamTestResponseDto> results = new ArrayList<>();
        for (int i = 0; i < dto.getNumberTest(); i++) {
         this.createByQuestion(dto);
        }
       return PROCESSED_SUCCESSFULLY;
    }

    private ExamQuestionEntity getLeveObjectId(ObjectId id) {
        return questionRepos.findById(id.toString()).orElseThrow(() -> new EOException(ENTITY_NOT_FOUND, MessageCodes.ENTITY_NOT_FOUND, id.toString()));
    }

    private void addValueToMap(Map<Integer, Set<ExamQuestionEntity>> map, int key, ExamQuestionEntity value) {
        if (!map.containsKey(key)) {
            map.put(key, new HashSet<>());
        }
        map.get(key).add(value);
    }

    @Override
    public String createByExaminers(@NonNull ExamTestAutoGenRequestDto dto) {
        this.validNumberQuestionInTest(dto);
        ExamEntity exam = examRepo.findById(dto.getExamId())
                .orElseThrow(() -> new EOException(ENTITY_NOT_FOUND, MessageCodes.ENTITY_NOT_FOUND, dto.getExamId()));
        List<ObjectId> examExaminersId = exam.getExaminersId();

        List<ExamTestEntity> examTestEntities = exam.getExamTest()
                .stream()
                .map(id -> examTestRepos.findById(id.toString()).orElse(null))
                .filter(Objects::nonNull).collect(Collectors.toList());


        Set<ObjectId> questions = new HashSet<>();
        Map<Integer, Set<ExamQuestionEntity>> listMap = new HashMap<>();

        examTestEntities.stream().map(test -> questions.addAll(test.getQuestion())).collect(Collectors.toList());
        for (int key = 1; key < 5; key++) {
            if (!listMap.containsKey(key)) {
                listMap.put(key, new HashSet<>());
            }
        }
        questions.forEach(id -> {
            ExamQuestionEntity questionEntity = getLeveObjectId(id);
            addValueToMap(listMap, questionEntity.getLevel(), questionEntity);

        });
        checkTotalQuestionInData(listMap.get(1).size(), listMap.get(2).size()
                , listMap.get(3).size()
                , listMap.get(4).size(), dto);

        for (ObjectId id : examExaminersId) {
            List<ExamQuestionEntity> entityList = new ArrayList<>();
            int i = 0;
            ExaminerEntity examiner = examinerRepos.findById(id.toString())
                    .orElseThrow(() -> new EOException(ENTITY_NOT_FOUND, MessageCodes.ENTITY_NOT_FOUND, id.toString()));
            ExamTestEntity entity;
            try {
                entity = examTestRepos.findByExaminerAndSession(id, examiner.getSession());
                if (Objects.nonNull(entity)) {
                    examTestRepos.deleteById(entity.getId());
                }
            } catch (Exception e) {

            }

            entity = examTestMapper.requestToEntity(dto.getExamTest());
            this.setListQuestion(listMap, dto, entity, entityList);
            entity.setExaminer(id);
            entity.setExam(new ObjectId(dto.getExamId()));
            entity.setTestName(dto.getExamTest().getTestName() + i + 1);
            entity.setType(ExamTestStatus.TEST_TYPE.DE_THI);
            entity.setSession(examiner.getSession());
            entity.setTestCode(RenderCodeTest.setValue());
            entity.setSubject(new ObjectId(dto.getExamTest().getSubject().getId()));
            entity.setStatus(ExamTestStatus.TEST_STATUS.CHUA_THI);
            examTestRepos.save(entity);

        }
        return PROCESSED_SUCCESSFULLY;
    }

    private void setQuestion(List<ObjectId> listQuestion, Integer num, List<ExamQuestionEntity> list, List<ExamQuestionEntity> lists) {
        Collections.shuffle(list);
        Collections.shuffle(list);
        Collections.shuffle(list);
        list = list.subList(0, num);
        for (ExamQuestionEntity e : list) {
            listQuestion.add(new ObjectId(e.getId()));
            lists.add(e);
        }
    }

    private void setListQuestion(Map<Integer, Set<ExamQuestionEntity>> questions, ExamTestAutoGenRequestDto dto, ExamTestEntity entity, List<ExamQuestionEntity> entityList) {
        List<ObjectId> listQuestion = new ArrayList<>();
        setQuestion(listQuestion, dto.getNumberEasy(), new ArrayList<>(questions.get(EASY.getCode())), entityList);
        setQuestion(listQuestion, dto.getNumberMedium(), new ArrayList<>(questions.get(MEDIUM.getCode())), entityList);
        setQuestion(listQuestion, dto.getNumberHard(), new ArrayList<>(questions.get(HARD.getCode())), entityList);
        setQuestion(listQuestion, dto.getNumberVeryHard(), new ArrayList<>(questions.get(VERY_HARD.getCode())), entityList);
        entity.setQuestion(listQuestion);
    }

    private ExamTestResponseDto entityToReponseNew(ExamTestEntity entity, List<ExamQuestionEntity> question) {
        ExamTestResponseDto examTestResponseDto = examTestMapper.entityToResponseDto(entity);
        if (Objects.nonNull(question)) {
            List<ExamQuestionResponseDto> questionResponseDtos = new ArrayList<>();
            for (ExamQuestionEntity e : question) {
                questionResponseDtos.add(questionMapper.entityToResponse(e));
            }
            examTestResponseDto.setQuestion(questionResponseDtos);
        }
        if (entity.getExam() != null)
            examTestResponseDto.setExam(examMapper.entityToResponse(examRepository.findById(entity.getExam().toString())
                    .orElseThrow(() -> new EntityNotFoundException(ExamQuestionEntity.class.getName(), "id", entity.getExam().toString()))));
        if (entity.getSubject() != null)
            examTestResponseDto.setSubject(subjectMapper.entityToResponse(subjectRepos.findById(entity.getSubject().toString())
                    .orElseThrow(() -> new EntityNotFoundException(ExaminerEntity.class.getName(), "id", entity.getSubject().toString()))));
        if (entity.getExaminer() != null)
            examTestResponseDto.setExaminer(examinerService.entityToResponse(examinerRepos.findById(entity.getExaminer().toString())
                    .orElseThrow(() -> new EntityNotFoundException(ExaminerEntity.class.getName(), "id", entity.getExaminer().toString()))));
        if (entity.getSession() != null) {
            examTestResponseDto.setSession(sessionService.entityToResponseMapper(sessionRepos.findById(entity.getSession().toString())
                    .orElseThrow(() -> new EntityNotFoundException(ExamSessionEntity.class.getName(), "id", entity.getSession().toString()))));
        }
        return examTestResponseDto;
    }

    @Override
    public ExamTestResponseDto entityToResponseDtoMapper(ExamTestEntity entity) {
        ExamTestResponseDto examTestResponseDto = examTestMapper.entityToResponseDto(entity);

        List<ExamQuestionResponseDto> questionResponseDtos = new ArrayList<>();
        for (ObjectId id : entity.getQuestion()) {
            ExamQuestionEntity question = questionRepos.findById(id.toString())
                    .orElseThrow(() -> new EntityNotFoundException(ExamQuestionEntity.class.getName(), "id", id.toString()));
            ExamQuestionResponseDto responseDto = questionMapper.entityToResponse(question);
            List<String> answers = new ArrayList<>();
            this.mapAnswerQuestionToList(question, answers);
            responseDto.setAnswers(answers);
            questionResponseDtos.add(responseDto);
        }
        examTestResponseDto.setQuestion(questionResponseDtos);
        if (entity.getExam() != null)
            examTestResponseDto.setExam(examMapper.entityToResponse(examRepository.findById(entity.getExam().toString())
                    .orElseThrow(() -> new EntityNotFoundException(ExamQuestionEntity.class.getName(), "id", entity.getExam().toString()))));
        if (entity.getSubject() != null)
            examTestResponseDto.setSubject(subjectMapper.entityToResponse(subjectRepos.findById(entity.getSubject().toString())
                    .orElseThrow(() -> new EntityNotFoundException(ExaminerEntity.class.getName(), "id", entity.getSubject().toString()))));
        if (entity.getExaminer() != null)
            examTestResponseDto.setExaminer(examinerService.entityToResponse(examinerRepos.findById(entity.getExaminer().toString())
                    .orElseThrow(() -> new EntityNotFoundException(ExaminerEntity.class.getName(), "id", entity.getExaminer().toString()))));
        if (entity.getSession() != null) {
            examTestResponseDto.setSession(sessionService.entityToResponseMapper(sessionRepos.findById(entity.getSession().toString())
                    .orElseThrow(() -> new EntityNotFoundException(ExamSessionEntity.class.getName(), "id", entity.getSession().toString()))));
        }
        return examTestResponseDto;
    }

    private void mapAnswerQuestionToList(ExamQuestionEntity question, List<String> answers) {
        answers.add(question.getAnswer1());
        answers.add(question.getAnswer2());
        answers.add(question.getAnswer3());
        answers.add(question.getAnswer4());
        answers.add(question.getAnswer5());
        answers.add(question.getAnswer6());
        answers.add(question.getAnswer7());
        answers.add(question.getAnswer8());
        answers.add(question.getAnswer9());
        answers.add(question.getAnswer10());
    }

    public ExamTestResponseDto entityToResponseDto(ExamTestEntity entity) {
        ExamTestResponseDto examTestResponseDto = examTestMapper.entityToResponseDto(entity);

        List<ExamQuestionResponseDto> questionResponseDtos = new ArrayList<>();
        for (ObjectId id : entity.getQuestion()) {
            ExamQuestionEntity question = questionRepos.findById(id.toString())
                    .orElseThrow(() -> new EntityNotFoundException(ExamQuestionEntity.class.getName(), "id", id.toString()));
            ExamQuestionResponseDto examQuestionResponseDto = questionMapper.entityToResponse(question);
            examQuestionResponseDto.setCorrectAnswer("");
            questionResponseDtos.add(examQuestionResponseDto);
        }
        examTestResponseDto.setQuestion(questionResponseDtos);
        if (entity.getExam() != null)
            examTestResponseDto.setExam(examMapper.entityToResponse(examRepository.findById(entity.getExam().toString())
                    .orElseThrow(() -> new EntityNotFoundException(ExamQuestionEntity.class.getName(), "id", entity.getExam().toString()))));
        if (entity.getSubject() != null)
            examTestResponseDto.setSubject(subjectMapper.entityToResponse(subjectRepos.findById(entity.getSubject().toString())
                    .orElseThrow(() -> new EntityNotFoundException(ExaminerEntity.class.getName(), "id", entity.getSubject().toString()))));
        if (entity.getExaminer() != null)
            examTestResponseDto.setExaminer(examinerService.entityToResponse(examinerRepos.findById(entity.getExaminer().toString())
                    .orElseThrow(() -> new EntityNotFoundException(ExaminerEntity.class.getName(), "id", entity.getExaminer().toString()))));
        if (entity.getSession() != null) {
            examTestResponseDto.setSession(sessionService.entityToResponseMapper(sessionRepos.findById(entity.getSession().toString())
                    .orElseThrow(() -> new EntityNotFoundException(ExamSessionEntity.class.getName(), "id", entity.getSession().toString()))));
        }
        return examTestResponseDto;
    }

    public ExamTestResponseDto entityToResponseMapperExam(ExamTestEntity entity) {
        ExamTestResponseDto examTestResponseDto = examTestMapper.entityToResponseDto(entity);

        if (entity.getSession() != null) {
            examTestResponseDto.setSession(sessionService.entityToResponseMapper(sessionRepos.findById(entity.getSession().toString())
                    .orElseThrow(() -> new EntityNotFoundException(ExamSessionEntity.class.getName(), "id", entity.getSession().toString()))));
        }

        if (entity.getSubject() != null) {
            examTestResponseDto.setSubject(subjectMapper.entityToResponse(subjectRepos.findById(entity.getSubject().toString())
                    .orElseThrow(() -> new EntityNotFoundException(ExaminerEntity.class.getName(), "id", entity.getSubject().toString()))));
        }

        return examTestResponseDto;
    }

    private ExamTestEntity requestToEntityMapper(ExamTestRequestDto dto) {
        ExamTestEntity examTestEntity = examTestMapper.requestToEntity(dto);
        List<ObjectId> question = new ArrayList<>();
        if (dto.getListQuestionid() != null) {
            for (String id : dto.getListQuestionid()) {
                question.add(new ObjectId(id));
            }
            examTestEntity.setQuestion(question);
        }
        if (dto.getExam() != null)
            examTestEntity.setExam(new ObjectId(dto.getExam().getId()));
        if (dto.getSession() != null) {
            examTestEntity.setSession(new ObjectId(dto.getSession().getId()));
        }
        if (dto.getSubject() != null)
            examTestEntity.setSubject(new ObjectId(dto.getSubject().getId()));

        if (dto.getExaminer() != null)
            examTestEntity.setExaminer(new ObjectId(dto.getExaminer().getId()));
        return examTestEntity;

    }

    @Override
    public String autoGenExampleTest(@NonNull ExamTestAutoGenRequestDto dto) {

        List<ExamQuestionEntity> examQuestionEntityList = examQuestionService.getAllExamQuestion();


        ExamEntity exam = examRepo.findById(dto.getExamId())
                .orElseThrow(() -> new EOException(ENTITY_NOT_FOUND, MessageCodes.ENTITY_NOT_FOUND, dto.getExamId()));
        List<ObjectId> examExaminersId = exam.getExaminersId();

        for (ObjectId id : examExaminersId) {
            List<ObjectId> questions = new ArrayList<>();
            for (String subjectId : dto.getSubjectIds()) {
                questions.addAll(this.getRandomQuestions(examQuestionEntityList, dto.getNumberEasy(), subjectId));
            }

            int i = 0;
            ExaminerEntity examiner = examinerRepos.findById(id);
            if (Objects.isNull(examiner)){
               throw new EOException(ENTITY_NOT_FOUND, MessageCodes.ENTITY_NOT_FOUND, id.toString());
            }

            ExamTestEntity entity;
            try {
                entity = examTestRepos.findByExaminerAndSession(id, examiner.getSession());
                if (Objects.nonNull(entity)) {
                    examTestRepos.deleteById(entity.getId());
                }
            } catch (Exception e) {

            }

            entity = examTestMapper.requestToEntity(dto.getExamTest());
            entity.setQuestion(questions);
            entity.setExaminer(id);
            entity.setExam(new ObjectId(dto.getExamId()));
            entity.setTestName(dto.getExamTest().getTestName() + i + 1);
            entity.setType(ExamTestStatus.TEST_TYPE.DE_THI);
            entity.setSession(examiner.getSession());
            entity.setTestCode(RenderCodeTest.setValue());
            entity.setSubject(new ObjectId(dto.getExamTest().getSubject().getId()));
            entity.setStatus(ExamTestStatus.TEST_STATUS.CHUA_THI);
            examTestRepos.save(entity);
            setExam(dto.getExamId(), entity.getId());
        }

        return PROCESSED_SUCCESSFULLY;
    }

    private List<ObjectId> getRandomQuestions(List<ExamQuestionEntity> examQuestionEntityList, int count, String subjectId) {

        List<ExamQuestionEntity> filteredQuestions = examQuestionEntityList.stream()
                .filter(entity -> entity.getSubject().toString().equals(subjectId))
                .collect(Collectors.toList());

        if (filteredQuestions.size() < count) {
            throw new EOException(CONFLICT_VALUE, NUMBER_QUESTION_NOT_ENOUGH, String.valueOf(count));
        }

        Collections.shuffle(filteredQuestions);

        return filteredQuestions.stream()
                .limit(count)
                .map(entity -> new ObjectId(entity.getId()))
                .collect(Collectors.toList());
    }

}
