package com.example.onlineexam.service.impl;


import com.example.onlineexam.dto.RoleDto;
import com.example.onlineexam.entity.Role;
import com.example.onlineexam.entity.User;
import com.example.onlineexam.error.RoleStatus;
import com.example.onlineexam.exception.EOException;
import com.example.onlineexam.repository.RoleRepository;
import com.example.onlineexam.service.RoleService;
import com.example.onlineexam.service.mapper.RoleMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
@RequiredArgsConstructor
public class RoleServiceImpl implements RoleService {
    private final RoleRepository roleRepository;
    private final RoleMapper roleMapper;
    private final MongoOperations mongoOperations;

    public Long generateId() {
        Query query = new Query().with(Sort.by(Sort.Direction.DESC, "id")).limit(1);
        Role latestUser = mongoOperations.findOne(query, Role.class);
        return latestUser != null ? latestUser.getId() : null;
    }

    @Override
    public RoleDto save(RoleDto dto) {
        this.validateDto(dto);
        Role role = new Role();
        role.setId(generateId() + 1);
        this.mapDtoToEntity(dto, role);
        return roleMapper.entityToDto(role);
    }

    @Override
    public RoleDto update(Long id, RoleDto dto) {
        this.validateDto(dto);

        Role role = roleRepository.findById(id)
                .orElseThrow(() -> new EOException(RoleStatus.ID_NOT_EXIST, id.toString()));

        this.mapDtoToEntity(dto, role);
        role = roleRepository.save(role);
        return roleMapper.entityToDto(role);
    }

    private void mapDtoToEntity(RoleDto dto, Role entity) {
        entity.setName(dto.getName());
        entity.setDescription(dto.getDescription());
    }

    public void validateDto(RoleDto dto) {
        if (StringUtils.isEmpty(dto.getName())) {
            throw new EOException(RoleStatus.NAME_IS_EMPTY, null);
        }

        if (roleRepository.hasName((long) -1, dto.getName())) {
            throw new EOException(RoleStatus.NAME_IS_EXIST, null);
        }
    }
}
