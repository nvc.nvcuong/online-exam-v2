package com.example.onlineexam.service.impl;

import com.example.onlineexam.constant.MessageCodes;
import com.example.onlineexam.dto.UserDto;
import com.example.onlineexam.dto.request.*;
import com.example.onlineexam.dto.response.ExamQuestionImportResp;
import com.example.onlineexam.dto.response.ExaminerImportResp;
import com.example.onlineexam.entity.ExamEntity;
import com.example.onlineexam.entity.ExamRoomEntity;
import com.example.onlineexam.entity.ExamSessionEntity;
import com.example.onlineexam.entity.ExaminerEntity;
import com.example.onlineexam.exception.EOException;
import com.example.onlineexam.exception.EntityNotFoundException;
import com.example.onlineexam.repository.ExamRepository;
import com.example.onlineexam.repository.ExamRoomRepository;
import com.example.onlineexam.repository.ExamSessionRepository;
import com.example.onlineexam.repository.ExaminerRepository;
import com.example.onlineexam.service.UploadExcelService;
import com.example.onlineexam.service.UserService;
import com.example.onlineexam.utils.ImportExcelUtil;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.example.onlineexam.constant.ErrorCodes.ENTITY_NOT_FOUND;

@Service
@RequiredArgsConstructor
public class UploadExcelServiceImpl implements UploadExcelService {
    private final ImportExcelUtil importExcelUtil;
    private final ExaminerRepository examinerRepository;
    private final ExamRepository examRepos;
    private final ExaminerServiceImpl examinerService;
    private final UserService userService;
    private final ExamSessionRepository examSessionRepo;
    private final ExamRoomRepository roomRepo;
    @Override
    public ExamQuestionImportResp importQuestions(MultipartFile[] uploadFiles, String examId, String subjectId) {
        String message = "";
        ExamQuestionImportResp resultDto = new ExamQuestionImportResp();

        for (MultipartFile file : uploadFiles)
            try {
                ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(file.getBytes());
                Workbook workbook = null;
                Sheet sheet = null;
                if (Objects.requireNonNull(FilenameUtils.getExtension(file.getOriginalFilename())).equalsIgnoreCase("xls")) {
                    workbook = new HSSFWorkbook(byteArrayInputStream);
                    sheet = workbook.getSheetAt(0);
                } else if (FilenameUtils.getExtension(file.getOriginalFilename()).equalsIgnoreCase("xlsx")) {
                    workbook = new XSSFWorkbook(byteArrayInputStream);
                    sheet = workbook.getSheetAt(0);
                } else {
                    message = "Vui long upload file excel .xls hoăc .xlsx.";
                }

                if (sheet != null) {

                    resultDto = importExcelUtil.importQuestion(sheet, examId, subjectId);
                }
                if (workbook != null) {
                    workbook.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        if (StringUtils.hasText(message)) {
            resultDto.setStatus(message);
        }
        return resultDto;
    }

    @Override
    public ExaminerImportResp importExaminer(MultipartFile[] uploadFiles, String examId) {
        String message = "";
        ExaminerImportResp resultDto = new ExaminerImportResp();

        ExamEntity exam = examRepos.findById(examId)
                .orElseThrow(() -> new EOException(ENTITY_NOT_FOUND, MessageCodes.ENTITY_NOT_FOUND, examId));

        for (MultipartFile file : uploadFiles)
            try {
                ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(file.getBytes());
                Workbook workbook = null;
                Sheet sheet = null;
                if (Objects.requireNonNull(FilenameUtils.getExtension(file.getOriginalFilename())).equalsIgnoreCase("xls")) {
                    workbook = new HSSFWorkbook(byteArrayInputStream);
                    sheet = workbook.getSheetAt(0);
                } else if (FilenameUtils.getExtension(file.getOriginalFilename()).equalsIgnoreCase("xlsx")) {
                    workbook = new XSSFWorkbook(byteArrayInputStream);
                    sheet = workbook.getSheetAt(0);
                } else {
                    message = "Vui long upload file excel .xls hoăc .xlsx.";
                }

                if (sheet != null) {

                    resultDto = importExcelUtil.importExaminer(sheet, examId);
                }
                if (workbook != null) {
                    workbook.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        if (StringUtils.hasText(message)) {
            resultDto.setStatus(message);
        }
        return resultDto;
    }

    @Override
    public boolean assignRoomForExaminees(String examId) {
        List<ExaminerEntity> examinees = examinerRepository.findByExam(new ObjectId(examId));
        List<ExamSessionEntity> sessionEntity = examSessionRepo.findByExamId(new ObjectId(examId));

        int totalRoomSlots = sessionEntity.stream()
                .mapToInt(session -> session.getExamRoom().stream()
                        .mapToInt(roomId -> {
                            ExamRoomEntity room = roomRepo.findById(roomId.toString())
                                    .orElseThrow(() -> new EntityNotFoundException(ExamRoomEntity.class.getName(), "id", roomId.toString()));
                            return room.getSlots();
                        })
                        .sum())
                .sum();

        if (totalRoomSlots < examinees.size()) {
            return false;
        }
        for (ExamSessionEntity session : sessionEntity) {

            for (ObjectId roomId : session.getExamRoom()) {
                ExamRoomEntity room = roomRepo.findById(roomId.toString())
                        .orElseThrow(() -> new EntityNotFoundException(ExamRoomEntity.class.getName(), "id", roomId));
                if (room.getSlots() > 0) {
                    List<ExaminerEntity> unassignedExaminees = examinees.stream()
                            .filter(examinee -> examinee.getRoom() == null)
                            .collect(Collectors.toList());
                    for (ExaminerEntity examinee : unassignedExaminees) {
                        examinee.setRoom(new ObjectId(room.getId()));
                        examinee.setSession(new ObjectId(session.getId()));
                        room.setSlots(room.getSlots() - 1);
                        if (room.getSlots() == 0) {
                            break;
                        }
                    }
                }
            }
        }
        examinerRepository.saveAll(examinees);
        return true;
    }

    @Override
    public ExaminerImportResp importAccountExaminer(MultipartFile[] uploadFiles, String examId) {
        String message = "";
        ExaminerImportResp resultDto = new ExaminerImportResp();

        for (MultipartFile file : uploadFiles)
            try {
                ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(file.getBytes());
                Workbook workbook = null;
                Sheet sheet = null;
                if (Objects.requireNonNull(FilenameUtils.getExtension(file.getOriginalFilename())).equalsIgnoreCase("xls")) {
                    workbook = new HSSFWorkbook(byteArrayInputStream);
                    sheet = workbook.getSheetAt(0);
                } else if (FilenameUtils.getExtension(file.getOriginalFilename()).equalsIgnoreCase("xlsx")) {
                    workbook = new XSSFWorkbook(byteArrayInputStream);
                    sheet = workbook.getSheetAt(0);
                } else {
                    message = "Vui long upload file excel .xls hoăc .xlsx.";
                }

                if (sheet != null) {

                    resultDto = importExcelUtil.importAccountExaminer(sheet, examId);
                }
                if (workbook != null) {
                    workbook.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        if (StringUtils.hasText(message)) {
            resultDto.setStatus(message);
        }
        return resultDto;
    }

    @Override
    public ExaminerImportResp importAccountExaminer(List<ExaminerRequestDto> dto) {
        ExaminerImportResp responseDtos=new ExaminerImportResp();
        for(ExaminerRequestDto examinerRequestDto:dto){
         ExaminerUserRequestDto examinerUserResponseDto=new ExaminerUserRequestDto();
         ExaminerEntity entity=examinerService.requestToEntityMapper(examinerRequestDto);
            examinerRepository.save(entity);
            UserDto userDto=new UserDto();
            importExcelUtil.setValueUser(userDto, entity);
            examinerUserResponseDto.setExaminerId(entity.getId());
            examinerUserResponseDto.setUser(userDto);
            userService.createAccountExaminer(examinerUserResponseDto);
        }
        responseDtos.setStatus("Xu ly thanh cong");
        responseDtos.setTotalPerson(dto.size());
        return responseDtos;
    }

}
