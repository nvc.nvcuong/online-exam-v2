package com.example.onlineexam.service.impl;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.example.onlineexam.config.JwtTokenBlacklist;
import com.example.onlineexam.constant.ErrorCodes;
import com.example.onlineexam.constant.MessageCodes;
import com.example.onlineexam.constant.Variables;
import com.example.onlineexam.constant.enums.ExaminerUserEnum;
import com.example.onlineexam.constant.enums.STATUS;
import com.example.onlineexam.dto.RoleDto;
import com.example.onlineexam.dto.TokenDto;
import com.example.onlineexam.dto.UserDto;
import com.example.onlineexam.dto.request.ExaminerUserRequestDto;
import com.example.onlineexam.dto.request.searchDto.ExamTestSearchDto;
import com.example.onlineexam.dto.request.searchDto.SearchUserDto;
import com.example.onlineexam.dto.response.*;
import com.example.onlineexam.entity.*;
import com.example.onlineexam.error.CommonStatus;
import com.example.onlineexam.error.UserStatus;
import com.example.onlineexam.exception.EOException;
import com.example.onlineexam.exception.EntityNotFoundException;
import com.example.onlineexam.repository.*;
import com.example.onlineexam.service.ExaminerService;
import com.example.onlineexam.service.UserService;
import com.example.onlineexam.service.mapper.*;
import com.example.onlineexam.utils.EbsSecurityUtils;
import com.example.onlineexam.utils.EbsTokenUtils;
import com.example.onlineexam.utils.PageUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.bson.types.ObjectId;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.LookupOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.ProviderNotFoundException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
import static com.example.onlineexam.constant.ErrorCodes.INVALID_STATUS;
import static com.example.onlineexam.constant.MessageCodes.ENTITY_NOT_FOUND;
import static com.example.onlineexam.constant.enums.ExaminerUserEnum.USER_STATUS.CHUA_THI;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserServiceImpl implements UserService, UserDetailsService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final UserMapper userMapper;
    private final JwtTokenBlacklist tokenBlacklist;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final ExaminerRepository examinerRepos;
    private final ExaminerResponseMapper examinerMapper;
    private final ExamTestRepository testRepos;
    private final ExamTestServiceImpl examTestService;
    private final MongoTemplate mongoTemplate;
    private final ExamRepository examRepository;
    private final MongoOperations mongoOperations;
    private final ExamQuestionServiceImpl examQuestionService;
    private final ExamQuestionResponseMapper questionMapper;
    private final ExamTestResponseMapper examTestMapper;
    private final ExamSessionRepository examSessionRepository;
    private final ExamRoomRepository examRoomRepository;
    private final ExamResponseMapper examResponseMapper;
    private final ExamSessionResponseMapper examSessionResponseMapper;
    private final ExamRoomResponseMapper examRoomResponseMapper;
    private final ExamSubjectResponseMapper subjectMapper;
    private final ExamSubjectRepository examSubjectRepository;
    public Long generateId() {
        Query query = new Query().with(Sort.by(Sort.Direction.DESC, "id")).limit(1);
        User latestUser = mongoOperations.findOne(query, User.class);
        return latestUser != null ? latestUser.getId() : null;
    }

    @Override
    public UserDto getUserDtoByUsername(String username) {
        return userMapper.entityToDto(getUserByUsername(username));
    }

    @Override
    public User getUserByUsername(String username) {
        if (!StringUtils.hasText(username))
            throw new EOException(UserStatus.USERNAME_IS_EXIST);

        User user = userRepository.findByUsername(username);
        if (null == user) {
            throw new EOException(CommonStatus.ACCOUNT_NOT_FOUND);
        }
        return user;
    }

    @Override
    public UserDto getInfo() {
        User user = userRepository.findByUsername(EbsSecurityUtils.getUsername());
        if (null == user) {
            throw new EOException(CommonStatus.ACCOUNT_NOT_FOUND);
        }

        return userMapper.entityToDto(user);
    }

    @Override
    public UserDto save(UserDto dto) {
        dto.setUsername(dto.getUsername());
        this.validateDto(dto);

        User user = new User();
        user.setId(generateId() + 1);
        this.mapDtoToEntity(dto, user, false);
        user = userRepository.save(user);
        return userMapper.entityToDto(user);
    }

    @Override
    public boolean createVerificationToChangePassword(String username) {
        return hasSendVerificationCodeSuccess(username);
    }

    @Override
    public TokenDto register(UserDto dto) {
        dto.setUsername(dto.getEmail());
        this.validateRegister(dto);

        User user = new User();
        this.mapDtoToEntity(dto, user, true);
        user = userRepository.save(user);

        String accessToken = EbsTokenUtils.createAccessToken(user);
        String refreshToken = EbsTokenUtils.createRefreshToken(user.getUsername());
        return new TokenDto(accessToken, refreshToken);
    }

    @Override
    public TokenDto refreshToken(String refreshToken) {
        Algorithm algorithm = Algorithm.HMAC256(Variables.SECRET_KEY.getBytes());
        JWTVerifier verifier = JWT.require(algorithm).build();
        DecodedJWT decodedJWT = verifier.verify(refreshToken);
        String username = decodedJWT.getSubject();
        User user = userRepository.findByUsername(username);

        if (null == user) {
            throw new EOException(CommonStatus.ACCOUNT_NOT_FOUND);
        }

        String accessToken = EbsTokenUtils.createAccessToken(user);
        return new TokenDto(accessToken, refreshToken);
    }

    @Override
    public void logout(HttpServletRequest request) {
        String header = request.getHeader("Authorization");
        if (!StringUtils.hasText(header) || !header.startsWith("Bearer ")) {
            throw new EOException(UserStatus.IS_NOT_TOKEN);
        }

        String token = header.substring(7);
        if (tokenBlacklist.isTokenExpired(token)) {
            throw new EOException(UserStatus.TOKEN_IS_EXPIRED);
        }

        tokenBlacklist.add(token);
    }

    @Override
    public boolean changePassword(UserDto dto) {
        this.validateChangePassword(dto);

        User user = userRepository.findByUsername(dto.getUsername());
        user.setPassword(bCryptPasswordEncoder.encode(dto.getPassword()));
        userRepository.save(user);
//        verificationCodeRepository.removeByUsername(user.getUsername());

        return true;
    }

    @Transactional
    @Override
    public void increaseFailedAttempts(String username, byte newFailAttempts) {
        userRepository.updateFailedAttempts(newFailAttempts, username);
    }

    @Transactional
    @Override
    public void temporaryLock(String username, int coefficient) {
        LocalDateTime date = LocalDateTime.now().plusMinutes((long) Variables.LOCK_TIME_DURATION * coefficient);

        userRepository.updateLockedTime(date, username);
    }

    @Override
    public boolean permanentLock(String username) {
        User user = userRepository.findByUsername(username);
        if (user == null || user.getLockTime() == null || LocalDateTime.now().isBefore(user.getLockTime())) {
            return false;
        }

        user.setAccountNonLocked(true);
        user.setLockTime(null);
        user.setFailedAttempt((byte) 0);
        userRepository.save(user);

        return true;
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new ProviderNotFoundException(CommonStatus.ACCOUNT_NOT_FOUND.getMessage());
        }

        if (user.getLockTime() != null && user.getLockTime().isAfter(LocalDateTime.now())) {
            throw new LockedException(CommonStatus.TEMPORARY_LOCK.getMessage());
        }

        return user;
    }

    private boolean hasSendVerificationCodeSuccess(String username) {
        if (!username.matches(Variables.REGEX_EMAIL)) {
            throw new EOException(CommonStatus.EMAIL_IS_WRONG_FORMAT);
        }

        if (userRepository.existsByEmail(username)) {
            throw new EOException(CommonStatus.EMAIL_IS_EXIST);
        }
        return true;
    }

    private void validateDto(UserDto dto) {
        if (!StringUtils.hasText(dto.getUsername())) {
            throw new EOException(UserStatus.USERNAME_IS_EMPTY);
        }

        if (userRepository.existsByUsername(dto.getUsername())) {
            throw new EOException(UserStatus.USERNAME_IS_EXIST);
        }

//        if (!StringUtils.hasText(dto.getEmail())) {
//            throw new EOException(UserStatus.EMAIL_IS_EMPTY);
//        }
//
//        if (!dto.getEmail().matches(Variables.REGEX_EMAIL)) {
//            throw new EOException(UserStatus.EMAIL_IS_WRONG_FORMAT);
//        }
//
//        if (userRepository.existsByEmail(dto.getEmail())) {
//            throw new EOException(UserStatus.EMAIL_IS_EXIST);
//        }

        if (!StringUtils.hasText(dto.getPassword())) {
            throw new EOException(UserStatus.PASSWORD_IS_EMPTY);
        }
    }

    private void validateChangePassword(UserDto dto) {
        String userName = dto.getUsername();
        if (!StringUtils.hasText(userName)) {
            throw new EOException(UserStatus.USERNAME_IS_EMPTY);
        }

        if (!userRepository.existsByUsername(userName)) {
            throw new EOException(CommonStatus.ACCOUNT_NOT_FOUND);
        }

        String newPassword = dto.getPassword();
        if (!StringUtils.hasText(newPassword)) {
            throw new EOException(UserStatus.PASSWORD_IS_EMPTY);
        }

        if (!newPassword.equals(dto.getConfirmPassword())) {
            throw new EOException(UserStatus.CONFIRM_PASSWORD_IS_ERROR);
        }
    }

    private void validateRegister(UserDto dto) {
        this.validateDto(dto);
    }

    private void mapDtoToEntity(UserDto dto, User entity, boolean isRegister) {
        entity.setUsername(dto.getUsername());
        entity.setEmail(dto.getEmail());
        entity.setActive(true);
        entity.setPassword(bCryptPasswordEncoder.encode(dto.getPassword()));
        if (isRegister) {
            Set<Role> role = dto.getRoles().stream().map(e -> roleRepository.findByName(e.getName())).collect(Collectors.toSet());
            entity.setRoles(new ArrayList<>(role));

        }

//        List<Role> roles = new ArrayList<>();
//        for (RoleDto element : dto.getRoles()) {
//            Role role = roleRepository.findByName(element.getName());
//            if (null != role)
//                roles.add(role);
//        }
//
//        entity.getRoles().clear();
//        entity.getRoles().addAll(roles);
    }

    public UserDto createAccountExaminer(ExaminerUserRequestDto dto) {

        this.validateDto(dto.getUser());
        User user = new User();
        user.setId(generateId() + 1);
        this.mapDtoToEntity(dto.getUser(), user, true);
        user = userRepository.save(user);
        if (Objects.nonNull(dto.getExaminerId())) {
            ExaminerEntity examiner = examinerRepos.findById(dto.getExaminerId())
                    .orElseThrow(() -> new EOException(ErrorCodes.ENTITY_NOT_FOUND, MessageCodes.EXAMINER_NOT_FOUND, dto.getExaminerId()));

            if (Objects.nonNull(examiner)) {
                examiner.setUserId(user.getId());
                examiner.setStatus(CHUA_THI);
                ExamEntity examEntity=examRepository.findById(examiner.getExam().toString()).orElseThrow(
                        ()->new EntityNotFoundException(ExamEntity.class.getName(),"Id",examiner.getExam().toString())
                );
                boolean kt = user.getRoles().stream().anyMatch(role -> role.getName().equals("ROLE_USER"));
                if(Objects.nonNull(examEntity) && kt){
                    List<ObjectId> list=new ArrayList<>();
                    if(examEntity.getExaminersId()!=null){
                        list=examEntity.getExaminersId();
                    }
                    list.add(new ObjectId(examiner.getId()));
                    examEntity.setExaminersId(list);
                    examRepository.save(examEntity);
                }
                examinerRepos.save(examiner);
            }
        }
        return userMapper.entityToDto(user);
    }

    public ExaminerUserResponseDto getExamTestByUser(ExamTestSearchDto dto ) {
        ExaminerUserResponseDto examinerUser = new ExaminerUserResponseDto();
        User user = userRepository.findByUsername(EbsSecurityUtils.getUsername());
        List<ExamQuestionEntity> examQuestionEntityList=examQuestionService.getAllExamQuestion();
        ExaminerEntity examiner = examinerRepos.findExaminerEntitiesByUserId(user.getId());
        if (Objects.isNull(user)) {
            throw new EOException(CommonStatus.ACCOUNT_NOT_FOUND);
        }
        examinerUser.setUser(userMapper.entityToDto(user));

        ExamTestEntity test = testRepos.findByExaminerAndSession(new ObjectId(examiner.getId()), examiner.getSession());
        List<ObjectId> listQuestion = test.getQuestion();
        List<ExamQuestionEntity> entityList = examQuestionEntityList.stream().filter(entity -> listQuestion
                .contains(new ObjectId(entity.getId()))).collect(Collectors.toList());

        List<ExamQuestionResponseDto> examQuestionResponseDtos = new ArrayList<>();

        for (ExamQuestionEntity entity : entityList) {
            List<String> answers = new ArrayList<>();
            ExamQuestionResponseDto responseDto = questionMapper.entityToResponse(entity);
            this.mapAnswerQuestionToList(entity, answers);
            responseDto.setAnswers(answers);
            examQuestionResponseDtos.add(responseDto);
        }

        ExamTestResponseDto examTestResponseDto=examTestMapper.entityToResponseDto(test);
        examTestResponseDto.setQuestion(examQuestionResponseDtos);
        examinerUser.setTest(examTestResponseDto);
        examinerUser.setExaminer(entityToResponseMapper(examiner));
        return examinerUser;
    }

    private void mapAnswerQuestionToList(ExamQuestionEntity question, List<String> answers) {
        answers.add(question.getAnswer1());
        answers.add(question.getAnswer2());
        answers.add(question.getAnswer3());
        answers.add(question.getAnswer4());
        answers.add(question.getAnswer5());
        answers.add(question.getAnswer6());
        answers.add(question.getAnswer7());
        answers.add(question.getAnswer8());
        answers.add(question.getAnswer9());
        answers.add(question.getAnswer10());
    }

    public ExaminerResponseDto entityToResponseMapper(ExaminerEntity entity){
        ExaminerResponseDto examinerResponseDto=examinerMapper.entityToResponse(entity);
        examinerResponseDto.setId(entity.getId().toString());
        if(entity.getExam()!=null){
            examinerResponseDto.setExam(examResponseMapper.entityToResponse(
                    examRepository.findById(entity.getExam().toString())
                            .orElseThrow(() -> new EOException(ErrorCodes.ENTITY_NOT_FOUND, ENTITY_NOT_FOUND, entity.getExam().toString()))
            ));
        }
        if(entity.getSession()!=null) {
            ExamSessionEntity examSessionEntity =examSessionRepository.findById(entity.getSession().toString())
                    .orElseThrow(() -> new EOException(ErrorCodes.ENTITY_NOT_FOUND, ENTITY_NOT_FOUND, entity.getSession().toString()));
            ExamSessionResponseDto examSessionResponseDto=examSessionResponseMapper.entityToResponse(examSessionEntity);
            if (examSessionEntity.getSubject() != null) {
                examSessionResponseDto.setSubject(subjectMapper.entityToResponse(examSubjectRepository.findById(examSessionEntity.getSubject().toString())
                        .orElseThrow(() -> new EntityNotFoundException(ExamSubjectEntity.class.getName(), "id", examSessionEntity.getSubject().toString()))));
            }
            examinerResponseDto.setSession(examSessionResponseDto);
        }
        if(entity.getRoom()!=null){
            examinerResponseDto.setRoom(examRoomResponseMapper.entityToResponseDto(
                    examRoomRepository.findById(entity.getRoom().toString())
                            .orElseThrow(() -> new EOException(ErrorCodes.ENTITY_NOT_FOUND, ENTITY_NOT_FOUND, entity.getRoom().toString()))));
        }
        return examinerResponseDto;
    }
    @Override
    public Page<ExaminerUserResponseDto> searchBy(SearchUserDto dto) {
        Pageable pageable = PageUtils.getPageable(dto.getPageIndex(), dto.getPageSize());

        LookupOperation lookupOperation = LookupOperation.newLookup()
                .from("tbl_user")
                .localField("userId")
                .foreignField("id")
                .as("user");

        Criteria criteria = new Criteria();
        if (Objects.nonNull(dto.getRole())) {
            criteria = Criteria.where("user.roles").elemMatch(Criteria.where("_id").is(dto.getRole()));
        }
        if (Objects.nonNull(dto.getUserName())) {
            criteria.and("user.username").is(dto.getUserName());
        }
        if (Objects.nonNull(dto.getExamId())) {
            criteria.and("exam_id").is(new ObjectId(dto.getExamId()));
        }
        if (Objects.nonNull(dto.getSessionId())) {
            criteria.and("session_id").is(new ObjectId(dto.getSessionId()));
        }
        AggregationOperation matchOperation = Aggregation.match(criteria);

        List<AggregationOperation> operations = new ArrayList<>();
        operations.add(matchOperation);
        operations.add(lookupOperation);
        Aggregation aggregation = Aggregation.newAggregation(operations);

        List<ExaminerEntity> entityList = mongoTemplate
                .aggregate(aggregation, "tbl_examiner", ExaminerEntity.class)
                .getMappedResults();
        long count = entityList.size();
        List<ExaminerUserResponseDto> responseDtos = new ArrayList<>();
        this.mapUserExaminer(responseDtos, entityList, dto);
        return new PageImpl<>(responseDtos, pageable, count);
    }

    private void mapUserExaminer(List<ExaminerUserResponseDto> users, List<ExaminerEntity> examiners, SearchUserDto dto) {
        if (!CollectionUtils.isEmpty(examiners)) {
            for (ExaminerEntity exam : examiners) {
                if (Objects.nonNull(exam.getUserId())) {
                    ExaminerUserResponseDto responseDto = new ExaminerUserResponseDto();

                    User user = userRepository.findById(exam.getUserId())
                            .orElseThrow(() -> new EntityNotFoundException(User.class.getName(), "id", exam.getUserId()));

                    responseDto.setExaminer(examinerMapper.entityToResponse(exam));
                    responseDto.setUser(userMapper.entityToDto(user));
                    users.add(responseDto);
                }
            }
        }
    }

    @Override
    public UserDto updateAccountExaminer(Long id, ExaminerUserRequestDto dto) {

        User user = userRepository.findById(id)
                .orElseThrow(()-> new EntityNotFoundException(User.class.getName(), "id", id));
        this.mapDtoToEntity(dto.getUser(), user, true);
        user = userRepository.save(user);
        ExaminerEntity examiner = examinerRepos.findByUserId(user.getId());

        if (Objects.nonNull(examiner)) {
            examiner.setUserId(user.getId());
            examinerRepos.save(examiner);
        }
        return userMapper.entityToDto(user);
    }

    @Override
    public Boolean deleteAccountExaminer(Long id) {

        User user = userRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(User.class.getName(), "id", id));
        ExaminerEntity examiner = examinerRepos.findByUserId(user.getId());
        this.validBeforeDelete(examiner);
        userRepository.deleteById(id);
        examinerRepos.deleteById(examiner.getId());
        return true;
    }

    private void validBeforeDelete(ExaminerEntity examiner){
        if (Objects.nonNull(examiner) && !examiner.getStatus().equals(CHUA_THI)){
            throw new EOException(INVALID_STATUS, MessageCodes.INVALID_STATUS, examiner.getStatus().getName());
        }
    }
}
