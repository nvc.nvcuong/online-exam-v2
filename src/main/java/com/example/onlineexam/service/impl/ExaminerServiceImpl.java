package com.example.onlineexam.service.impl;

import com.example.onlineexam.constant.ErrorCodes;
import com.example.onlineexam.constant.MessageCodes;
import com.example.onlineexam.dto.UserDto;
import com.example.onlineexam.dto.request.ExaminerRequestDto;
import com.example.onlineexam.dto.request.ExaminerUserRequestDto;
import com.example.onlineexam.dto.request.searchDto.ExaminerSearchDto;
import com.example.onlineexam.dto.response.ExaminerResponseDto;
import com.example.onlineexam.dto.response.ExaminerUserResponseDto;
import com.example.onlineexam.entity.ExamEntity;
import com.example.onlineexam.entity.ExaminerEntity;
import com.example.onlineexam.entity.User;
import com.example.onlineexam.exception.EOException;
import com.example.onlineexam.exception.EntityNotFoundException;
import com.example.onlineexam.repository.*;
import com.example.onlineexam.service.ExaminerService;
import com.example.onlineexam.service.UserService;
import com.example.onlineexam.service.mapper.ExamResponseMapper;
import com.example.onlineexam.service.mapper.ExamRoomResponseMapper;
import com.example.onlineexam.service.mapper.ExamSessionResponseMapper;
import com.example.onlineexam.service.mapper.ExaminerResponseMapper;
import com.example.onlineexam.utils.ImportExcelUtil;
import com.example.onlineexam.utils.PageUtils;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.example.onlineexam.constant.MessageCodes.ENTITY_NOT_FOUND;
import static com.example.onlineexam.error.CommonStatus.NOT_FOUND;

@Service
@AllArgsConstructor
public class ExaminerServiceImpl implements ExaminerService {

    private final ExaminerRepository examinerRepos;
    private final ExaminerResponseMapper examinerResponseMapper;
    private final MongoTemplate mongoTemplate;
    private final ExamSessionRepository examSessionRepository;
    private final ExamRepository examRepository;
    private final ExamRoomRepository examRoomRepository;
    private final ExamResponseMapper examResponseMapper;
    private final ExamSessionResponseMapper examSessionResponseMapper;
    private final ExamRoomResponseMapper examRoomResponseMapper;
    private final  UserRepository userRepository;
    private final UserService userService;
    private final ImportExcelUtil importExcelUtil;
    @Override
    public ExaminerResponseDto getById(String id) {
        return this.entityToResponseMapper(examinerRepos.findById(id)
                .orElseThrow(() -> new EOException(ErrorCodes.ERROR_CODE, MessageCodes.ENTITY_NOT_FOUND, id)));
    }

    @Override
    public Page<ExaminerResponseDto> searchBy(ExaminerSearchDto searchDto) {

        Query query = new Query();
        Pageable pageable = PageUtils.getPageable(searchDto.getPageIndex(), searchDto.getPageSize());
        if (StringUtils.hasText(searchDto.getKeyword())) {
            query.addCriteria(Criteria.where("").is(searchDto.getKeyword()));
        }
        if (StringUtils.hasText(searchDto.getExamId())) {
            query.addCriteria(Criteria.where("exam").is(new ObjectId(searchDto.getExamId())));
        }
        query.with(Sort.by(Sort.Order.desc("startDate")));
        query.with(pageable);
        List<ExaminerEntity> entityList = mongoTemplate.find(query, ExaminerEntity.class);
        long count = entityList.size();
        List<ExaminerResponseDto> responseDtos = entityList.stream().map(this::entityToResponseMapper).collect(Collectors.toList());
        return new PageImpl<>(responseDtos, pageable, count);
    }
   public List<ExaminerUserResponseDto> xuly(List<ExaminerEntity> examinerEntityList, String Role){
        List<ExaminerUserResponseDto> responseDtos=new ArrayList<>();
       for(ExaminerEntity e:examinerEntityList){
           if(e.getUserId()!=null) {
               User user = userRepository.findById(e.getUserId()).orElseThrow(()->new EOException(NOT_FOUND, e.getUserId().toString()));
               boolean kt = user.getRoles().stream().anyMatch(role -> role.getName().equals(Role));
               if (user != null && kt ) {
                   ExaminerUserResponseDto personDto = new ExaminerUserResponseDto();

                   ExaminerResponseDto examinerResponseDto=new ExaminerResponseDto();
                   examinerResponseDto.setName(e.getName());
                   examinerResponseDto.setEmail(e.getEmail());
                   examinerResponseDto.setBirthday(e.getBirthday());
                   personDto.setExaminer(examinerResponseDto);

                   UserDto userDto=new UserDto();
                   userDto.setUsername(user.getUsername());
                   personDto.setUser(userDto);
                   responseDtos.add(personDto);
               }
           }
       }
       return responseDtos;
   }
    @Override
    public Page<ExaminerUserResponseDto> searchByPerson(ExaminerSearchDto searchDto) {

        searchDto.setPageIndexAndPageSize(searchDto.getPageIndex(), searchDto.getPageSize());
        Pageable pageable = PageRequest.of(searchDto.getPageIndex(), searchDto.getPageSize());
        List<ExaminerUserResponseDto> responseDtos=new ArrayList<>();
        List<ExaminerEntity> examinerEntityList=examinerRepos.findByExam(new ObjectId(searchDto.getExamId()));
        responseDtos=xuly(examinerEntityList, searchDto.getRole());
        long count=responseDtos.size();

        return new PageImpl<>(responseDtos, pageable, count);
    }
    @Override
    public ExaminerResponseDto createBy(@NonNull ExaminerRequestDto dto) {
        ExaminerEntity entity = this.requestToEntityMapper(dto);
        examinerRepos.save(entity);
        ExaminerUserRequestDto userRequestDto = new ExaminerUserRequestDto();
        UserDto userDto = new UserDto();
        importExcelUtil.setValueUser(userDto, entity);
        userRequestDto.setUser(userDto);
        userRequestDto.setExaminerId(entity.getId());
        userService.createAccountExaminer(userRequestDto);
        return this.entityToResponseMapper(entity);
    }


    @Override
    public ExaminerResponseDto updateBy(@NonNull String id, @NonNull ExaminerRequestDto dto) {
        ExaminerEntity entity = examinerRepos.findById(id)
                .orElseThrow(() -> new EOException(ErrorCodes.ERROR_CODE, MessageCodes.ENTITY_NOT_FOUND, id));

        entity = this.requestToEntityMapper(dto);
        entity.setId(new ObjectId(id));
        examinerRepos.save(entity);
        return this.entityToResponseMapper(entity);
    }

    @Override
    public void deleteBy(@NonNull String id) {
        examinerRepos.deleteById(id);
    }

    @Override
    public ExaminerEntity requestToEntityMapper(ExaminerRequestDto dto) {

        ExaminerEntity examinerEntity = examinerResponseMapper.requestToEntity(dto);
        if (dto.getExam() != null) {
            examinerEntity.setExam(new ObjectId(dto.getExam().getId()));
        }
        if (dto.getRoom() != null) {
            examinerEntity.setRoom(new ObjectId(dto.getRoom().getId()));
        }
        if (dto.getSession() != null) {
            examinerEntity.setSession(new ObjectId(dto.getSession().getId()));
        }

        return examinerEntity;
    }

  public ExaminerResponseDto entityToResponseMapper(ExaminerEntity entity){
        ExaminerResponseDto examinerResponseDto=examinerResponseMapper.entityToResponse(entity);
        examinerResponseDto.setId(entity.getId().toString());
        if(entity.getExam()!=null){
          examinerResponseDto.setExam(examResponseMapper.entityToResponse(
                  examRepository.findById(entity.getExam().toString())
                          .orElseThrow(() -> new EOException(ErrorCodes.ENTITY_NOT_FOUND, ENTITY_NOT_FOUND, entity.getExam().toString()))
          ));
        }
        if(entity.getSession()!=null){
            examinerResponseDto.setSession(
                    examSessionResponseMapper.entityToResponse(examSessionRepository.findById(entity.getSession().toString())
                            .orElseThrow(() -> new EOException(ErrorCodes.ENTITY_NOT_FOUND, ENTITY_NOT_FOUND, entity.getSession().toString()))));
        }
        if(entity.getRoom()!=null){
            examinerResponseDto.setRoom(examRoomResponseMapper.entityToResponseDto(
                    examRoomRepository.findById(entity.getRoom().toString())
                            .orElseThrow(() -> new EOException(ErrorCodes.ENTITY_NOT_FOUND, ENTITY_NOT_FOUND, entity.getRoom().toString()))));
        }
        return examinerResponseDto;
    }
}
