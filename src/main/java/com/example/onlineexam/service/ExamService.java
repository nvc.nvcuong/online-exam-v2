package com.example.onlineexam.service;

import com.example.onlineexam.dto.request.ExamRequestDto;
import com.example.onlineexam.dto.request.searchDto.ExamSearchDto;
import com.example.onlineexam.dto.response.ExamResponseDto;
import lombok.NonNull;
import org.springframework.data.domain.Page;

import java.util.UUID;

public interface ExamService {

    ExamResponseDto getById(UUID id);

    ExamResponseDto createBy (ExamRequestDto dto);

    ExamResponseDto updateBy(UUID id, ExamRequestDto dto);

    Page<ExamResponseDto> searchBy (@NonNull ExamSearchDto searchDto);

    void deleteBy(UUID id);

    void deleteByExamier(UUID id,UUID idExamier);
}
