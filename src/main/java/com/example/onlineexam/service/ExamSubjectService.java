package com.example.onlineexam.service;

import com.example.onlineexam.dto.request.ExamSubjectRequestDto;
import com.example.onlineexam.dto.request.searchDto.ExamSubjectSearchDto;
import com.example.onlineexam.dto.response.ExamSubjectResponseDto;
import lombok.NonNull;
import org.springframework.data.domain.Page;

public interface ExamSubjectService {

    ExamSubjectResponseDto getById(String id);

    ExamSubjectResponseDto createBy(ExamSubjectRequestDto dto);

    ExamSubjectResponseDto updateBy(String id, ExamSubjectRequestDto dto);

    Page<ExamSubjectResponseDto> searchBy(@NonNull ExamSubjectSearchDto searchDto);

    void deleteBy(String id);
}
