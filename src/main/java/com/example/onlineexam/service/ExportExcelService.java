package com.example.onlineexam.service;

import com.example.onlineexam.dto.request.searchDto.ExamTestResultSerchDto;
import org.springframework.core.io.ByteArrayResource;

import java.io.IOException;

public interface ExportExcelService {

    ByteArrayResource exportDiemThi(ExamTestResultSerchDto searchDto) throws IOException;
}
