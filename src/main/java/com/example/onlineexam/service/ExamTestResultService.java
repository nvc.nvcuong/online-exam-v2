package com.example.onlineexam.service;

import com.example.onlineexam.dto.request.ExamDayRequestDto;
import com.example.onlineexam.dto.request.ExamTestResultRequestDto;
import com.example.onlineexam.dto.request.searchDto.ExamDaySearchDto;
import com.example.onlineexam.dto.request.searchDto.ExamTestResultSerchDto;
import com.example.onlineexam.dto.response.ExamDayResponseDto;
import com.example.onlineexam.dto.response.ExamTestResultResponseDto;
import com.example.onlineexam.dto.response.TestResultDetailReponse;
import org.springframework.data.domain.Page;

import javax.validation.constraints.NotNull;

public interface ExamTestResultService {
    ExamTestResultResponseDto getById(String id);

    ExamTestResultResponseDto createBy(ExamTestResultRequestDto dto);

    ExamTestResultResponseDto updateBy(String id, ExamTestResultRequestDto dto);
    ExamTestResultResponseDto showMark(String id);
    Page<ExamTestResultResponseDto> searchBy (ExamTestResultSerchDto dto);

    Boolean deleteBy(String id);
    Boolean deleteByTestId(String id);
    Page<TestResultDetailReponse> testDetail(ExamTestResultSerchDto dto);
}
