package com.example.onlineexam.service;

import com.example.onlineexam.dto.request.ExamSessionRequestDto;
import com.example.onlineexam.dto.request.searchDto.ExamSessionSearchDto;
import com.example.onlineexam.dto.response.ExamSessionResponseDto;
import com.example.onlineexam.entity.ExamSessionEntity;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ExamSessionService {

    ExamSessionResponseDto getById(String id);
    ExamSessionResponseDto createBy(ExamSessionRequestDto dto);
    ExamSessionResponseDto updateBy(String id, ExamSessionRequestDto dto);
    Page<ExamSessionResponseDto> searchBy (ExamSessionSearchDto dto);

    List<ExamSessionResponseDto> getByExam(String id);

    ExamSessionResponseDto entityToResponseMapper(ExamSessionEntity entity);

    void deleteBy(String id);
}
