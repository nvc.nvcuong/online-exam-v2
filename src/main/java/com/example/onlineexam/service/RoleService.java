package com.example.onlineexam.service;


import com.example.onlineexam.dto.RoleDto;

public interface RoleService {
    RoleDto save(RoleDto dto);
    RoleDto update(Long id , RoleDto dto);
}
