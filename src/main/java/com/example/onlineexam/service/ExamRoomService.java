package com.example.onlineexam.service;

import com.example.onlineexam.dto.request.ExamRoomRequestDto;
import com.example.onlineexam.dto.request.searchDto.ExamRoomSearchDto;
import com.example.onlineexam.dto.response.ExamRoomResponseDto;
import lombok.NonNull;
import org.springframework.data.domain.Page;

public interface ExamRoomService {
    ExamRoomResponseDto getById(String id);

    ExamRoomResponseDto createBy(ExamRoomRequestDto dto);

    ExamRoomResponseDto updateBy(String id, ExamRoomRequestDto dto);

    Page<ExamRoomResponseDto> searchBy(@NonNull ExamRoomSearchDto searchDto);

    void deleteBy(String id);
}
