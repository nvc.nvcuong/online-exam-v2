package com.example.onlineexam.service;

import com.example.onlineexam.dto.request.ExamQuestionRequestDto;
import com.example.onlineexam.dto.request.searchDto.ExamQuestionSearchDto;
import com.example.onlineexam.dto.response.ExamQuestionResponseDto;
import com.example.onlineexam.entity.ExamQuestionEntity;
import lombok.NonNull;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ExamQuestionService {

    ExamQuestionResponseDto getById(String id);

    ExamQuestionResponseDto createBy(ExamQuestionRequestDto dto);

    ExamQuestionResponseDto updateBy(String id, ExamQuestionRequestDto dto);

    List<ExamQuestionResponseDto> getBySubject(String id);

    Page<ExamQuestionResponseDto> searchBy(@NonNull ExamQuestionSearchDto searchDto);

    void deleteBy(String id);

    List<ExamQuestionEntity> getAllExamQuestion();


}
