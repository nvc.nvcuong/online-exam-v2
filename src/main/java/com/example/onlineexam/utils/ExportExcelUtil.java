package com.example.onlineexam.utils;

import com.example.onlineexam.dto.response.ExamTestResultResponseDto;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.example.onlineexam.constant.DateConstants.DATE_FORMAT_DD_MM_YYYY;

public class ExportExcelUtil {

    static Logger logger = LoggerFactory.getLogger(ExportExcelUtil.class);
    public static SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT_DD_MM_YYYY);

    public static <T> byte[] exportToExcel(List<ExamTestResultResponseDto> data, String fileName, int rowStart, int colNumStart) throws IOException {
        try (FileInputStream inputStream = new FileInputStream(fileName);
             XSSFWorkbook workbook = new XSSFWorkbook(inputStream)) {
            Sheet sheet = workbook.getSheetAt(0);
            if (!data.isEmpty()) {
                appendData(data, workbook, sheet, rowStart, colNumStart);
            }
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            workbook.write(outputStream);
            return outputStream.toByteArray();
        }
    }

    public static <T> int appendData(List<ExamTestResultResponseDto> data, XSSFWorkbook workbook, Sheet sheet, int rowStart, int colNumStart) {
        if (!data.isEmpty()) {
            CellStyle cellStyle = workbook.createCellStyle();
            for (int rowNum = 1; rowNum <= data.size(); rowNum++) {
                Row row = sheet.createRow(rowStart);
                ExamTestResultResponseDto item = data.get(rowNum - 1);
                int colNum = colNumStart;

                Cell cell = row.createCell(colNum);
                setStyleSheet(workbook, cellStyle);

                CellStyle orderNumStyle = workbook.createCellStyle();
                orderNumStyle.setAlignment(HorizontalAlignment.CENTER);
                orderNumStyle.setVerticalAlignment(VerticalAlignment.CENTER);
                setStyleSheet(workbook, orderNumStyle);

                cell.setCellValue(rowNum);
                cell.setCellStyle(orderNumStyle);

                colNum++;
                cell = row.createCell(colNum);
                setStyleSheet(workbook, cellStyle);
                setCellValueByFieldType(cell, cellStyle, item.getExaminer().getName());

                colNum++;
                cell = row.createCell(colNum);
                setStyleSheet(workbook, cellStyle);
                setCellValueByFieldType(cell, cellStyle, item.getExaminer().getBirthday());

                colNum++;
                cell = row.createCell(colNum);
                setStyleSheet(workbook, cellStyle);
                setCellValueByFieldType(cell, cellStyle, item.getExaminer().getPersonCode());

                colNum++;
                cell = row.createCell(colNum);
                setStyleSheet(workbook, cellStyle);
                setCellValueByFieldType(cell, cellStyle, item.getCorrectAnswers() + "/" + item.getTotalQuestion());

                colNum++;
                cell = row.createCell(colNum);
                setStyleSheet(workbook, cellStyle);
                setCellValueByFieldType(cell, cellStyle, item.getMark());

                colNum++;
                cell = row.createCell(colNum);
                setStyleSheet(workbook, cellStyle);
                setCellValueByFieldType(cell, cellStyle, item.getExaminer().getDepartments());

                colNum++;
                cell = row.createCell(colNum);
                setStyleSheet(workbook, cellStyle);
                setCellValueByFieldType(cell, cellStyle, item.getExaminer().getClassRoom());

                colNum++;
                cell = row.createCell(colNum);
                setStyleSheet(workbook, cellStyle);
                setCellValueByFieldType(cell, cellStyle, item.getExaminer().getMajors());

                colNum++;
                cell = row.createCell(colNum);
                setStyleSheet(workbook, cellStyle);
                setCellValueByFieldType(cell, cellStyle, item.getExaminer().getSchool());

                rowStart++;
            }
        }
        return rowStart;
    }

    public static void setStyleSheet(XSSFWorkbook workbook, CellStyle cellStyle) {
        Font font = createDefaultFont(workbook);

        cellStyle.setFont(font);
        setBorderStyle(cellStyle);
    }

    private static Font createDefaultFont(XSSFWorkbook workbook) {
        Font font = workbook.createFont();
        font.setFontHeightInPoints((short) 13);
        font.setFontName("Times New Roman");
        return font;
    }

    private static void setBorderStyle(CellStyle cellStyle) {
        cellStyle.setBorderBottom(BorderStyle.THIN);
        cellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        cellStyle.setBorderLeft(BorderStyle.THIN);
        cellStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        cellStyle.setBorderRight(BorderStyle.THIN);
        cellStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
        cellStyle.setBorderTop(BorderStyle.THIN);
        cellStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
    }

    public static void setCellValueByFieldType(Cell cell, CellStyle cellStyle, Object fieldValue) {
        if (fieldValue != null) {
            if (fieldValue instanceof Date) {
                SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_DD_MM_YYYY);
                String formattedDate = dateFormat.format((Date) fieldValue);
                cell.setCellValue(formattedDate);
            } else if (fieldValue instanceof Timestamp) {
                SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_DD_MM_YYYY);
                String formattedDate = dateFormat.format((Timestamp) fieldValue);
                cell.setCellValue(formattedDate);
            } else if (fieldValue instanceof Number) {
                cell.setCellValue(String.format("%.0f", ((Number) fieldValue).doubleValue()));
            } else if (fieldValue instanceof String) {
                cell.setCellValue((String) fieldValue);
            } else if (fieldValue instanceof Boolean) {
                String value = (boolean) fieldValue ? "Có" : "Không";
                cell.setCellValue(value);
            } else {
                cell.setCellValue(fieldValue.toString());
            }
            cell.setCellStyle(cellStyle);
        } else {
            cell.setCellValue("");
            cell.setCellStyle(cellStyle);
        }
    }
}
