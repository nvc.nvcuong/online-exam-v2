package com.example.onlineexam.utils;

import com.example.onlineexam.constant.MessageCodes;
import com.example.onlineexam.constant.enums.ExamTestEnum;
import com.example.onlineexam.constant.enums.QuestionStatusEnum;
import com.example.onlineexam.dto.RoleDto;
import com.example.onlineexam.dto.UserDto;
import com.example.onlineexam.dto.request.*;
import com.example.onlineexam.dto.response.ExamQuestionImportResp;
import com.example.onlineexam.dto.response.ExaminerImportResp;
import com.example.onlineexam.entity.ExamEntity;
import com.example.onlineexam.entity.ExamQuestionEntity;
import com.example.onlineexam.entity.ExaminerEntity;
import com.example.onlineexam.exception.EOException;
import com.example.onlineexam.repository.*;
import com.example.onlineexam.service.UserService;
import lombok.RequiredArgsConstructor;
import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.*;

import static com.example.onlineexam.constant.ErrorCodes.ENTITY_NOT_FOUND;
import static com.example.onlineexam.constant.enums.ExamTestEnum.DIFFICULTY.values;

@Service
@RequiredArgsConstructor
public class ImportExcelUtil {

    private final ExamQuestionRepository questionRepos;
    private final ExaminerRepository examinerRepos;
    private final UserService userService;
    private final ExamRepository examRepository;

    public ExamQuestionImportResp importQuestion(Sheet sheet, String examId, String subjectId) {

        ExamQuestionImportResp resp = new ExamQuestionImportResp();
        List<ExamQuestionEntity> listQuestions = new ArrayList<>();
        CellReference cr = new CellReference("A3");
        int startRowIndex = cr.getRow();
        int lastRowNum = sheet.getLastRowNum();

        Row currentRow;
        Cell currentCell;

        while (startRowIndex <= lastRowNum) {
            int cellIndex = 1;

            ExamQuestionEntity question = new ExamQuestionEntity();
            String questionCode = null;
            int level = 0;
            String questionContent = null;
            String answer1 = null;
            String answer2 = null;
            String answer3 = null;
            String answer4 = null;
            String answer5 = null;
            String answer6 = null;
            String answer7 = null;
            String answer8 = null;
            String answer9 = null;
            String answer10 = null;
            String correctAnswer = null;

            currentRow = sheet.getRow(startRowIndex);
            if (Objects.nonNull(currentRow)) {

//                currentCell = currentRow.getCell(cellIndex);
//                if (currentCell != null && currentCell.getCellTypeEnum() == CellType.STRING) {
//                    questionCode = currentCell.getStringCellValue();
//
//                } else if (currentCell != null && currentCell.getCellTypeEnum() == CellType.NUMERIC) {
//                    questionCode = String.valueOf(currentCell.getNumericCellValue());
//                }
//                question.setQuestionCode(questionCode);
//
//                ++cellIndex;
                currentCell = currentRow.getCell(cellIndex);
                if (currentCell != null && currentCell.getCellTypeEnum() == CellType.STRING) {
                    level = this.setLevelByName(currentCell);
                } else if (currentCell != null && currentCell.getCellTypeEnum() == CellType.NUMERIC) {
                    level = this.setLevelByCode(currentCell);
                }
                question.setLevel(level);

                ++cellIndex;
                currentCell = currentRow.getCell(cellIndex);
                if (currentCell != null && currentCell.getCellTypeEnum() == CellType.STRING) {
                    questionContent = currentCell.getStringCellValue();
                } else if (currentCell != null && currentCell.getCellTypeEnum() == CellType.NUMERIC) {
                    questionContent = String.valueOf(currentCell.getNumericCellValue());
                }
                question.setQuestionContent(questionContent);

                ++cellIndex;
                currentCell = currentRow.getCell(cellIndex);
                if (currentCell != null && currentCell.getCellTypeEnum() == CellType.STRING) {
                    correctAnswer = currentCell.getStringCellValue();
                } else if (currentCell != null && currentCell.getCellTypeEnum() == CellType.NUMERIC) {
                    if (currentCell.getNumericCellValue() == Math.floor(currentCell.getNumericCellValue())) {
                        correctAnswer = String.valueOf((int) currentCell.getNumericCellValue());
                    } else {
                        correctAnswer = String.valueOf(currentCell.getNumericCellValue());
                    }
                }
                question.setCorrectAnswer(correctAnswer);

                ++cellIndex;
                currentCell = currentRow.getCell(cellIndex);
                if (currentCell != null && currentCell.getCellTypeEnum() == CellType.STRING) {
                    answer1 = currentCell.getStringCellValue();
                } else if (currentCell != null && currentCell.getCellTypeEnum() == CellType.NUMERIC) {
                    answer1 = String.valueOf(currentCell.getNumericCellValue());
                }
                question.setAnswer1(answer1);

                ++cellIndex;
                currentCell = currentRow.getCell(cellIndex);
                if (currentCell != null && currentCell.getCellTypeEnum() == CellType.STRING) {
                    answer2 = currentCell.getStringCellValue();
                } else if (currentCell != null && currentCell.getCellTypeEnum() == CellType.NUMERIC) {
                    answer2 = String.valueOf(currentCell.getNumericCellValue());
                }
                question.setAnswer2(answer2);

                ++cellIndex;
                currentCell = currentRow.getCell(cellIndex);
                if (currentCell != null && currentCell.getCellTypeEnum() == CellType.STRING) {
                    answer3 = currentCell.getStringCellValue();
                } else if (currentCell != null && currentCell.getCellTypeEnum() == CellType.NUMERIC) {
                    answer3 = String.valueOf(currentCell.getNumericCellValue());
                }
                question.setAnswer3(answer3);

                ++cellIndex;
                currentCell = currentRow.getCell(cellIndex);
                if (currentCell != null && currentCell.getCellTypeEnum() == CellType.STRING) {
                    answer4 = currentCell.getStringCellValue();
                } else if (currentCell != null && currentCell.getCellTypeEnum() == CellType.NUMERIC) {
                    answer4 = String.valueOf(currentCell.getNumericCellValue());
                }
                question.setAnswer4(answer4);

                ++cellIndex;
                currentCell = currentRow.getCell(cellIndex);
                if (currentCell != null && currentCell.getCellTypeEnum() == CellType.STRING) {
                    answer5 = currentCell.getStringCellValue();
                } else if (currentCell != null && currentCell.getCellTypeEnum() == CellType.NUMERIC) {
                    answer5 = String.valueOf(currentCell.getNumericCellValue());
                }
                question.setAnswer5(answer5);

                ++cellIndex;
                currentCell = currentRow.getCell(cellIndex);
                if (currentCell != null && currentCell.getCellTypeEnum() == CellType.STRING) {
                    answer6 = currentCell.getStringCellValue();
                } else if (currentCell != null && currentCell.getCellTypeEnum() == CellType.NUMERIC) {
                    answer6 = String.valueOf(currentCell.getNumericCellValue());
                }
                question.setAnswer6(answer6);

                ++cellIndex;
                currentCell = currentRow.getCell(cellIndex);
                if (currentCell != null && currentCell.getCellTypeEnum() == CellType.STRING) {
                    answer7 = currentCell.getStringCellValue();
                } else if (currentCell != null && currentCell.getCellTypeEnum() == CellType.NUMERIC) {
                    answer7 = String.valueOf(currentCell.getNumericCellValue());
                }
                question.setAnswer7(answer7);

                ++cellIndex;
                currentCell = currentRow.getCell(cellIndex);
                if (currentCell != null && currentCell.getCellTypeEnum() == CellType.STRING) {
                    answer8 = currentCell.getStringCellValue();
                } else if (currentCell != null && currentCell.getCellTypeEnum() == CellType.NUMERIC) {
                    answer8 = String.valueOf(currentCell.getNumericCellValue());
                }
                question.setAnswer8(answer8);

                ++cellIndex;
                currentCell = currentRow.getCell(cellIndex);
                if (currentCell != null && currentCell.getCellTypeEnum() == CellType.STRING) {
                    answer9 = currentCell.getStringCellValue();
                } else if (currentCell != null && currentCell.getCellTypeEnum() == CellType.NUMERIC) {
                    answer9 = String.valueOf(currentCell.getNumericCellValue());
                }
                question.setAnswer9(answer9);

                ++cellIndex;
                currentCell = currentRow.getCell(cellIndex);
                if (currentCell != null && currentCell.getCellTypeEnum() == CellType.STRING) {
                    answer10 = currentCell.getStringCellValue();
                } else if (currentCell != null && currentCell.getCellTypeEnum() == CellType.NUMERIC) {
                    answer10 = String.valueOf(currentCell.getNumericCellValue());
                }
                question.setAnswer10(answer10);

                question.setQuestionCode(RenderCodeTest.setValue());

                question.setStatus(QuestionStatusEnum.DISABLED);
                question.setExam(new ObjectId(examId));
                question.setSubject(new ObjectId(subjectId));
                listQuestions.add(question);
            }
            startRowIndex++;
        }
        questionRepos.saveAll(listQuestions);
        resp.setStatus("Import thành công vào ngân hàng đề thi !");
        resp.setTotalQuestion(listQuestions.size());
        return resp;
    }

    private ObjectId convertUUIDToObjectId(UUID uuid) {
        byte[] uuidBytes = truncateUUID(uuid);
        return new ObjectId(uuidBytes);
    }

    private static byte[] truncateUUID(UUID uuid) {
        long mostSigBits = uuid.getMostSignificantBits();
        long leastSigBits = uuid.getLeastSignificantBits();

        byte[] bytes = new byte[12];

        for (int i = 0; i < 6; i++) {
            bytes[i] = (byte) (mostSigBits >> (8 * (5 - i)));
            bytes[6 + i] = (byte) (leastSigBits >> (8 * (5 - i)));
        }

        return bytes;
    }

    private int setLevelByName(Cell cell) {
        for (ExamTestEnum.DIFFICULTY difficulty : values()) {
            if (Objects.nonNull(cell) && cell.getStringCellValue().equals(difficulty.getName())) {
                return difficulty.getCode();
            }
        }
        return 0;
    }

    private int setLevelByCode(Cell cell) {
        for (ExamTestEnum.DIFFICULTY difficulty : values()) {
            if (Objects.nonNull(cell) && cell.getNumericCellValue() == difficulty.getCode()) {
                return difficulty.getCode();
            }
        }
        return 0;
    }

    public ExaminerImportResp importExaminer(Sheet sheet, String examId) {
        ExaminerImportResp resp = new ExaminerImportResp();
        List<ExaminerEntity> listExaminer = new ArrayList<>();
        CellReference cr = new CellReference("A3");
        int startRowIndex = cr.getRow();
        int lastRowNum = sheet.getLastRowNum();

        while (startRowIndex <= lastRowNum) {
            int cellIndex = 1;
            this.setValueExaminer(sheet, startRowIndex, cellIndex, examId, listExaminer);
            startRowIndex++;
        }

        resp.setStatus("Import thành công vào danh sách thí sinh !");
        resp.setTotalPerson(listExaminer.size());
        return resp;
    }

    private void setValueExaminer(Sheet sheet, int startRowIndex, int cellIndex,
                                  String examId, List<ExaminerEntity> listExaminer) {

        ExamEntity exam = examRepository.findById(new ObjectId(examId));
        if (Objects.isNull(exam)){
            throw new EOException(ENTITY_NOT_FOUND, MessageCodes.ENTITY_NOT_FOUND, examId);
        }
        List<ObjectId> examinerIds = new ArrayList<>();
        ExaminerEntity examiner = new ExaminerEntity();
        String userName = null;
        String email = null;
        String code = null;
        Date birthday = null;
        String personCode = null;
        String birthPlace = null;
        String classCode = null;
        String major = null;
        String educatePlace = null;
        String department = null;
        String educationLevel = null;
        String phoneNumber = null;

        Row currentRow = sheet.getRow(startRowIndex);
        if (Objects.nonNull(currentRow)) {

            Cell currentCell = currentRow.getCell(cellIndex);
            if (currentCell != null && currentCell.getCellTypeEnum() == CellType.STRING) {
                code = currentCell.getStringCellValue();

            } else if (currentCell != null && currentCell.getCellTypeEnum() == CellType.NUMERIC) {
                code = String.valueOf(currentCell.getNumericCellValue());
            }
            examiner.setCode(code);

            ++cellIndex;
            currentCell = currentRow.getCell(cellIndex);
            if (currentCell != null && currentCell.getCellTypeEnum() == CellType.STRING) {
                userName = currentCell.getStringCellValue();
            } else if (currentCell != null && currentCell.getCellTypeEnum() == CellType.NUMERIC) {
                userName = currentCell.getStringCellValue();
            }
            examiner.setName(userName);

            ++cellIndex;
            currentCell = currentRow.getCell(cellIndex);
            if (currentCell != null && currentCell.getCellTypeEnum() == CellType.STRING) {
                email = currentCell.getStringCellValue();
            }
            examiner.setEmail(email);

            ++cellIndex;
            currentCell = currentRow.getCell(cellIndex);
            if (currentCell != null && currentCell.getCellTypeEnum() == CellType.STRING) {
                birthday = DateUtils.convertFromString(currentCell.getStringCellValue());
            }
            examiner.setBirthday(birthday);

            ++cellIndex;
            currentCell = currentRow.getCell(cellIndex);
            if (currentCell != null && currentCell.getCellTypeEnum() == CellType.STRING) {
                birthPlace = currentCell.getStringCellValue();
            }
            examiner.setBirthPlace(birthPlace);

            ++cellIndex;
            currentCell = currentRow.getCell(cellIndex);
            if (currentCell != null && currentCell.getCellTypeEnum() == CellType.STRING) {
                personCode = currentCell.getStringCellValue();
            } else if (currentCell != null && currentCell.getCellTypeEnum() == CellType.NUMERIC) {
                personCode = String.valueOf(currentCell.getNumericCellValue());
            }
            examiner.setPersonCode(personCode);

            ++cellIndex;
            currentCell = currentRow.getCell(cellIndex);
            if (currentCell != null && currentCell.getCellTypeEnum() == CellType.STRING) {
                classCode = currentCell.getStringCellValue();
            } else if (currentCell != null && currentCell.getCellTypeEnum() == CellType.NUMERIC) {
                classCode = String.valueOf(currentCell.getNumericCellValue());
            }
            examiner.setClassRoom(classCode);

            ++cellIndex;
            currentCell = currentRow.getCell(cellIndex);
            if (currentCell != null && currentCell.getCellTypeEnum() == CellType.STRING) {
                major = currentCell.getStringCellValue();
            } else if (currentCell != null && currentCell.getCellTypeEnum() == CellType.NUMERIC) {
                major = String.valueOf(currentCell.getNumericCellValue());
            }
            examiner.setMajors(major);

            ++cellIndex;
            currentCell = currentRow.getCell(cellIndex);
            if (currentCell != null && currentCell.getCellTypeEnum() == CellType.STRING) {
                educatePlace = currentCell.getStringCellValue();
            } else if (currentCell != null && currentCell.getCellTypeEnum() == CellType.NUMERIC) {
                educatePlace = String.valueOf(currentCell.getNumericCellValue());
            }
            examiner.setSchool(educatePlace);

            ++cellIndex;
            currentCell = currentRow.getCell(cellIndex);
            if (currentCell != null && currentCell.getCellTypeEnum() == CellType.STRING) {
                department = currentCell.getStringCellValue();
            } else if (currentCell != null && currentCell.getCellTypeEnum() == CellType.NUMERIC) {
                department = String.valueOf(currentCell.getNumericCellValue());
            }
            examiner.setDepartments(department);

            ++cellIndex;
            currentCell = currentRow.getCell(cellIndex);
            if (currentCell != null && currentCell.getCellTypeEnum() == CellType.STRING) {
                educationLevel = currentCell.getStringCellValue();
            } else if (currentCell != null && currentCell.getCellTypeEnum() == CellType.NUMERIC) {
                educationLevel = String.valueOf(currentCell.getNumericCellValue());
            }
            examiner.setEducationLevel(educationLevel);

            ++cellIndex;
            currentCell = currentRow.getCell(cellIndex);
            if (currentCell != null && currentCell.getCellTypeEnum() == CellType.STRING) {
                phoneNumber = currentCell.getStringCellValue();
            } else if (currentCell != null && currentCell.getCellTypeEnum() == CellType.NUMERIC) {
                phoneNumber = String.valueOf(currentCell.getNumericCellValue());
            }
            examiner.setNumberPhone(phoneNumber);

            examiner.setExam(new ObjectId(examId));
            examinerRepos.save(examiner);
            examinerIds.add(new ObjectId(examiner.getId()));

            ExaminerUserRequestDto userRequestDto = new ExaminerUserRequestDto();
            UserDto userDto = new UserDto();
            this.setValueUser(userDto, examiner);
            userRequestDto.setUser(userDto);
            userRequestDto.setExaminerId(examiner.getId().toString());
            userService.createAccountExaminer(userRequestDto);
            listExaminer.add(examiner);
        }
        exam.setExaminersId(examinerIds);
    }

    public ExaminerImportResp importAccountExaminer(Sheet sheet, String examId) {

        ExaminerImportResp resp = new ExaminerImportResp();
        List<ExaminerEntity> listExaminer = new ArrayList<>();
        CellReference cr = new CellReference("A3");
        int startRowIndex = cr.getRow();
        int lastRowNum = sheet.getLastRowNum();

        Row currentRow;
        Cell currentCell;

        while (startRowIndex <= lastRowNum) {
            int cellIndex = 1;

            ExaminerEntity examiner = new ExaminerEntity();
            String userName = null;
            String email = null;
            String code = null;
            Date birthday = null;
            String personCode = null;

            currentRow = sheet.getRow(startRowIndex);
            if (Objects.nonNull(currentRow)) {

                currentCell = currentRow.getCell(cellIndex);
                if (currentCell != null && currentCell.getCellTypeEnum() == CellType.STRING) {
                    code = currentCell.getStringCellValue();

                } else if (currentCell != null && currentCell.getCellTypeEnum() == CellType.NUMERIC) {
                    code = String.valueOf(currentCell.getNumericCellValue());
                }
                examiner.setCode(code);

                ++cellIndex;
                currentCell = currentRow.getCell(cellIndex);
                if (currentCell != null && currentCell.getCellTypeEnum() == CellType.STRING) {
                    userName = currentCell.getStringCellValue();
                } else if (currentCell != null && currentCell.getCellTypeEnum() == CellType.NUMERIC) {
                    userName = currentCell.getStringCellValue();
                }
                examiner.setName(userName);

                ++cellIndex;
                currentCell = currentRow.getCell(cellIndex);
                if (currentCell != null && currentCell.getCellTypeEnum() == CellType.STRING) {
                    email = currentCell.getStringCellValue();
                }
                examiner.setEmail(email);

                ++cellIndex;
                currentCell = currentRow.getCell(cellIndex);
                if (currentCell != null && currentCell.getCellTypeEnum() == CellType.STRING) {
                    birthday = DateUtils.convertFromString(currentCell.getStringCellValue());
                }
                examiner.setBirthday(birthday);

                ++cellIndex;
                currentCell = currentRow.getCell(cellIndex);
                if (currentCell != null && currentCell.getCellTypeEnum() == CellType.STRING) {
                    personCode = currentCell.getStringCellValue();
                } else if (currentCell != null && currentCell.getCellTypeEnum() == CellType.NUMERIC) {
                    personCode = String.valueOf(currentCell.getNumericCellValue());
                }
                examiner.setPersonCode(personCode);
                examinerRepos.save(examiner);
                ExaminerUserRequestDto userRequestDto = new ExaminerUserRequestDto();
                UserDto userDto = new UserDto();
                this.setValueUser(userDto, examiner);
                userRequestDto.setUser(userDto);
                userRequestDto.setExaminerId(examiner.getId().toString());
                userService.createAccountExaminer(userRequestDto);
                listExaminer.add(examiner);
            }
            startRowIndex++;
        }
        resp.setStatus("Import thành công vào danh sách thí sinh !");
        resp.setTotalPerson(listExaminer.size());
        return resp;
    }

    public void setValueUser(UserDto userDto, ExaminerEntity examiner) {
        userDto.setUsername(examiner.getCode());
        userDto.setEmail(examiner.getEmail());
        userDto.setActive(true);
        userDto.setPassword("123456");
        RoleDto roleDto = new RoleDto();
        roleDto.setId(2L);
        roleDto.setName("ROLE_USER");
        roleDto.setDescription("USER");
        Set<RoleDto> roles = new HashSet<>();
        roles.add(roleDto);
        userDto.setRoles(roles);
    }

}
