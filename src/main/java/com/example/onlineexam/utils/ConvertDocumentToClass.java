package com.example.onlineexam.utils;

import org.bson.Document;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class ConvertDocumentToClass <T>{
    public T convertToClass(Document document, Class<T> clazz) throws IllegalAccessException, InstantiationException {
        T instance = clazz.newInstance();

        for (String fieldName : document.keySet()) {
            try {
                Field field = clazz.getDeclaredField(fieldName);
                field.setAccessible(true);
                field.set(instance, document.get(fieldName));
            } catch (NoSuchFieldException e) {
//                Todo: catch exception
            }
        }

        return instance;
    }

    public List<T> convertToClassList(List<Document> documents, Class<T> clazz) throws InstantiationException, IllegalAccessException {
        List<T> resultList = new ArrayList<>();
        for (Document document : documents) {
            T instance = convertToClass(document, clazz);
            resultList.add(instance);
        }
        return resultList;
    }
}
