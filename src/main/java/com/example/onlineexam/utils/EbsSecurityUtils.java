package com.example.onlineexam.utils;


import com.example.onlineexam.entity.User;
import com.example.onlineexam.error.CommonStatus;
import com.example.onlineexam.exception.EOException;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Objects;

public class EbsSecurityUtils {
    public static boolean isAuthenticated() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return !Objects.isNull(authentication) && authentication.isAuthenticated() && !(authentication instanceof AnonymousAuthenticationToken);
    }

    public static Object getPrincipal() {
        if (isAuthenticated()) {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            return authentication.getPrincipal();
        }

        return null;
    }

    public static String getUsername() {
        if (isAuthenticated()) {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            return ((User) authentication.getPrincipal()).getUsername();
        }

        throw new EOException(CommonStatus.FORBIDDEN);
    }

    public static User getCurrentUser() {
        if (isAuthenticated()) {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            return (User) authentication.getPrincipal();
        }

        throw new EOException(CommonStatus.FORBIDDEN);
    }
}
