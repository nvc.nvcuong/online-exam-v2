package com.example.onlineexam.utils;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import static com.example.onlineexam.constant.DateConstants.NORMAL_DATE;

public class DateUtils {

    public static Date convertFromString(String date) {
        SimpleDateFormat format = new SimpleDateFormat(NORMAL_DATE);
        java.util.Date parsed = null;
        try {
            parsed = format.parse(date);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
        return new Date(parsed.getTime());
    }
}
