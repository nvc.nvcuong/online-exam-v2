package com.example.onlineexam.utils;

import com.example.onlineexam.dto.request.searchDto.SearchDto;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import lombok.AllArgsConstructor;
import org.bson.Document;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Query;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
public class PageUtils {

    private static MongoCollection<Document> collection;

    public static <T> List<T> getContent(SearchDto dto, Query query, Class<T> clazz)
            throws IllegalAccessException, InstantiationException {
        int pageIndex = dto.getPageIndex() - 1;
        int pageSize = dto.getPageSize();

        int startPosition = pageSize * pageSize;

        List<T> getResultList = new ArrayList<>();
        FindIterable<Document> findIterable = collection
                .find(query.getQueryObject())
                .skip(startPosition)
                .limit(pageSize);
        for (Document document : findIterable) {
            ConvertDocumentToClass<T> converter = new ConvertDocumentToClass<>();
            T instance = converter.convertToClass(document, clazz);
            getResultList.add(instance);
        }
        return getResultList;
    }

    public static int getPageIndex(Integer pageIndex) {
        return pageIndex != null && pageIndex > 0 ? pageIndex - 1 : 1;
    }

    public static int getPageSize(Integer pageSize) {
        return pageSize != null && pageSize > 0 ? pageSize : 10;
    }

    public static Pageable getPageable(Integer pageIndex, Integer pageSize) {
        pageIndex = getPageIndex(pageIndex);
        pageSize = getPageSize(pageSize);
        return PageRequest.of(pageIndex, pageSize);
    }
}
