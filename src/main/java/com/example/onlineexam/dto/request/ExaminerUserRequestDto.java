package com.example.onlineexam.dto.request;

import com.example.onlineexam.dto.UserDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ExaminerUserRequestDto {

    private UserDto user;
    private String examinerId;
    private ExaminerRequestDto examiner;
}
