package com.example.onlineexam.dto.request.searchDto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ExamSubjectSearchDto extends SearchDto{
    private String subjectName;

}
