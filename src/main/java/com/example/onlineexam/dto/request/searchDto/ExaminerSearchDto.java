package com.example.onlineexam.dto.request.searchDto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExaminerSearchDto extends SearchDto{

    private String examId;
    private String role;

}
