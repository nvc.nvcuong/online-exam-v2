package com.example.onlineexam.dto.request.searchDto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SearchUserDto extends SearchDto{
    private Long role;

    private String userName;

    private String examId;

    private String sessionId;
}
