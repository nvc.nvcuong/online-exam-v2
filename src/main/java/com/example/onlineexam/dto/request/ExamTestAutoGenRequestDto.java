package com.example.onlineexam.dto.request;

import lombok.*;

import javax.validation.constraints.NotNull;

import java.util.List;

import static com.example.onlineexam.constant.MessageCodes.NOT_NULL;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ExamTestAutoGenRequestDto {

    private ExamTestRequestDto examTest;

    @NotNull(message = NOT_NULL)
    private Integer numberEasy;

    @NotNull(message = NOT_NULL)
    private Integer numberMedium;

    @NotNull(message = NOT_NULL)
    private Integer numberHard;
    @NotNull(message = NOT_NULL)
    private Integer  numberVeryHard;
    @NotNull(message = NOT_NULL)
    private Integer numberQuestion;

    private Integer numberTest;

    private String examId;

    private List<String> subjectIds;
}
