package com.example.onlineexam.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ExamSessionRequestDto {
    private String id;

    private Date examDay;

    private ExamRequestDto exam;

    private ExamSubjectRequestDto subject;

    private List<ExamRoomRequestDto> examRoom;

    private int startSession;

    private int endSession;
}
