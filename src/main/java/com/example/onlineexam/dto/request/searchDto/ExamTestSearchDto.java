package com.example.onlineexam.dto.request.searchDto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalTime;

@Setter
@Getter
public class ExamTestSearchDto extends SearchDto{

    private String testName;

    private String password;

    private Integer level;

    private Integer totalQuestion;

    private LocalTime totalToDo;

    private String note;

    private int status;

    private LocalTime times;

    private String examId;

    private String sessionId;

    private Integer type;
}
