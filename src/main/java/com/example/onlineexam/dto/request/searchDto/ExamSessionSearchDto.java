package com.example.onlineexam.dto.request.searchDto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExamSessionSearchDto extends SearchDto{
    private int startTime;
    private int endTime;
}
