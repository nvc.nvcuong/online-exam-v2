package com.example.onlineexam.dto.request;

import com.example.onlineexam.dto.response.ExamSessionResponseDto;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
@Getter
@Setter
public class ExaminerImportRequest {

    private List<ExamSessionRequestDto> session;
    private String examId;
}
