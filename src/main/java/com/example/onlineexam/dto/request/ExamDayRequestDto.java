package com.example.onlineexam.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalTime;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ExamDayRequestDto {

    private Date examDay;

    private int examSlot;

    private LocalTime startTime;

    private LocalTime endTime;

    private ExamRoomRequestDto room;

    private ExamRequestDto exam;
}
