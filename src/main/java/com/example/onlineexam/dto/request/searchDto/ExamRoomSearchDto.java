package com.example.onlineexam.dto.request.searchDto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExamRoomSearchDto extends SearchDto {

    private String nameRoom;
    private int slots;
}
