package com.example.onlineexam.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ExamRoomRequestDto {
    private  String id;
    private String nameRoom;
    private int slots;
}
