package com.example.onlineexam.dto.request.searchDto;

import lombok.Getter;
import lombok.Setter;
import org.bson.types.ObjectId;

import java.util.List;
import java.util.Map;
@Getter
@Setter
public class ExamTestResultSerchDto extends SearchDto{
    private String examId;
    private String testId;
}
