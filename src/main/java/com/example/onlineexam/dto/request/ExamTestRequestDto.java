package com.example.onlineexam.dto.request;

import com.example.onlineexam.constant.enums.ExamTestStatus;
import com.example.onlineexam.constant.enums.STATUS;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.LocalTime;
import java.util.Date;
import java.util.List;

import static com.example.onlineexam.constant.DateConstants.HCM_TIMEZONE;
import static com.example.onlineexam.constant.DateConstants.NORMAL_DATE;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ExamTestRequestDto {

    private String testName;

    private String testCode;

    private String password;

    private int level;

    private int totalQuestion;

    @JsonFormat(
            shape = JsonFormat.Shape.STRING,
            pattern = "HH:mm:ss"
    )
    private LocalTime totalToDo;

    private String note;

//    @JsonFormat(
//            shape = JsonFormat.Shape.STRING,
//            pattern = "HH:mm:ss"
//    )
    private int times;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = NORMAL_DATE, timezone = HCM_TIMEZONE)
    private Date examDay;

    private List<String> listQuestionid;

    private ExaminerRequestDto examiner;

    private ExamSubjectRequestDto subject;

    private ExamRequestDto exam;

    private ExamSessionRequestDto session;
}
