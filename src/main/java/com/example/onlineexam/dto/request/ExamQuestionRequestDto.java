package com.example.onlineexam.dto.request;

import com.example.onlineexam.constant.enums.QuestionStatusEnum;
import lombok.*;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ExamQuestionRequestDto {

    private String questionCode;
    private int level;
    private String questionContent;
    private String answer1;
    private String answer2;
    private String answer3;
    private String answer4;
    private String answer5;
    private String answer6;
    private String answer7;
    private String answer8;
    private String answer9;
    private String answer10;
    private String correctAnswer;
    private QuestionStatusEnum status;
    private ExamSubjectRequestDto subject;
    private String suggest;
    private ExamRequestDto exam;
    private String fileName;
    private String fileId;
}
