package com.example.onlineexam.dto.request.searchDto;

import lombok.Getter;

import java.time.LocalTime;
import java.util.Date;
import java.util.UUID;

@Getter
public class ExamDaySearchDto extends SearchDto{
    private Date examDay;
    private int examSlot;
    private LocalTime startTime;
    private LocalTime endTime;
    private UUID roomId;
    private UUID examId;
}
