package com.example.onlineexam.dto.request.searchDto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StatisticalQuestSearchDto extends SearchDto{
    private String examId;

    private int level;

    private String subjectId;

    private String sessionId;
}
