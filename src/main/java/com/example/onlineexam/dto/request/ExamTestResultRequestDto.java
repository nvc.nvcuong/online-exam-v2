package com.example.onlineexam.dto.request;

import com.example.onlineexam.constant.enums.ExamTestStatus.RESULT_STATUS;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bson.types.ObjectId;

import java.util.List;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ExamTestResultRequestDto {
    private String examinerId;
    private String testId;
    private List<QuestionAnswerRequestDto> questionAnswer;
    private int totalQuestion;
    private RESULT_STATUS status;
    private int violate;
}
