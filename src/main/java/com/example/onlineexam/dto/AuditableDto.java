package com.example.onlineexam.dto;

import com.example.onlineexam.constant.DateConstants;
import com.example.onlineexam.entity.AuditableEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
public class AuditableDto implements Serializable {
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateConstants.NORMAL_DATE_TIME)
    protected LocalDateTime createDate;

    protected String createdBy;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateConstants.NORMAL_DATE_TIME)
    protected LocalDateTime modifyDate;

    protected String modifyBy;

    public AuditableDto(AuditableEntity entity) {
        if (entity != null) {
            this.createDate = entity.getCreateDate();
            this.createdBy = entity.getCreateBy();
            this.modifyDate = entity.getModifyDate();
            this.modifyBy = entity.getModifyBy();
        }
    }

    public AuditableDto() {
        this.createDate = LocalDateTime.now();
        this.modifyDate = LocalDateTime.now();
    }
}
