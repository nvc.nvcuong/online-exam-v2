package com.example.onlineexam.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static com.example.onlineexam.constant.DateConstants.HCM_TIMEZONE;
import static com.example.onlineexam.constant.DateConstants.NORMAL_DATE;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserDto extends AuditableDto{
    private Long id;
    private String username;
    private String email;
    private Set<RoleDto> roles = new HashSet<>();
    private Boolean active;
    private String codeSMS;
    private String oldPassword;
    private String password;
    private String confirmPassword;

}
