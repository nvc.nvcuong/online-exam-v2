package com.example.onlineexam.dto.response;

import com.example.onlineexam.entity.ExamRoomEntity;
import com.example.onlineexam.entity.ExaminerEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalTime;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor

public class ExamDayResponseDto {

    private String id;
    private Date examDay;
    private int examSlot;
    private LocalTime startTime;
    private LocalTime endTime;
    private ExamRoomEntity room;
    private ExaminerEntity examEntity;
}
