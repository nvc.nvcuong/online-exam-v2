package com.example.onlineexam.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class TestResultDetailReponse {
    private String content;
    private List<String> answers;
    private QuestionAnswerResponseDto questionAnswer;

}
