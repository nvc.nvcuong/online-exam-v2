package com.example.onlineexam.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class ExamRoomResponseDto {
    private String id;
    private String nameRoom;
    private int slots;
}
