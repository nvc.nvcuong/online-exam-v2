package com.example.onlineexam.dto.response;

import com.example.onlineexam.entity.Role;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UserResponseDto {
    private String id;
    private String userName;
    private String password;
    private String name;
    private LocalDateTime lastLogin;
    private Role role;
    private String email;
}
