package com.example.onlineexam.dto.response;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;

import static com.example.onlineexam.constant.DateConstants.HCM_TIMEZONE;
import static com.example.onlineexam.constant.DateConstants.NORMAL_DATE;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ExamSessionResponseDto {

    private String id;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = NORMAL_DATE, timezone = HCM_TIMEZONE)
    private Date examDay;

    private ExamResponseDto exam;
    private ExamSubjectResponseDto subject;
    private List<ExamRoomResponseDto> room;
    private int startSession;
    private int endSession;
}
