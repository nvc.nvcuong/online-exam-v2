package com.example.onlineexam.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class StatisticalQuestionRespDto {

    private String subjectId;
    private int totalQuestion;
    private int totalQuestEasy;
    private int totalQuestMedium;
    private int totalQuestHard;
    private int totalQuestVeryHard;
}
