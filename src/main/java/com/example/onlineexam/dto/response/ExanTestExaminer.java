package com.example.onlineexam.dto.response;

import com.example.onlineexam.entity.ExaminerEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

import static com.example.onlineexam.constant.DateConstants.HCM_TIMEZONE;
import static com.example.onlineexam.constant.DateConstants.NORMAL_DATE;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ExanTestExaminer {
    private String id;
    private String nameTest;
    private ExaminerUserResponseDto examinerUser;
    private ExamSubjectResponseDto subject;
    private ExamSessionResponseDto examSession;
}
