package com.example.onlineexam.dto.response;


import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ExamQuestionImportResp {

    private String status;
    private int totalQuestion;
    private List<ExamQuestionResponseDto> questions;
}
