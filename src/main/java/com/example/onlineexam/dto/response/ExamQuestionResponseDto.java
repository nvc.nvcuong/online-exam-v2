package com.example.onlineexam.dto.response;

import com.example.onlineexam.constant.enums.QuestionStatusEnum;
import lombok.*;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ExamQuestionResponseDto {

    private String id;
    private String questionCode;
    private int level;
    private String questionContent;
    private List<String> answers;
    private String correctAnswer;
    private QuestionStatusEnum status;    // Todo EnumStatus
    private ExamSubjectResponseDto subject;
    private String suggest;
    private ExamResponseDto exam;
    private String fileName;
    private String fileId;
}
