package com.example.onlineexam.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FileDescriptionResponseDto {

    private String id;

    private String uuid;

    private String name;

    private String contentType;

    private Long contentSize;

    private String extension;

    private String filePath;
}
