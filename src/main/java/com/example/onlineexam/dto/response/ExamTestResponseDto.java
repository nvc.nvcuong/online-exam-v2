package com.example.onlineexam.dto.response;

import com.example.onlineexam.constant.enums.ExamTestStatus;
import com.example.onlineexam.constant.enums.STATUS;
import com.example.onlineexam.dto.request.ExamSessionRequestDto;
import com.example.onlineexam.entity.ExamQuestionEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bson.types.ObjectId;

import java.time.LocalTime;
import java.util.Date;
import java.util.List;

import static com.example.onlineexam.constant.DateConstants.HCM_TIMEZONE;
import static com.example.onlineexam.constant.DateConstants.NORMAL_DATE;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ExamTestResponseDto {

    private String id;

    private String testName;

    private String testCode;

    private String password;

    private int level;

    private int totalQuestion;

    @JsonFormat(
            shape = JsonFormat.Shape.STRING,
            pattern = "HH:mm:ss"
    )
    private LocalTime totalToDo;

    private String note;
    private ExamTestStatus.TEST_TYPE type;
    private ExamTestStatus.TEST_STATUS status;
//    @JsonFormat(
//            shape = JsonFormat.Shape.STRING,
//            pattern = "HH:mm:ss"
//    )

    private int times;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = NORMAL_DATE, timezone = HCM_TIMEZONE)
    private Date examDay;

    private List<ExamQuestionResponseDto> question;

    private ExaminerResponseDto examiner;

    private ExamSubjectResponseDto subject;
    private ExamResponseDto exam;
    private ExamSessionResponseDto session;
}
