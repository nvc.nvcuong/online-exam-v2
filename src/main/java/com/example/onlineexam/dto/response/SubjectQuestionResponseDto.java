package com.example.onlineexam.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SubjectQuestionResponseDto {
    private String subjectId;
    private String subjectName;
    private StatisticalQuestionRespDto stats;
}
