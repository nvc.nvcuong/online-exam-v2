package com.example.onlineexam.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StatsSubjectAnswerQuestResp {

    private String subjectId;
    private String subjectName;
    private int totalWrong;
    private int totalCorrect;
}
