package com.example.onlineexam.dto.response;

import com.example.onlineexam.dto.UserDto;
import com.example.onlineexam.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ExaminerUserResponseDto {

    private UserDto user;
    private ExaminerResponseDto examiner;
    private ExamTestResponseDto test;

}
