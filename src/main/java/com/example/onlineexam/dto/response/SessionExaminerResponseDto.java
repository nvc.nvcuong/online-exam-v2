package com.example.onlineexam.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SessionExaminerResponseDto {

    private String sessionId;
    private String sessionName;
    private int numberExaminer;

}
