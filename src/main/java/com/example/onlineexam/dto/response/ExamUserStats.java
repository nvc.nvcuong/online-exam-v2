package com.example.onlineexam.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ExamUserStats {

    private String examId;
    private String examName;
    private int numberExaminers;
}
