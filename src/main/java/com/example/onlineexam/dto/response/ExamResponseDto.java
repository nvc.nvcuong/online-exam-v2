package com.example.onlineexam.dto.response;

import com.example.onlineexam.constant.enums.STATUS;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;

import static com.example.onlineexam.constant.DateConstants.HCM_TIMEZONE;
import static com.example.onlineexam.constant.DateConstants.NORMAL_DATE;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ExamResponseDto {

    private String id;

    private String examName;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = NORMAL_DATE, timezone = HCM_TIMEZONE)
    private Date startDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = NORMAL_DATE, timezone = HCM_TIMEZONE)
    private Date endDate;

    private List<ExaminerResponseDto> examinersId;

    private List<ExamTestResponseDto> examTest;

    private STATUS status;

    private List<ExamSessionResponseDto> sessions;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
