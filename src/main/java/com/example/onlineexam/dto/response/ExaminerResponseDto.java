package com.example.onlineexam.dto.response;

import com.example.onlineexam.constant.enums.ExaminerUserEnum.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


import java.time.LocalTime;
import java.util.Date;


import static com.example.onlineexam.constant.DateConstants.HCM_TIMEZONE;
import static com.example.onlineexam.constant.DateConstants.NORMAL_DATE;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ExaminerResponseDto {

    private String id;

    private String name;

    private String email;

    private String code;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = NORMAL_DATE, timezone = HCM_TIMEZONE)
    private Date birthday;

    private String personCode;

    private String doing;

    @JsonFormat(
            shape = JsonFormat.Shape.STRING,
            pattern = "HH:mm:ss"
    )
    private LocalTime startTime;

    @JsonFormat(
            shape = JsonFormat.Shape.STRING,
            pattern = "HH:mm:ss"
    )
    private LocalTime timeRemaining;

    private ExamRoomResponseDto room;

    private ExamResponseDto exam;

    private ExamSessionResponseDto session;

    private String sex;

    private String classRoom;

    private String majors;

    private String school;

    private String departments;
    private Long userId;
    private String birthPlace;

    private String educationLevel;

    private String numberPhone;

    private String fileName;

    private String fileId;

    private USER_STATUS status;

}
