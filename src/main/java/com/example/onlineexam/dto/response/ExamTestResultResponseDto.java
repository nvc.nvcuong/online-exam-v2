package com.example.onlineexam.dto.response;

import com.example.onlineexam.constant.enums.ExamTestStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bson.types.ObjectId;

import java.util.List;
import java.util.Map;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ExamTestResultResponseDto {
    private String id;
    private ExaminerResponseDto examiner;
    private ExamTestResponseDto test;
    private List<QuestionAnswerResponseDto> questionAnswer;
    private int correctAnswers;
    private int incorrectAnswers;
    private int totalQuestion;
    private float mark;
    private ExamTestStatus.RESULT_STATUS status;
    private int violate;
}
