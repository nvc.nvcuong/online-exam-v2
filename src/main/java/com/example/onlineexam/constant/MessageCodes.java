package com.example.onlineexam.constant;

public class MessageCodes {
    public static final String ENTITY_NOT_FOUND = "errors.not_found_entity";
    public static final String NOT_NULL = "errors.not_null";
    public static final String INVALID_VALUE = "error.value.invalid_value";

    public static final String DUPLICATE_USER_NAME = "error.user.duplicate_user";

    public static final String INVALID_STATUS = "error.invalid_status";
    public static final String ENTITY_NOT_EXIST = "error.entity.entity_not_exist";

    public static final String EXAM_ID_NOT_NULL = "error.entity.exam_id_not_null";
    public static final String EXAM_NOT_EXIST = "error.entity.exam_not_exist";

    public static final String NUMBER_QUESTION_IS_NULL_OR_BIGER_TOTAL_QUESTION = "error.entity.number_question_is_null_or_biger_total_question";
    public static final String NUMBER_QUESTION_NOT_ENOUGH="error.number of questions is not enough";

    public static final String EXAMINER_NOT_FOUND = "error.entity.examiner_not_found";
    public static final String PROCESSED_SUCCESSFULLY ="Successfully";
}

