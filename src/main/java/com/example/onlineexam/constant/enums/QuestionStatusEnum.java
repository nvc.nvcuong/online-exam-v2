package com.example.onlineexam.constant.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum QuestionStatusEnum {
    DISABLED(1, "Disabled"),
    ENABLED(2, "Enabled");

    private int code;
    private String name;

    @JsonCreator
    public static QuestionStatusEnum parseByCode(@JsonProperty("code") Integer code) {
        if (code == null) {
            return null;
        }
        for (QuestionStatusEnum status : values()) {
            if (status.code == code) {
                return status;
            }
        }
        return null;
    }
    @JsonValue
    public int getCode() {
        return code;
    }

}
