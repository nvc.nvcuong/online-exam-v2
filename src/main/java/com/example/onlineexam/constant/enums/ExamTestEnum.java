package com.example.onlineexam.constant.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

public class ExamTestEnum {

    @AllArgsConstructor
    @Getter
    public enum DIFFICULTY {
        EASY(1, "Dễ"),
        MEDIUM(2, "Trung bình"),
        HARD(3, "Khó"),
        VERY_HARD(4, "Vận dụng");

        private final int code;
        private final String name;

        @JsonCreator
        public static DIFFICULTY parseByCode(@JsonProperty("code") Integer code) {
            if (code == null) {
                return null;
            }
            for (DIFFICULTY status : values()) {
                if (status.code == code) {
                    return status;
                }
            }
            return null;
        }
    }
}
