package com.example.onlineexam.constant.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

public class ExamTestStatus {

    @Getter
    @AllArgsConstructor
    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public enum TEST_STATUS {

        CHUA_DUYET(1, "Chưa duyệt"),
        DA_DUYET(2, "Đã duyệt"),
        CHUA_THI(3, "Chưa thi"),
        DANG_THI(4, "Đang thi"),
        DA_THI(5, "Đã thi")
        ;

        private final int code;
        private final String name;

        @JsonCreator
        public static TEST_STATUS parseByCode(@JsonProperty("code") Integer code) {
            if (code == null) {
                return null;
            }
            for (TEST_STATUS status : values()) {
                if (status.code == code) {
                    return status;
                }
            }
            return null;
        }

        @Override
        public String toString() {
            return this.name;
        }
    }

    @Getter
    @AllArgsConstructor
    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public enum RESULT_STATUS {

        CHUA_CHAM(1, "Chưa chấm"),
        DA_CHAM(2, "Đã chấm");

        private final int code;
        private final String name;

        @JsonCreator
        public static RESULT_STATUS parseByCode(@JsonProperty("code") Integer code) {
            if (code == null) {
                return null;
            }
            for (RESULT_STATUS status : values()) {
                if (status.code == code) {
                    return status;
                }
            }
            return null;
        }

        @Override
        public String toString() {
            return this.name;
        }
    }

    @Getter
    @AllArgsConstructor
    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public enum TEST_TYPE {

        DE_MAU(1, "Đề mẫu"),
        DE_THI(2, "Đề thi");

        private final int code;
        private final String name;

        @JsonCreator
        public static TEST_TYPE parseByCode(@JsonProperty("code") Integer code) {
            if (code == null) {
                return null;
            }
            for (TEST_TYPE type : values()) {
                if (type.code == code) {
                    return type;
                }
            }
            return null;
        }

        @Override
        public String toString() {
            return this.name;
        }
    }
}
