package com.example.onlineexam.constant.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

public class ExaminerUserEnum{
    @Getter
    @AllArgsConstructor
    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public enum USER_STATUS {

        CHUA_THI(1, "Chưa thi"),
        DANG_THI(2, "Đang thi"),
        DA_THI(3, "Đã thi");

        private final int code;
        private final String name;

        @JsonCreator
        public static USER_STATUS parseByCode(@JsonProperty("code") Integer code) {
            if (code == null) {
                return null;
            }
            for (USER_STATUS status : values()) {
                if (status.code == code) {
                    return status;
                }
            }
            return null;
        }

        @Override
        public String toString() {
            return this.name;
        }
    }
}

