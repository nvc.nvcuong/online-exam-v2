package com.example.onlineexam.constant;

public class ErrorCodes {

    public static final int ERROR_CODE = 400;
    public static final int ENTITY_NOT_FOUND = 5001;
    public static final int CONFLICT_VALUE  = 409;
    public static final int DUPLICATE_USER = 410;

    public static final int NOT_EMPTY = 411;

    public static final int INVALID_STATUS = 412;
    public static final int ENTITY_NOT_EXIST = 413;
}
