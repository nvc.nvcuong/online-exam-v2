package com.example.onlineexam.error;

/*
* code 412_***
* */
public interface ErrorStatus {
    int getCode();

    String getMessage();
}
