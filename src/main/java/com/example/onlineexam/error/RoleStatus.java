package com.example.onlineexam.error;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum RoleStatus implements ErrorStatus{
    ID_NOT_EXIST(400, "errors.id_not_exist"),
    NAME_IS_EXIST(400, "errors.name_is_exist"),
    NAME_IS_EMPTY(400, "errors.name_is_exist"),
    ;

    private final int code;
    private final String message;

    @Override
    public int getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}
