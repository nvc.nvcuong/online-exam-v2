package com.example.onlineexam.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Objects;

/**
 * Quyền
 */

@Entity
@Table(name = "tbl_role")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Role extends BaseObject implements GrantedAuthority {

    @Transient
    private static final long serialVersionUID = 6318192070978248463L;
    @Column(length = 150, nullable = false, unique = true)
    private String name;
    @Column(length = 150)
    private String description;

    @Override
    public String getAuthority() {
        return this.name;
    }
}
