package com.example.onlineexam.entity;

import com.example.onlineexam.constant.enums.ExamTestStatus.RESULT_STATUS;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bson.types.ObjectId;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_test_result")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ExamTestResultEntity extends BaseObject {

    @Column(name = "examiner_id")
    private ObjectId examiner;

    @Column(name = "exam_test_id")
    private ObjectId test;

//    @Column(name = "question_answer")
//    private List<QuestionAnswerEntity> questionAnswer;

    @Column(name = "correct_answers")
    private int correctAnswers;

    @Column(name = "incorrect_answers")
    private int incorrectAnswers;

    @Column(name = "total_question")
    private int totalQuestion;

    @Column(name = "mark")
    private float mark;

    @Column(name = "status")
    private int status;

    @Column(name = "exam_id")
    private ObjectId examId;
    @Column(name = "violate")
    private int violate;

    public RESULT_STATUS getStatus() {
        return  RESULT_STATUS.parseByCode(this.status);
    }

    public void setStatus(RESULT_STATUS status) {
        this.status = status.getCode();
    }

}
