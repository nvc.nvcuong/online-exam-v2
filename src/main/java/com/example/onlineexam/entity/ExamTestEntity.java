package com.example.onlineexam.entity;

import com.example.onlineexam.constant.enums.ExamTestStatus.TEST_STATUS;
import com.example.onlineexam.constant.enums.ExamTestStatus.TEST_TYPE;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalTime;
import java.util.Date;

@Entity
@Document(collection = "tbl_test")
@Table(name = "tbl_test")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ExamTestEntity extends BaseObject{

    @Column(name = "test_code")
    private String testCode;

    @Column(name = "test_name")
    private String testName;

    @Column(name = "password")
    private String password;

    @Column(name = "level")
    private int level;

    @Column(name = "total_questions")
    private int totalQuestion;

    @Column(name = "total_todo")
    private LocalTime totalToDo;

    @Column(name = "note")
    private String note;

    @Column(name = "times")
    private int times;

    @Column(name = "exam_day")
    private Date examDay;

//    @Column(name = "question_ids")
//    private List<ObjectId> question;

    @Column(name = "examiner_id")
    private ObjectId examiner ;

    @Column(name = "subject_id")
    private ObjectId subject;

    @Column(name = "exam_id")
    private ObjectId exam;

    @Column(name = "session_id")
    private ObjectId session;

    @Column(name = "status")
    private int status;

    @Column(name = "type")
    private int type;

    @Column(name = "level_score")
    private int levelScore;

    public void setStatus(TEST_STATUS status) {
        this.status = status.getCode();
    }

    public TEST_STATUS getStatus() {
        return TEST_STATUS.parseByCode(this.status);
    }

    public void setType(TEST_TYPE type) {
        this.type = type.getCode();
    }

    public TEST_TYPE getType() {
        return TEST_TYPE.parseByCode(this.type);
    }

}
