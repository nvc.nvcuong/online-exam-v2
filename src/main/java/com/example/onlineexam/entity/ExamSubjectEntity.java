package com.example.onlineexam.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "tbl_subject")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ExamSubjectEntity extends BaseObject {

    @Column(name = "subject_name")
    private String subjectName;

//    @Field("question_ids")
//    private List<ObjectId> questions;
}
