package com.example.onlineexam.entity;

import com.example.onlineexam.constant.enums.STATUS;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;


@Getter
@Setter
@Entity
@Table(name = "tbl_exam")
@NoArgsConstructor
@AllArgsConstructor
public class ExamEntity extends BaseObject {
    @Column(name = "exam_name")
    private String examName;

    @Column(name = "start_date")
    private Date startDate;

    @Column(name = "end_date")
    private Date endDate;

//    @Column(name = "examiner_ids")
//    private List<ObjectId> examinersId;
//
//    @Column(name = "exam_tests")
//    private List<ObjectId> examTest;

    @Column(name = "status")
    private int status;

    public STATUS getStatus() {
        return STATUS.parseByCode(this.status);
    }

    public void setStatus(STATUS status) {
        this.status = status.getCode();
    }

}
