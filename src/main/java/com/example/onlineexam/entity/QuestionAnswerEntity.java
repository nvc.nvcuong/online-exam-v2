package com.example.onlineexam.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name = "question_answer")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class QuestionAnswerEntity extends BaseObject{

    @Column(name = "question_id")
    private UUID questionId;

    @Column(name = "subject_id")
    private UUID subjectId;

    @Column(name = "answer")
    private String answer;

    @Column(name = "correct_answer")
    private String correctAnswer;

}
