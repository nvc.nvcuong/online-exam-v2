package com.example.onlineexam.entity;

import com.example.onlineexam.constant.enums.ExaminerUserEnum.USER_STATUS;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bson.types.ObjectId;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalTime;
import java.util.Date;

@Entity
@Table(name = "tbl_examiner")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ExaminerEntity extends BaseObject {

    @Column(name = "name")
    private String name;

    @Column(name = "email")
    private String email;

    @Column(name = "code")
    private String code;

    @Column(name = "birthday")
    private Date birthday;

    @Column(name = "person_code")
    private String personCode;

    @Column(name = "doing")
    private String doing;

    @Column(name = "start_time")
    private LocalTime startTime;

    @Column(name = "time_remaining")
    private LocalTime timeRemaining;

    @Column(name = "room_id")
    private ObjectId room;

    @Column(name = "exam_id")
    private ObjectId exam;

    @Column(name = "session_id")
    private ObjectId session;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "sex")
    private String sex;

    @Column(name = "class_room")
    private String classRoom;

    @Column(name = "majors")
    private String majors;

    @Column(name = "school")
    private String school;

    @Column(name = "departments")
    private String departments;

    @Column(name = "birth_place")
    private String birthPlace;

    @Column(name = "education_level")
    private String educationLevel;

    @Column(name = "number_Phone")
    private String numberPhone;

    @Column(name = "file_Name")
    private String fileName;

    @Column(name = "fileId")
    private String fileId;

    @Column(name = "status")
    private int status;

    public USER_STATUS getStatus() {
        return USER_STATUS.parseByCode(this.status);
    }

    public void setStatus(USER_STATUS status) {
        this.status = status.getCode();
    }

}
