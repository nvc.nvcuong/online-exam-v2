package com.example.onlineexam.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name = "tbl_rooms")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ExamRoomEntity extends BaseObject {

    @Column(name = "name_room")
    private String nameRoom;

    @Column(name = "slots")
    private int slots;

    @Column(name = "session_id")
    private UUID sessionId;

}
