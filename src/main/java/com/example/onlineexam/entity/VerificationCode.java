package com.example.onlineexam.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

/**
 * Mã xác thực khi tạo tài khoản hoặc đổi mật khẩu
 */

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Document("tbl_verification_codes")
public class VerificationCode extends BaseObject {

    private static final long serialVersionUID = 4572951405687566992L;
    private Long userId;
    private String username;
    private String email;
    private String codeSMS;
    private LocalDateTime expired;
    private Integer type;
}
