package com.example.onlineexam.entity;

import com.example.onlineexam.constant.enums.QuestionStatusEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bson.types.ObjectId;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "tbl_question")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ExamQuestionEntity extends BaseObject {

    @Column(name = "question_code")
    private String questionCode;

    @Column(name = "level")
    private int level;

    @Column(name = "question_content")
    private String questionContent;

    @Column(name = "answer_1")
    private String answer1;

    @Column(name = "answer_2")
    private String answer2;

    @Column(name = "answer_3")
    private String answer3;

    @Column(name = "answer_4")
    private String answer4;

    @Column(name = "answer_5")
    private String answer5;

    @Column(name = "answer_6")
    private String answer6;

    @Column(name = "answer_7")
    private String answer7;

    @Column(name = "answer_8")
    private String answer8;

    @Column(name = "answer_9")
    private String answer9;

    @Column(name = "answer_10")
    private String answer10;

    @Column(name = "correct_answer")
    private String correctAnswer;

    // Todo EnumStatus
    @Column(name = "status")
    private int status;

    @Column(name = "subject_id")
    private ObjectId subject;

    @Column(name = "suggest")
    private String suggest;

    @Column(name = "exam_id")
    private ObjectId exam;

    @Column(name = "file_Name")
    private String fileName;

    @Column(name = "fileId")
    private String fileId;

    public void setStatus(QuestionStatusEnum status) {
        this.status = status.getCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ExamQuestionEntity)) return false;
        if (!super.equals(o)) return false;
        ExamQuestionEntity that = (ExamQuestionEntity) o;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getId());
    }

    public QuestionStatusEnum getStatus() {
        return QuestionStatusEnum.parseByCode(status);
    }
}
