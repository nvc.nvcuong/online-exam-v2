package com.example.onlineexam.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bson.types.ObjectId;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;
import java.util.UUID;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "tbl_session")
public class ExamSessionEntity extends BaseObject {

    @Column(name = "exam_day")
    private Date examDay;

    @Column(name = "exam_id")
    private UUID examId;

    @Column(name = "subject_id")
    private UUID subjectId;

//    @Column(name = "room_id")
//    private List<ObjectId> examRoom;

    @Column(name = "start_session")
    private int startSession;

    @Column(name = "end_session")
    private int endSession;
}